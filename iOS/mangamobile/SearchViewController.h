//
//  SearchViewController.h
//  mangamobile
//
//  Created by rukanishino on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController <UITextFieldDelegate>
{
    //Textfield for searching
    IBOutlet UITextField *m_SearchField;
}
@property (nonatomic, retain) IBOutlet UITextField *m_SearchField;

//Method for handling background touched
-(IBAction)backgroundTouched:(id)sender;

@end
