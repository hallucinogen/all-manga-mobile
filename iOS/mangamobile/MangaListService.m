//
//  MangaListService.m
//  mangamobile
//
//  Created by rukanishino on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MangaListService.h"

@implementation MangaListService

-(id) initWithTitle:(NSString *)title andImage:(UIImage *)image
{
    self = [super init];
    
    if (self)
    {
        self.mangaTitle = title;
        self.mangaImage = image;
    }
    
    return self;
}

+(NSArray*) getSampleData
{
    MangaListService* service1 = [[MangaListService alloc] initWithTitle:@"Taiyou ni Ie" andImage:[UIImage imageNamed:@"manga-image-test.png"]];
    MangaListService* service2 = [[MangaListService alloc] initWithTitle:@"Taiyou ni Ie" andImage:[UIImage imageNamed:@"manga-image-test.png"]];
    MangaListService* service3 = [[MangaListService alloc] initWithTitle:@"Taiyou ni Ie" andImage:[UIImage imageNamed:@"manga-image-test.png"]];
    MangaListService* service4 = [[MangaListService alloc] initWithTitle:@"Taiyou ni Ie" andImage:[UIImage imageNamed:@"manga-image-test.png"]];
    
    return [NSArray arrayWithObjects:service1, service2, service3, service4, nil];
}

@synthesize mangaTitle;
@synthesize mangaImage;

@end
