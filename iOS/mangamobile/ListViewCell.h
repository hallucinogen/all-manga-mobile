//
//  ListViewCell.h
//  mangamobile
//
//  Created by rukanishino on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQGridView.h"

@interface ListViewCell : AQGridViewCell

//Manga Image
@property (nonatomic, retain) UIImageView*  mangaImage;

//Manga Label
@property (nonatomic, retain) UILabel*      mangaLabel;

@end
