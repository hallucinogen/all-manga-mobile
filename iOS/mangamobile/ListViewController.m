//
//  ListViewController.m
//  mangamobile
//
//  Created by rukanishino on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ListViewController.h"
#import "ListViewCell.h"
#import "MangaListService.h"

@interface ListViewController ()

@end

@implementation ListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Init background
    UIImage* bg = [UIImage imageNamed:@"bg-app.png"];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:bg]];
    
    //Init services 
    self.services = [MangaListService getSampleData];
    
	//Init the grid view
    self.listView                       = [[AQGridView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
    self.listView.autoresizingMask      = UIViewAutoresizingFlexibleWidth 
                                            | UIViewAutoresizingFlexibleHeight;
    self.listView.autoresizesSubviews   = YES;
    self.listView.delegate              = self;
    self.listView.dataSource            = self;
    
    //Add to view
    [self.view addSubview:self.listView];
    [self.listView reloadData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//Return number of item in the grid
- (NSUInteger) numberOfItemsInGridView:(AQGridView *)gridView
{ 
    return [services count];
}

//Return cell at index
- (AQGridViewCell*) gridView:(AQGridView *)gridView cellForItemAtIndex:(NSUInteger)index
{
    //Hell is this?
    static NSString* PlainCellIdentifier = @"PlainCellIdentifier";
    
    //Find cell
    ListViewCell* cell = (ListViewCell*) [gridView dequeueReusableCellWithIdentifier:@"PlainCellIdentified"];
    if (cell == nil)
    {
        //Create one if doesn't find anything
        cell = [[ListViewCell alloc] initWithFrame:CGRectMake(0, 0, 320, 158) reuseIdentifier:PlainCellIdentifier];
    }
    
    //Get from service
    MangaListService* service = [services objectAtIndex:index];
    
    //Set image
    [cell.mangaImage setImage:service.mangaImage];
    [cell.mangaLabel setText:service.mangaTitle];
    
    return cell;
}

//Implement data source cell size
- (CGSize) portraitGridCellSizeForGridView:(AQGridView *)gridView
{
    return (CGSizeMake(320,158));
}

@synthesize listView;
@synthesize services;

@end
