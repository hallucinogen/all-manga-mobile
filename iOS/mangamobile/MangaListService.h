//
//  MangaListService.h
//  mangamobile
//
//  Created by rukanishino on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MangaListService : NSObject

@property (nonatomic, copy)     NSString*   mangaTitle;
@property (nonatomic, retain)   UIImage*    mangaImage;

//Custom constructor for creating model
-(id) initWithTitle:(NSString*)title andImage:(UIImage*)image;

//Create sample data
+(NSArray*) getSampleData;

@end
