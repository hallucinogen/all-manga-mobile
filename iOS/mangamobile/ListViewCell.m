//
//  ListViewCell.m
//  mangamobile
//
//  Created by rukanishino on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ListViewCell.h"

@implementation ListViewCell

- (id) initWithFrame:(CGRect)frame reuseIdentifier:(NSString *)reuseIdentifier
{
    //Parent constructor
    self = [super initWithFrame:frame reuseIdentifier:reuseIdentifier];
    
    //Assign value if success
    if (self)
    {
        //Create main view
        UIView* mainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 158)];
        [mainView setBackgroundColor:[UIColor clearColor]];
        
        //Create shelf image
        UIImageView* shelfImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,158)];
        [shelfImageView setImage:[UIImage imageNamed:@"shelf.png"]];
        
        //Create manga image
        self.mangaImage = [[UIImageView alloc] initWithFrame:CGRectMake(13, 8, 84, 129)];
        
        //Create manga label
        self.mangaLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 20, 200, 20)];
        [self.mangaLabel setFont:[UIFont systemFontOfSize:16]];
        [self.mangaLabel setBackgroundColor:[UIColor clearColor]];
        
        //Add to the view
        [mainView addSubview:shelfImageView];
        [mainView addSubview:self.mangaImage];
        [mainView addSubview:self.mangaLabel];
        
        //Add view to self
        [self.contentView addSubview:mainView];
    }
    
    return self;
}

@synthesize mangaImage;
@synthesize mangaLabel;

@end
