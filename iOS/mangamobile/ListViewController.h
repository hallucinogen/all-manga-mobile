//
//  ListViewController.h
//  mangamobile
//
//  Created by rukanishino on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQGridView.h"

@interface ListViewController : UIViewController <AQGridViewDelegate, AQGridViewDataSource>

@property (nonatomic, retain) IBOutlet AQGridView* listView;
@property (nonatomic, retain) NSArray* services;

@end
