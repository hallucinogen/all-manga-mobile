//
//  SearchViewController.m
//  mangamobile
//
//  Created by rukanishino on 3/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

@synthesize m_SearchField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Init background
    UIImage* bg = [UIImage imageNamed:@"bg-app.png"];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:bg]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//So the text field returned after "return" is pressed
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

//Called after the editing is stopped
- (void) textFieldDidEndEditing:(UITextField *)textField
{
    //Search manga if not empty
    if (![textField.text isEqual:@""])
    {
        NSLog(@"[Search] Manga to be searched: %@", textField.text);
    }
}

//Method for handling background touched
- (IBAction)backgroundTouched:(id)sender
{
    
    [m_SearchField resignFirstResponder];
}

@end
