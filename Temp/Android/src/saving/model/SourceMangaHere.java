package net.gogo.mobile.allmanga.saving.model;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class SourceMangaHere extends MangaSource {
	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String downloadImagePath(String pageURL) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("img[id=image]") == null || soup.select("img[id=image]").first() == null) continue;
				String imageSRC = soup.select("img[id=image]").first().attr("src");
				return imageSRC;
			} catch (IOException ex){
			}
		}
		
		return null;
	}

	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains("http://mangahere.com");
	}
}
