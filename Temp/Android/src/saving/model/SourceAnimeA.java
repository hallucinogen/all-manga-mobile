package net.gogo.mobile.allmanga.saving.model;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class SourceAnimeA extends MangaSource {
	private final static String TAG = "Source Good Manga";
	@Override
	public String downloadImagePath(String pageURL) {
		boolean gagal = true;
		while (gagal){
			try {

				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("img[class=mangaimg]") == null || soup.select("img[class=mangaimg]").first() == null) continue;
				String imageSRC = soup.select("img[class=mangaimg]").first().attr("src");
				return imageSRC;
			} catch (IOException ex){
			}
		}
		return null;
	}

	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(chapter.getFullURL()).get();
				Element selection = soup.select("select[name=page]").first();
				
				final String hurdle = ".html";
				
				String pageURLPredcessor = chapter.getFullURL();
				if (pageURLPredcessor.endsWith(hurdle)) {
					// remove .html at the end of page
					pageURLPredcessor = pageURLPredcessor.substring(0, pageURLPredcessor.length() - hurdle.length());
				}
				pageURLPredcessor += "-page-";

				if (selection == null) continue;
				Elements options = selection.getElementsByTag("option");
				gagal = false;
				int i = 0;
				for (Element option : options){
					// by javascript
					String push = pageURLPredcessor + option.attr("value") + ".html";
					chapter.mImageURLs.add(push);
					++i;
				}
			} catch (IOException ex){
			}
		}
	}

	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains(Utilities.ANIMEA_URL);
	}
}
