package net.gogo.mobile.allmanga.saving.model;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class SourceGoodManga extends MangaSource {
	private final static String TAG = "Source Good Manga";
	@Override
	public String downloadImagePath(String pageURL) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("img[id=manga_image]") == null || soup.select("img[id=manga_image]").first() == null) continue;
				String imageSRC = soup.select("img[id=manga_image]").first().attr("src");
				return imageSRC;
			} catch (IOException ex){
			}
		}
		return null;
	}

	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(chapter.getFullURL()).get();
				Element selection = soup.select("select[id=bottom_page_list]").first();

				if (selection == null) continue;
				Elements options = selection.getElementsByTag("option");
				gagal = false;
				int i = 0;
				for (Element option : options){
					String push = Utilities.GOODMANGA_URL + option.attr("value");
					chapter.mImageURLs.add(push);
					++i;
				}
			} catch (IOException ex){
			}
		}
	}

	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains(Utilities.GOODMANGA_URL) || url.contains(Utilities.GOODMANGA_URL_2);
	}
}
