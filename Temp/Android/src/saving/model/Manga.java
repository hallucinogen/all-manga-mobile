package net.gogo.mobile.allmanga.saving.model;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class Manga implements Comparable<Manga> {
	private static String TAG = "MANGA";
	
	public Manga(String title, String inURL){
		mTitle 			= title;
		url				= inURL;
		mChapters		= new ArrayList<Chapter>();
		mGenre			= new ArrayList<String>();
		mRecommended 	= new ArrayList<Manga>();
		mAuthor			= "";
		
		// make tags from title for search
		final String[] tags = mTitle.toLowerCase().split("(?!^)\\b");
		mTags	= new ArrayList<String>();
		
		for (int i = 0; i < tags.length; ++i) {
			mTags.add(tags[i]);
		}
	}

	// getter
	//public ArrayList<Chapter> getChapters()	{	return mChapters;		}
	public String getTitle()					{	return mTitle;			}
	public String getUrl() 						{	return url;				}
	public float getRating() 					{	return mRating;			}
	public ArrayList<String> getGenre() 		{	return mGenre;			}
	public ArrayList<String> getTags()			{	return mTags;			}
	public ArrayList<Manga> getRecommended()	{	return mRecommended;	}

	public String getBookmarkPreferenceString() {
		return mTitle + "_BOOKMARK";
	}

	public void setRating(float rating) 	{	mRating = rating;		}
	
	public void setUrl(String inURL) {
		url = inURL;
	}

	public void addChapter(String title, String url) {
		mChapters.add(new Chapter(url, this, title));
	}

	public void addGenre(String genre) {
		mGenre.add(genre);
	}
	
	public JSONObject toJSON() {
		JSONObject jsonResult = new JSONObject();

		try {
			jsonResult.put(JSON_TITLE, mTitle);
			jsonResult.put(JSON_URL, url);
			jsonResult.put(JSON_ARTIST, mAuthor != null ? mAuthor : "");
			jsonResult.put(JSON_COVER_IMAGE_PATH, mCoverImagePath);
			jsonResult.put(JSON_RATING, mRating);
			JSONArray array = new JSONArray();
			for (int i = 0, n = mChapters.size(); i < n; ++i) {
				array.put(mChapters.get(i).toJSON());
			}
			jsonResult.put(JSON_CHAPTERS, array);
			
			// put genre
			if (mGenre != null && mGenre.size() > 0) {
				array = new JSONArray();
				for (int i = 0, n = mGenre.size(); i < n; ++i) {
					array.put(mGenre.get(i));
				}
				jsonResult.put(JSON_GENRE, array);
			}

			// add recommendation
			if (mRecommended != null && mRecommended.size() > 0) {
				array = new JSONArray();
				for (int i = 0, n = mRecommended.size(); i < n; ++i) {
					array.put(mRecommended.get(i).toJSON());
				}
				jsonResult.put(JSON_RECOMMENDED, array);
			}
		} catch (JSONException jEx) {
			
		}
		return jsonResult;
	}
	
	@Override
	public int compareTo(Manga o) {
		return mTitle.compareToIgnoreCase(o.mTitle);
	}

	private String url;
	String mCoverImagePath;
	private String mTitle;
	private String mAuthor, mStatusAtOrigin, mStatusScanlation;
	private ArrayList<String> mTags;
	private transient ArrayList<Manga> mRecommended;
	private float mRating;
	ArrayList<String> mGenre;
	ArrayList<Chapter> mChapters;
	

	public static final String JSON_TITLE = "title";
	public static final String JSON_URL = "url";
	public static final String JSON_CHAPTERS = "chapters";
	public static final String JSON_DESCRIPTION = "description";
	public static final String JSON_COVER_IMAGE_PATH = "coverImagePath";
	public static final String JSON_RATING = "rating";
	public static final String JSON_GENRE = "genre";
	public static final String JSON_ARTIST = "artist";
	public static final String JSON_RECOMMENDED = "recommended";
}
