package net.gogo.mobile.allmanga.saving.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Chapter {
	public Chapter(String rawURL, Manga manga, String chapterTitle){
		mImageURLs 			= new ArrayList<String>();
		mManga 				= manga;
		mTitle				= chapterTitle;
		
		mFullURL = rawURL;
		
		mJSONCalculated = false;
	}
	
	public void downloadAllPageURL() {
		mImageURLs.clear();
		//ArrayList<String> realImageURL = new ArrayList<String>();
		MangaSourceManager.allInstance().downloadImageURLs(this);
		/*for (int i = 0; i < mImageURLs.size(); ++i) {
			String imageURL = downloadImage(i);
			realImageURL.add(imageURL);
		}
		
		mImageURLs = realImageURL;*/
	}

	public String downloadImage(int index) {
		String image = MangaSourceManager.allInstance().downloadImagePath(this, index);
		return image;
	}
	
	public String downloadImage(String pageURL) {
		return MangaSourceManager.allInstance().downloadImagePath(pageURL);
	}
	
	public int getPageCount() {
		return mImageURLs.size();
	}
	
	public String getFullURL(){
		return mFullURL;
	}
	
	public String getTitle(){
		return mTitle;
	}
	
	public Manga getManga(){
		return mManga;
	}

	public String getBookmarkPreferenceString() {
		return mManga.getTitle() + "_" + mTitle + "_BOOKMARK";
	}
	
	public JSONObject toJSON() {
		if (!mJSONCalculated) {
			mJSONResult = new JSONObject();
			
			try {
				mJSONResult.put(JSON_TITLE, mTitle);
				mJSONResult.put(JSON_URL, mFullURL);
				JSONArray array = new JSONArray();
				for (int i = 0, n = mImageURLs.size(); i < n; ++i) {
					array.put(mImageURLs.get(i));
				}
				mJSONResult.put(JSON_IMAGE_URLS, array);
				mJSONCalculated = true;
			} catch (JSONException jEx) {
				
			}
		}
		return mJSONResult;
	}
	
	private boolean mJSONCalculated;
	private JSONObject mJSONResult;
	ArrayList<String> mImageURLs;
	private String mFullURL;
	private Manga mManga;
	private String mTitle;
	
	public static final String JSON_TITLE = "title";
	public static final String JSON_URL = "url";
	public static final String JSON_IMAGE_URLS = "imageURLs";
}
