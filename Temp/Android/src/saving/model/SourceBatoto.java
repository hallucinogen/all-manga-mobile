package net.gogo.mobile.allmanga.saving.model;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

// TODO: test this
public class SourceBatoto extends MangaSource {
	private final static String TAG = "Batoto Manga Source";

	@Override
	public String downloadImagePath(String pageURL) {
		boolean gagal = true;
		while (gagal){
			try {

				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("div[id=full_image]") == null || soup.select("div[id=full_image]").select("img") == null || soup.select("div[id=full_image]").select("img").first() == null) continue;
				String imageSRC = soup.select("div[id=full_image]").select("img").first().attr("src");
				return imageSRC;
			} catch (IOException ex){
			}
		}
		return null;
	}
	
	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(chapter.getFullURL()).get();
				Element selection = soup.select("select[id=page_select]").first();

				if (selection == null) continue;
				Elements options = selection.getElementsByTag("option");
				gagal = false;
				int i = 0;
				for (Element option : options){
					String push = option.attr("value");
					chapter.mImageURLs.add(push);
					++i;
				}
			} catch (IOException ex){
			}
		}
	}
	
	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains(Utilities.BATOTO_URL) || url.contains(Utilities.BATOTO_URL_2);
	}
}
