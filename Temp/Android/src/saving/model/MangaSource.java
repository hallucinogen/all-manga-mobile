package net.gogo.mobile.allmanga.saving.model;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class MangaSource {
	public MangaSource() {
		
	}
	
	// functions to be implemented by each manga source
	public abstract void downloadChapterImageURLs(Chapter chapter);
	public abstract String downloadImagePath(String pageURL);
	public abstract boolean isUsingTheMangaSource(String url);

	public String downloadImagePath(Chapter chapter, int index) {
		return downloadImagePath(chapter.mImageURLs.get(index));
	}
}
