package net.gogo.mobile.allmanga.saving.model;

public class Utilities {


	public static final int DOWNLOAD_ATTEMPT	= 5;
	public static final String MANGAREADER_URL	= "http://mangareader.net";
	public static final String MANGAREADER_URL_2= "http://www.mangareader.net";
	public static final String MANGAPANDA_URL	= "http://mangapanda.com";
	public static final String MANGAPANDA_URL_2	= "http://www.mangapanda.com";
	public static final String GOODMANGA_URL	= "http://goodmanga.net";
	public static final String GOODMANGA_URL_2	= "http://www.goodmanga.net";
	public static final String BATOTO_URL		= "http://batoto.net";
	public static final String BATOTO_URL_2		= "http://www.batoto.net";
	public static final String ANIMEA_URL		= "http://manga.animea.net";
	//public static final String RATING_QUERY		= "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=mangaupdates+";
	public static final String[] MANGAUPDATES_QUERY	= new String[] {
		"http://www.mangaupdates.com/series.html?stype=title&type=manga&search=",
		"http://www.mangaupdates.com/series.html?stype=title&type=manhwa&search="
	};
	public static final String RATING_STRING		= "Bayesian Average:";
	public static final String RECOMEMEND_STRING	= "Recommendations";
	
}
