package net.gogo.mobile.allmanga.saving.model;

import java.util.ArrayList;

import android.util.Log;

public class MangaSourceManager {
	private MangaSourceManager() {
		mMangaSources = new ArrayList<MangaSource>();
	}

	public ArrayList<MangaSource> getMangaSources() {
		return mMangaSources;
	}

	public void addMangaSource(MangaSource source) {
		mMangaSources.add(source);
	}

	public void removeMangaSource(MangaSource source) {
		mMangaSources.remove(source);
	}

	public void clearMangaSource() {
		mMangaSources.clear();
	}

	public String downloadImagePath(Chapter chapter, int index) {
		for (int i = 0, n = mMangaSources.size(); i < n; ++i) {
			MangaSource mangaSource = mMangaSources.get(i);
			try {
				if (mangaSource.isUsingTheMangaSource(chapter.getFullURL())) {
					return mangaSource.downloadImagePath(chapter, index);
				}
			} catch (OutOfMemoryError oom) {
				System.gc();
				Log.e("MangaSourceManager", "Out of memory error!");
			}
		}
		return null;
	}
	
	public String downloadImagePath(String pageURL) {
		for (int i = 0, n = mMangaSources.size(); i < n; ++i) {
			MangaSource mangaSource = mMangaSources.get(i);
			if (mangaSource.isUsingTheMangaSource(pageURL)) {
				return mangaSource.downloadImagePath(pageURL);
			}
		}
		return null;
	}

	public void downloadImageURLs(Chapter chapter) {
		for (int i = 0, n = mMangaSources.size(); i < n; ++i) {
			MangaSource mangaSource = mMangaSources.get(i);
			if (mangaSource.isUsingTheMangaSource(chapter.getFullURL())) {
				mangaSource.downloadChapterImageURLs(chapter);
				return;
			}
		}
	}

	// singleton
	public static MangaSourceManager instance() {
		if (sInstance == null)
			sInstance = new MangaSourceManager();
		return sInstance;
	}

	// return instance which contains all manga source
	public static MangaSourceManager allInstance() {
		if (sAllInstance == null) {
			sAllInstance = new MangaSourceManager();
			sAllInstance.addMangaSource(new SourceGoodManga());
			sAllInstance.addMangaSource(new SourceMangaReader());
			sAllInstance.addMangaSource(new SourceBatoto());
			sAllInstance.addMangaSource(new SourceAnimeA());
			sAllInstance.addMangaSource(new SourceMangaHere());
			sAllInstance.addMangaSource(new SourceMangaPanda());
		}
		return sAllInstance;
	}

	private ArrayList<MangaSource> mMangaSources;
	private static MangaSourceManager sInstance, sAllInstance;
}
