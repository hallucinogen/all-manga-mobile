package net.gogo.mobile.allmanga.saving.model;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SourceMangaPanda extends MangaSource {
	private final static String TAG = "Source Manga Panda";
	@Override
	public String downloadImagePath(String pageURL) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("img[name=img]") == null || soup.select("img[name=img]").first() == null) continue;
				String imageSRC = soup.select("img[name=img]").first().attr("src");
				return imageSRC;
			} catch (IOException ex){
			}
		}
		return null;
	}

	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		boolean gagal = true;
		while (gagal){
			try {
				System.out.println(chapter.getFullURL());
				Document soup = Jsoup.connect(chapter.getFullURL()).get();
				Element selection = soup.select("select[id=pageMenu]").first();

				if (selection == null) continue;
				Elements options = selection.getElementsByTag("option");
				gagal = false;
				int i = 0;
				for (Element option : options){
					String push = Utilities.MANGAPANDA_URL + option.attr("value");
					chapter.mImageURLs.add(push);
					++i;
				}
			} catch (IOException ex){
			}
		}
	}

	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains(Utilities.MANGAPANDA_URL) || url.contains(Utilities.MANGAPANDA_URL_2);
	}
}
