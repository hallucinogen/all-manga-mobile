package net.gogo.mobile.allmanga;

import net.gogo.mobile.allmangalib.R;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import net.gogo.mobile.allmanga.ReadAct.BatchPageDownloaderTask;
import net.gogo.mobile.allmanga.helper.AlphanumComparator;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Chapter;
import net.gogo.mobile.allmanga.model.Manga;
import net.gogo.mobile.allmanga.model.Chapter.LocalImageStrategyResolver;

import com.actionbarsherlock.view.MenuItem;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

/**
 * @author Hallucinogen
 */
public class OfflineReadAct extends ReadAct {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final Intent intent = getIntent();
	    final String action = intent.getAction();
	    final String type = intent.getType();
	    
	    // Yoda style!
		if (Intent.ACTION_VIEW.equals(action) && type != null) {
	        if (type.startsWith("image/")) {
	        	Uri imageUri = intent.getData();

        		File file = new File(Uri.decode(imageUri.getEncodedSchemeSpecificPart()));
        		File parent = new File(file.getParent());
        		File grandParent = new File(parent.getParent());
        		String deducedMangaName = grandParent.exists() ? grandParent.getName() : "";
        		String deducedURL = grandParent.exists() ? grandParent.getAbsolutePath() : "";
        		mSelectedPagePath = file.getAbsolutePath();

	        	// start making chapter
	        	mChapter = new Chapter(parent.getAbsolutePath(), new Manga(deducedMangaName, deducedURL), parent.getName());

	        	// try to download the chapter data
				downloadChapterImageList();

				// get current chapter number
				mActionBar.setTitle(String.format("%s (%d / %d)",
						mChapter.getTitle(),
						mChapter.getSelection() + 1,
						mChapter.getPageCount()));
	        }
	    } else {
	    	Toast.makeText(OfflineReadAct.this, R.string.random_error, Toast.LENGTH_SHORT).show();
	    	finish();
	    }
	}
	
	@Override
	protected void downloadChapterImageList() {
		new Thread() {
			public void run() {
				// instead of downloading, we resolve each page according to file name
				{
					File chapterFile = new File(mChapter.getURL());
					File[] children = chapterFile.listFiles(new FileFilter() {
						@Override
						public boolean accept(File file) {
							final String fileName = file.getName();
	
							return (fileName.matches("([^\\s]+(\\.(?i)(jpg|png|gif|bmp|amm))$)"));
						}
					});
					ArrayList<String> sortedChildren = new ArrayList<String>();
					for (int i = 0; i < children.length; ++i) {
						sortedChildren.add(children[i].getAbsolutePath());
					}
	
					// sort file name by id
					// TODO make a strategy to get page pattern and apply it on Chapter.goToPage
					Collections.sort(sortedChildren, new AlphanumComparator());
	
					// prepare!! And find current selected page whereabout!
					int idx = 0;
					for (int i = 0; i < children.length; ++i) {
						final String fileName = sortedChildren.get(i);
						mChapter.addChapterURL(fileName);
						if (fileName.equals(mSelectedPagePath)) {
							idx = i;
						}
					}
	
					for (int i = 0; i < children.length; ++i) {
						mChapter.resolvePage(i, sortedChildren.get(i));
					}
					mChapter.goToPage(idx);
				}

				// add strategy
				mChapter.setStrategy(new LocalImageStrategyResolver() {
					@Override
					public String getLocalImage(int index) {
						return mChapter.getResolvedPage(index);
					}
				});

				{
					final Manga manga = mChapter.getManga();
					final String mangaFolder = manga.getURL();
					final String chapterFolder = mChapter.getURL();
					File[] chaptersInManga = new File(mangaFolder).listFiles();
					ArrayList<String> fileNames = new ArrayList<String>();

					for (int i = 0; i < chaptersInManga.length; ++i) {
						fileNames.add(chaptersInManga[i].getAbsolutePath());
					}
					// do super sort!
					Collections.sort(fileNames, new AlphanumComparator());

					for (String fileName: fileNames) {
						manga.addChapter(new Chapter(fileName, manga, new File(fileName).getName()));
					}
				}

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// download image urls fail. We should let the user retry
						if (mChapter.getPageCount() == 0) {
							Toast.makeText(OfflineReadAct.this, R.string.random_error, Toast.LENGTH_SHORT).show();
						}

						startViewAndBatchDownloader();
					}
				});
			}
		}.start();
	}
	
	@Override
	protected void goToNextChapter() {
		jumpOneChapter(1);
	}

	@Override
	protected void goToPrevChapter() {
		jumpOneChapter(-1);
	}
	
	private void jumpOneChapter(int direction) {
		final Manga manga = mChapter.getManga();
		final ArrayList<Chapter> chapters = manga.getChapters();
		int idx = -1;
		int chapterLimitation = direction > 0 ? chapters.size() - 1 : 0;
		for (int i = 0, n = chapters.size(); i < n; ++i) {
			if (mChapter.getURL().equals(chapters.get(i).getURL())) {
				idx = i;
				break;
			}
		}

		if (idx == -1) {
			// folder not found (or manga file doesn't exist). Output error
			Toast.makeText(OfflineReadAct.this, R.string.random_error, Toast.LENGTH_SHORT).show();
			finish();
		} else {
			if (idx == chapterLimitation) {
				// if no chapter ahead, output no more chapter
				Toast.makeText(OfflineReadAct.this, R.string.no_more_chapter, Toast.LENGTH_SHORT).show();
				finish();
			} else {
				// go to next faking chapter
				String nextChapterFolder = chapters.get(idx + direction).getURL();
				File[] children = new File(nextChapterFolder).listFiles(new FileFilter() {
					@Override
					public boolean accept(File file) {
						final String fileName = file.getName();
						
						return (fileName.matches("([^\\s]+(\\.(?i)(jpg|png|gif|bmp|amm))$)"));
					}
				});
				if (children != null && children.length == 0) {
					Toast.makeText(OfflineReadAct.this, R.string.no_more_chapter, Toast.LENGTH_SHORT).show();
					finish();
				} else {
					Intent intent = new Intent(getApplicationContext(), OfflineReadAct.class);
					intent.setAction(Intent.ACTION_VIEW);
					// set random image type so long it works
					intent.setDataAndType(Uri.fromFile(children[0]), "image/png");

					startActivity(intent);

					finish();
				}
			}
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		final int itemId = item.getItemId();
		Intent intent = null;
		
		if (itemId == android.R.id.home) {
			finish();
		} else if (itemId == R.id.setting) {
			intent = new Intent(this, SettingAct.class);
			startActivity(intent);
		} else if (itemId == R.id.refresh) {
			mReaderImageView.setImage(BitmapFactory.decodeFile(mChapter.getChapterDirectoryPath() + "/" + mChapter.getSelection() + ".png"));
		}

		return true;
	}
	
	private String mSelectedPagePath;
}
