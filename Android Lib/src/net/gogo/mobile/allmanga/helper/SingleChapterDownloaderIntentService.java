package net.gogo.mobile.allmanga.helper;

import java.io.File;

import net.gogo.mobile.allmanga.model.Chapter;
import net.gogo.mobile.allmanga.notification.MangaMobileNotification;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

public class SingleChapterDownloaderIntentService extends CancelableIntentService {
	private final static String TAG = SingleChapterDownloaderIntentService.class.getSimpleName();
	
	public SingleChapterDownloaderIntentService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		try {
			final Chapter chapter = new Chapter(new JSONObject(intent.getStringExtra(Utilities.INTENT_CHAPTER)));
			MangaMobileNotification.makeDownloadChapterNotification(this, 0, chapter);
			chapter.downloadImageURLs(false);
			new File(chapter.getChapterDirectoryPath()).mkdirs();

			for (int i = 0, m = chapter.getPageCount(); i < m; ++i) {
				// start batch downloader, retry 3 times to make sure every single page is downloaded
				for (int retry = 0; retry < 3; ++retry) {
					// only download if it should be downloaded
	
					final String cacheURL = chapter.getLocalImagePath(i);
					// let us check whether the file is already downloaded or are there any broken file
					if (new File(cacheURL).length() < IOUtilities.FILE_MINIMAL_SIZE) {
						// assume the file will never corrupt
						final String url = chapter.resolvePage(i);
	
						IOUtilities.downloadImageAndSave(url, cacheURL);
					}
				}
				final int progress = 100 * (i + 1) / m;
				MangaMobileNotification.makeDownloadChapterNotification(this, progress, chapter);
			}
			SharedPreferences chapterPref = getSharedPreferences(Utilities.CHAPTER_PREFERENCES, 0);
			MangaMobileStorageStub.saveChapterToPreferences(chapterPref, chapter);
			MangaMobileNotification.makeDownloadChapterNotification(this, 100, chapter);
		} catch (JSONException jEx) {
			Log.e(TAG, jEx.toString());
		}	
	}

	@Override
	protected void onCanceledIntent(Intent intent) {
		// TODO Auto-generated method stub
		
	}
}
