package net.gogo.mobile.allmanga.helper;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.gogo.mobile.allmanga.model.Achievement;
import net.gogo.mobile.allmanga.model.Chapter;
import net.gogo.mobile.allmanga.model.Manga;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.SQLException;
import android.os.AsyncTask;
import android.util.Log;

// this class is used to save things to preferences (database)
// what's good about this class is it seperates database logic with main logic
// and it commits the database asynchronously, thus improving UI response
public class MangaMobileStorageStub {
	private final static String TAG = MangaMobileStorageStub.class.getSimpleName();
	public static void saveMangaToReadList(final Context context, final Manga manga) {
		new Thread() {
			@Override
			public void run() {
				System.gc();
				MangaDatabaseAdapter dbAdapter = MangaDatabaseAdapter.instance(context);;
				boolean locked = true;
				boolean reported = false;
				while (locked) {
					try {
						//dbAdapter.open();
						locked = false;
						dbAdapter.insertOrUpdateManga(manga);
						//dbAdapter.close();
					} catch (SQLException sEx) {
						if (!reported) {
							reported = true;
						}
					}
				}

			}
		}.start();
	}
	
	public static void saveChapterToPreferences(final SharedPreferences pref, final Chapter chapter) {
		new Thread() {
			@Override
			public void run() {
				System.gc();
				SharedPreferences.Editor editor = pref.edit();
				
				// guard
				if (chapter == null) return;
				
				editor.putString(chapter.getURL(), chapter.toJSON().toString());
				editor.commit();
			}
		}.start();
	}
	
	public static void addChapterScoreInPreference(final Context context) {
		new Thread() {
			public void run() {
				System.gc();
				SharedPreferences pref = context.getSharedPreferences(Utilities.PREFERENCES, 0);
				Achievement achievement = new Achievement(pref.getString(Utilities.ACHIEVEMENT, ""));
				//Log.e(TAG, "Achievement json = " + pref.getString(Utilities.ACHIEVEMENT, "") + "Achievement score " + achievement.toJSON().toString());
				achievement.incrementChapterRead();

				Editor editor = pref.edit();
				
				//Log.e(TAG, "Achievement score " + achievement.toJSON().toString());
				
				editor.putString(Utilities.ACHIEVEMENT, achievement.toJSON().toString());
				editor.commit();
			}
		}.start();
	}
	
	public static void addMangaScoreInPreference(final Context context) {
		new Thread() {
			public void run() {
				System.gc();
				SharedPreferences pref = context.getSharedPreferences(Utilities.PREFERENCES, 0);
				Achievement achievement = new Achievement(pref.getString(Utilities.ACHIEVEMENT, ""));
				achievement.incrementMangaRead();
				
				Editor editor = pref.edit();
				
				editor.putString(Utilities.ACHIEVEMENT, achievement.toJSON().toString());
				editor.commit();
			}
		}.start();
	}
	
	public static void addAdsScoreInPreference(final Context context) {
		new Thread() {
			public void run() {
				System.gc();
				SharedPreferences pref = context.getSharedPreferences(Utilities.PREFERENCES, 0);
				Achievement achievement = new Achievement(pref.getString(Utilities.ACHIEVEMENT, ""));
				achievement.incrementAdsPressed();
				
				Editor editor = pref.edit();
				
				editor.putString(Utilities.ACHIEVEMENT, achievement.toJSON().toString());
				editor.commit();
			}
		}.start();
	}

	// secretely change ads policy
	// TODO: maybe it is too hacky, let's design a better one
	public static void changeAdsPolicy(final Context context) {
		if (ADS_CHANGED) {
			SharedPreferences.Editor editor = context.getSharedPreferences(Utilities.PREFERENCES, 0).edit();
			editor.putBoolean(Utilities.ADS_ENABLED, ADS_ENABLED);
			editor.commit();
		}
	}
	
	public static void removeMangaFromReadList(final Context context, final Manga manga) {
		new Thread() {
			@Override
			public void run() {
				System.gc();
				MangaDatabaseAdapter dbAdapter = MangaDatabaseAdapter.instance(context);;
				boolean locked = true;
				boolean reported = false;

				while (locked) {
					try {
						//dbAdapter.open();
						locked = false;
						dbAdapter.deleteManga(manga);
						//dbAdapter.close();
					} catch (SQLException sEx) {
						if (!reported) {
							reported = true;
						}
					}
				}
			}
		}.start();
	}
	
	/**
	 * Legacy readlist saving method (from preferences)
	 * @param pref the shared preferences
	 * @return read list manga
	 */
	@Deprecated
	public static ArrayList<Manga> getReadListFromPreferences(SharedPreferences pref) {
		String readListJSONFormated = pref.getString(Utilities.READ_LIST_DATABASE, null);
		ArrayList<Manga> mangas = new ArrayList<Manga>();

		try {
			if (readListJSONFormated != null) {
				System.gc();
				JSONObject jsonFav = new JSONObject(readListJSONFormated);
				JSONArray array = jsonFav.names();

				for (int i = 0; i < jsonFav.length(); ++i) {
					// get manga JSON from here
					JSONObject mangaJSON = jsonFav.getJSONObject(array.getString(i));
					mangas.add(new Manga(mangaJSON));
				}

				Collections.sort(mangas);
			}
		} catch (Exception ex) {
		}

		return mangas;
	}
	
	public static ArrayList<Manga> getReadListFromStorage(Context context) {
		MangaDatabaseAdapter dbAdapter = MangaDatabaseAdapter.instance(context);
		//dbAdapter.open();
		ArrayList<Manga> retval = dbAdapter.getAllMangas(false);
		//dbAdapter.close();
		
		return retval;
	}
	
	public static int getUsedReadLimit(Context context) {
		ArrayList<Manga> mangas = getReadListFromStorage(context);
		
		int retval = 0;
		for (int i = 0, n = mangas.size(); i < n ; ++i) {
			if (mangas.get(i).getURL().startsWith("http")) {
				++retval;
			}
		}
		
		return retval;
	}
	
	public static Manga getDetailedManga(Context context, String url) {
		MangaDatabaseAdapter dbAdapter = MangaDatabaseAdapter.instance(context);;
		//dbAdapter.open();
		Manga manga = dbAdapter.getManga(url);
		//dbAdapter.close();

		return manga;
	}

	public static boolean isMangaBookmarked(Context context, Manga manga) {
		MangaDatabaseAdapter dbAdapter = MangaDatabaseAdapter.instance(context);;
		Manga mangaFromDB = null;
		try {
			//dbAdapter.open();
			mangaFromDB = dbAdapter.getManga(manga.getURL());
			//dbAdapter.close();
		} catch (SQLException sEx) {
		}

		return mangaFromDB != null;
	}
	
	public static void backupReadListToFile(SharedPreferences pref) {
		int databaseVersion = pref.getInt(Utilities.DATABASE_VERSION_PREF, 0);

		// if version is actually the same / greater, neglect
		if (databaseVersion >= Utilities.DATABASE_VERSION) return;

		Log.e(TAG, "Is backing up data from version " + databaseVersion);

		ArrayList<Manga> allMangaFromPreferences = getReadListFromPreferences(pref);

		// get all manga from old database and from backup file
		final String BACKUP_FILE = Utilities.BASE_DIRECTORY + "/" + Utilities.READ_LIST_DATABASE;
		String allBackup = IOUtilities.readFileAsString(BACKUP_FILE);
		try {
			JSONObject json = null;
			try {
				json = allBackup != null ? new JSONObject(allBackup) : new JSONObject();
			} catch (JSONException jEx) {
				json = new JSONObject();
			}

			Log.e(TAG, "preference size " + allMangaFromPreferences.size());
			
			for (int i = 0, n = allMangaFromPreferences.size(); i < n; ++i) {
				final Manga manga = allMangaFromPreferences.get(i);

				json.put(manga.getURL(), manga.toJSON());
			}

			// put all manga to new database
			IOUtilities.writeToFileAsString(BACKUP_FILE, json.toString());

			// save current version in preferences
			Editor editor = pref.edit();
			editor.putInt(Utilities.DATABASE_VERSION_PREF, Utilities.DATABASE_VERSION);
			// empty pref
			editor.putString(Utilities.READ_LIST_DATABASE, "");
			editor.commit();
		} catch (JSONException jEx) {
			Log.e(TAG, "Some error occured " + jEx);
		}
	
	}

	public static class CheckBookmarkTask extends AsyncTask<Void, Void, Void> {
		public CheckBookmarkTask(Context context, Manga manga) {
			mContext = context;
			mManga = manga;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			System.gc();
			mBookmarked = isMangaBookmarked(mContext, mManga);
			return null;
		}

		public boolean isBookmared() 	{	return mBookmarked;	}
		
		protected Context mContext;
		protected boolean mBookmarked;
		protected Manga mManga;
	}

	// ads stuff
	public static boolean ADS_ENABLED;
	public static boolean ADS_CHANGED;

	static {
		ADS_ENABLED = false;
		ADS_CHANGED = false;
	}
}
