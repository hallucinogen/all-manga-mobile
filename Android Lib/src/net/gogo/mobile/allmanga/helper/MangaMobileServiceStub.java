package net.gogo.mobile.allmanga.helper;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;

import net.gogo.mobile.allmanga.model.Chapter;
import net.gogo.mobile.allmanga.model.Manga;

public class MangaMobileServiceStub {
	private final static String TAG = MangaMobileServiceStub.class.getSimpleName();

	public static ArrayList<Manga> searchManga(String title,
			ArrayList<String> include,
			ArrayList<String> exclude,
			ArrayList<String> sources) {
		ArrayList<Manga> mangas = new ArrayList<Manga>();

		try {
			// make include string request
			String includeString = "";
			if (include != null && include.size() > 0) {
				StringBuilder sb = new StringBuilder();
				sb.append(include.get(0));
				for (int i = 1, n = include.size(); i < n; ++i) {
					sb.append("_");
					sb.append(include.get(i));
				}
				includeString = sb.toString();
			}

			// make exclude string request
			String excludeString = "";
			if (exclude != null && exclude.size() > 0) {
				StringBuilder sb = new StringBuilder();
				sb.append(exclude.get(0));
				for (int i = 1, n = exclude.size(); i < n; ++i) {
					sb.append("_");
					sb.append(exclude.get(i));
				}
				excludeString = sb.toString();
			}
			
			// make sources string request
			String sourcesString = "";
			if (sources != null && sources.size() > 0) {
				StringBuilder sb = new StringBuilder();
				sb.append(sources.get(0));
				for (int i = 1, n = sources.size(); i < n; ++i) {
					sb.append("___");
					sb.append(sources.get(i));
				}
				sourcesString = sb.toString();
			}

			// make request url
			final String requestURL = getSearchUrl() + "?"
					+ (title.trim().equals("") ? "" : "title=" + URLEncoder.encode(title, "UTF-8"))
					+ (includeString.trim().equals("") ? "" : "&genre=" + URLEncoder.encode(includeString, "UTF-8"))
					+ (excludeString.trim().equals("") ? "" : "&exclude=" + URLEncoder.encode(excludeString, "UTF-8"))
					+ (sourcesString.trim().equals("") ? "" : "&source=" + URLEncoder.encode(sourcesString, "UTF-8"));

			// get JSONObject from requesting the url
			JSONObject json = new JSONObject(RestClient.get(requestURL));

			// get results set
			JSONArray array = json.getJSONArray("results");
			
			// for each result, make manga model
			for (int i = 0, n = array.length(); i < n; ++i) {
				JSONObject mangaJSON = array.getJSONObject(i);
				
				mangas.add(new Manga(mangaJSON));
			}
		} catch (JSONException jEx) {
			Log.e(TAG, "Error while decoding JSON " + jEx.toString());
		} catch (IOException iEx) {
			Log.e(TAG, "Error while downloading");
		}

		return mangas;
	}
	
	/**
	 * Download manga metadata including chapter list
	 * Make sure that the chapter downloaded is sorted ascending
	 * 
	 * We also secretly add server-triggered-ads here
	 * @param manga
	 */
	public static void downloadMangaMetadata(Manga manga) {
		try {
			String[] strippedURL = manga.getURL().split("#");
			String language = "english";
			if (strippedURL.length > 1) {
				language = strippedURL[1];
			}
			// make request url
			final String requestURL = String.format("%s?title=%s&lang=%s&url=%s",
					getMangaMetadataUrl(),
					URLEncoder.encode(manga.getTitle(), "UTF-8"),
					URLEncoder.encode(language, "UTF-8"),
					URLEncoder.encode(manga.getURL(), "UTF-8"));
			//Log.i(TAG, "request url = " + requestURL);

			// get JSONObject from requesting the url
			JSONObject json = new JSONObject(RestClient.get(requestURL));
			Manga newManga = new Manga(json);

			// persist all metadata
			manga.setCoverImagePath(newManga.getCoverImagePath());
			manga.setDescription(newManga.getDescription());
			manga.setRating(newManga.getRating());
			manga.setArtist(newManga.getArtist());

			// persist chapter
			// clear existing chapter first
			manga.getChapters().clear();
			for (int i = 0, n = newManga.getChapters().size(); i < n; ++i) {
				manga.addChapter(newManga.getChapters().get(i));
			}

			// persist genre
			// clear existing genre first
			manga.getGenre().clear();
			for (int i = 0, n = newManga.getGenre().size(); i < n; ++i) {
				manga.addGenre(newManga.getGenre().get(i));
			}

			// persist recommendation
			manga.getRecommendation().clear();
			for (int i = 0, n = newManga.getRecommendation().size(); i < n; ++i) {
				manga.addRecommendation(newManga.getRecommendation().get(i));
			}
			// the secret! We control ads here!
			if (json.has(Utilities.ADS_ENABLED)) {
				MangaMobileStorageStub.ADS_CHANGED = true;
				MangaMobileStorageStub.ADS_ENABLED = json.getBoolean(Utilities.ADS_ENABLED);
			}
		} catch (JSONException jEx) {
			Log.e(TAG, "Error while decoding JSON");
		} catch (IOException iEx) {
			Log.e(TAG, "Error while downloading");
		}
	}

	public static void downloadChapterMetadata(Chapter chapter) {
		try {
			// get JSONObject from requesting the url
			final String requestURL = getChapterMetadataUrl() + "?url=" + URLEncoder.encode(chapter.getURL(), "UTF-8");
			JSONObject json = new JSONObject(RestClient.get(requestURL));
			// use saving mode
			/*net.gogo.mobile.allmanga.saving.model.Chapter savingChapter = 
					new net.gogo.mobile.allmanga.saving.model.Chapter(chapter.getURL(), null, chapter.getTitle());
			savingChapter.downloadAllPageURL();
			JSONObject json = savingChapter.toJSON();*/

			// persist imageURLS
			JSONArray imageArray = json.getJSONArray("imageURLs");
			for (int i = 0, n = imageArray.length(); i < n; ++i) {
				chapter.addChapterURL(imageArray.getString(i));
			}
		} catch (JSONException jEx) {
			Log.e(TAG, "Error while decoding JSON");
		} catch (IOException iEx) {
			Log.e(TAG, "Error while downloading");
		}
	}
	
	public static String downloadImageURLByPageURL(String pageURL) {
		try {
			// make request url
			final String requestURL = getPageResolveUrl() + "?page_url=" + URLEncoder.encode(pageURL, "UTF-8");
			Log.i(TAG, "request url = " + requestURL);
			
			return RestClient.get(requestURL);
		} catch (IOException iEx) {
			Log.e(TAG, "Error while downloading");
		}

		/*net.gogo.mobile.allmanga.saving.model.Chapter savingChapter = 
				new net.gogo.mobile.allmanga.saving.model.Chapter(pageURL, null, "");*/

		//return savingChapter.downloadImage(pageURL);
		
		return null;
	}

	public static void initializeServiceStub() {
		shuffleService();

		Log.i(TAG, "Connected to " + SERVICE_URL );
	}

	private static void shuffleService() {
		final int server = (int)Math.abs(Math.random() * 100.0f) % 15;
		String serverInfix = server == 0 || server > MIRROR_COUNT? "" : Integer.toString(server);

		SERVICE_URL	= "http://hallucinogenplus" + serverInfix + ".appspot.com";
	}

	// shuffle to make sure 90% of load goes to main server
	private static void shuffleServiceExtreme() {
		final int server = (int)Math.abs(Math.random() * 1000.0f) % 900;
		String serverInfix = server == 0 || server > MIRROR_COUNT ? "" : Integer.toString(server);

		SERVICE_URL	= "http://hallucinogenplus" + serverInfix + ".appspot.com";
	}
		
	// always go to main site
	private static String getSearchUrl() {
		//shuffleServiceExtreme();
		return MAIN_SERVICE + "/search";
	}
	
	// always go to main site
	private static String getMangaMetadataUrl() {
		shuffleServiceExtreme();
		return SERVICE_URL + "/manga_metadata";
	}
	
	private static String getChapterMetadataUrl() {
		shuffleService();
		return SERVICE_URL + "/chapter_metadata";
	}
	
	private static String getPageResolveUrl() {
		shuffleService();
		return SERVICE_URL + "/image_resolver";
	}

	private static String MAIN_SERVICE	= "http://hallucinogenplus.appspot.com";
	private static String SERVICE_URL	= "http://hallucinogenplus.appspot.com";
	private static int MIRROR_COUNT		= 9;
}
