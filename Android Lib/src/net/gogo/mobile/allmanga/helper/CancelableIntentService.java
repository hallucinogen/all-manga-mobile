package net.gogo.mobile.allmanga.helper;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public abstract class CancelableIntentService extends Service {
	private volatile Looper mServiceLooper;
	private volatile ServiceHandler mServiceHandler;
    private String mName;

    private final class ServiceHandler extends Handler {
    	public ServiceHandler(Looper looper) {
            super(looper);
            // TODO: wifi check is super slow. How do I get around this? let's commented it out for now
            /*mPreferences = getSharedPreferences(Utilities.SETTING_PREFERENCES, 0);

        	mWifiMode = mPreferences.getBoolean("wifiOnly", false);*/
        }

        @Override
        public void handleMessage(Message msg) {
        	/*if (mWifiMode) {
        		ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        		NetworkInfo wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        		// wait until wifi connected
        		while (!wifi.isConnected() && mWifiMode) {
        			try {
        				Thread.sleep(3000);
        			} catch (InterruptedException iEx) {
        				
        			}
        			mWifiMode = mPreferences.getBoolean("wifiOnly", false);
        		}
        		Log.e("CancelableIntent", "Wifi check successfully completed");
        	}*/

        	onHandleIntent((Intent)msg.obj);

        	//stopSelf(msg.arg1);
        }
        
        private boolean mWifiMode;
        private SharedPreferences mPreferences;
    }

    public CancelableIntentService(String name) {
        super();
        mName = name;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread thread = new HandlerThread("IntentService[" + mName + "]");
        thread.start();

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        // guard
        if (intent == null) return;
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 	= startId;
        msg.obj 	= intent;
        msg.what 	= intent.getIntExtra(INTENT_REQUEST_ID, 0);

        boolean isPrioritized = intent.getBooleanExtra(INTENT_PRIORITY, false);

        if (isPrioritized) {
        	mServiceHandler.sendMessageAtFrontOfQueue(msg);
        } else {
        	mServiceHandler.sendMessage(msg);
        }
    }

    @Override
    public boolean stopService(Intent name) {
    	mServiceHandler.removeMessages(name.getIntExtra(INTENT_REQUEST_ID, 0));
    	onCanceledIntent(name);

    	return super.stopService(name);
    }

    @Override
    public void onDestroy() {
        mServiceLooper.quit();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    
    protected abstract void onHandleIntent(Intent intent);
    protected abstract void onCanceledIntent(Intent intent);
    
    public static final String INTENT_REQUEST_ID 	= "requestID";
    public static final String INTENT_PRIORITY 		= "priority";
}