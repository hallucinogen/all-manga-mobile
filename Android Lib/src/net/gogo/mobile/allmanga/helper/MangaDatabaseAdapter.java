package net.gogo.mobile.allmanga.helper;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.gogo.mobile.allmanga.model.Manga;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MangaDatabaseAdapter {
	private final static String TAG = MangaDatabaseAdapter.class.getSimpleName();
	
	private final static String DATABASE_TABLE = "manga";
	private final static String DATABASE_CREATE = 
		"CREATE table " + DATABASE_TABLE + "(" +
			Utilities.MANGA_URL + " TEXT PRIMARY KEY, " + 
			Utilities.MANGA_TITLE + " TEXT, " +
			Utilities.MANGA_COVER_PATH + " TEXT, " +
			Utilities.MANGA_ARTIST + " TEXT, " +
			Utilities.MANGA_CHAPTER_LIST + " TEXT, " +
			Utilities.MANGA_DESCRIPTION + " TEXT, " +
			Utilities.MANGA_RATING + " REAL, " + 
			Utilities.MANGA_GENRE + " TEXT, " +
			Utilities.MANGA_RECOMMENDATION + " TEXT, " +
			Utilities.MANGA_BOOKMARK + " INTEGER " +
		")";

	private MangaDatabaseAdapter(Context context) {
		// make sure it is application context
		context = context.getApplicationContext();
		mHelper = new DatabaseHelper(context);
	}

	public static MangaDatabaseAdapter instance(Context context) {
		if (sInstance == null) {
			sInstance = new MangaDatabaseAdapter(context.getApplicationContext());
			sInstance.open();
		}
		return sInstance;
	}

	// open database
	public void open() throws SQLException {
		mDatabase = mHelper.getWritableDatabase();
	}

	public void close() {
		mHelper.close();
	}

	/**
	 * insert a manga to database or update it if it already exists in database
	 * @param manga to be inserted/updated
	 * @return true if it is inserted, false if it is updated/not updated
	 */
	public boolean insertOrUpdateManga(Manga manga) {
		return insertOrUpdateManga(mDatabase, manga);
	}
	
	public static boolean insertOrUpdateManga(SQLiteDatabase db, Manga manga) {
		ContentValues value = manga.toContentValue();
		long result = -1;
		try {
			result = db.insertOrThrow(DATABASE_TABLE, null, value);
		} catch (SQLException sEx) {
			result = -1;
		}

		if (result <= 0) {
			// data is there. We should update instead
			db.update(DATABASE_TABLE, value, Utilities.MANGA_URL + " = '" + manga.getURL() + "'", null);
		}

		return result > 0;
	}

	public boolean deleteManga(Manga manga) {
		return deleteManga(manga.getURL());
	}

	public boolean deleteManga(String url) {
		return mDatabase.delete(DATABASE_TABLE, Utilities.MANGA_URL + " = '" + url + "'", null) > 0;
	}

	public ArrayList<Manga> getAllMangas(boolean detailed) {
		return getAllMangas(mDatabase, detailed);
	}

	public static ArrayList<Manga> getAllMangas(SQLiteDatabase db, boolean detailed) {
		Cursor cursor = null;
		
		try {
			cursor = db.query(DATABASE_TABLE, new String[] {
				Utilities.MANGA_URL, 
				Utilities.MANGA_TITLE,
				Utilities.MANGA_COVER_PATH,
				Utilities.MANGA_ARTIST,
				Utilities.MANGA_CHAPTER_LIST,
				Utilities.MANGA_DESCRIPTION,
				Utilities.MANGA_RATING, 
				Utilities.MANGA_GENRE,
				Utilities.MANGA_RECOMMENDATION,
				Utilities.MANGA_BOOKMARK
			}, null, null, null, null, null);
		} catch (SQLiteException sEx) {
			Log.w(TAG, "Some SQL exception occur but handled" + sEx);
		}
		
		ArrayList<Manga> retval = getAllMangaFromCursor(cursor, detailed);

		// close the cursor
		if (cursor != null) {
			cursor.close();
		}

		return retval;
	}
	
	public Manga getManga(String url) {
		Cursor cursor = null;
		
		try {
			cursor = mDatabase.query(DATABASE_TABLE, new String[] {
				Utilities.MANGA_URL, 
				Utilities.MANGA_TITLE,
				Utilities.MANGA_COVER_PATH,
				Utilities.MANGA_ARTIST,
				Utilities.MANGA_CHAPTER_LIST,
				Utilities.MANGA_DESCRIPTION,
				Utilities.MANGA_RATING, 
				Utilities.MANGA_GENRE,
				Utilities.MANGA_RECOMMENDATION,
				Utilities.MANGA_BOOKMARK
				}, Utilities.MANGA_URL + " = '" + url + "'", null, null, null, null);
		} catch (SQLiteException sEx) {
			Log.w(TAG, "Some SQL exception occur but handled" + sEx);
		}

		ArrayList<Manga> allQueriedManga = getAllMangaFromCursor(cursor, true);
		
		// don't forget to close cursor
		if (cursor != null) {
			cursor.close();
		}

		if (allQueriedManga.size() > 0) {
			return allQueriedManga.get(0);
		}

		return null;
	}
	
	/**
	 * * Retrieves all manga from particular cursor.
	 * Can handle invalid cursor (cursor not from manga database)
	 * @param cursor from database
	 * @param detailed if detailed, fetch data from JSON, too
	 * @return
	 */
	public static ArrayList<Manga> getAllMangaFromCursor(Cursor cursor, boolean detailed) {
		ArrayList<Manga> retval = new ArrayList<Manga>();
		String temp = null;
		
		if (cursor != null) {
			cursor.moveToFirst();
			
			while (!cursor.isAfterLast()) {
				try {
					String url = cursor.getString(0);
					String title = cursor.getString(1);
					Manga manga = new Manga(title, url);
					
					// get data from cursor
					// IMPORTANT: we have to use numbering on column which is very unstable.
					// So when we want to add things to Manga model we should add it AT THE END of the table
					manga.setCoverImagePath(cursor.getString(2));
					manga.setArtist(cursor.getString(3));
					final JSONArray chapterJSON = (temp = cursor.getString(4)) != null && detailed ? new JSONArray(temp) : new JSONArray();
					manga.setChapterFromJSON(chapterJSON);
					manga.setDescription(cursor.getString(5));
					manga.setRating(cursor.getFloat(6));
					final JSONArray genreJSON = (temp = cursor.getString(7)) != null && detailed ? new JSONArray(temp) : new JSONArray();
					manga.setGenreFromJSON(genreJSON);
					final JSONArray recommendationJSON = (temp = cursor.getString(8)) != null && detailed ? new JSONArray(temp) : new JSONArray();
					manga.setRecommendationFromJSON(recommendationJSON);
					manga.setBookmark(cursor.getInt(9));

					retval.add(manga);
				} catch (JSONException jEx) {
					Log.e(TAG, jEx.toString());
				}
				cursor.moveToNext();
			}
		}

		Collections.sort(retval);
		
		return retval;
	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper {
		public DatabaseHelper(Context context) {
			super(context, Utilities.DATABASE_NAME, null, Utilities.DATABASE_VERSION);
		}
		
		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(DATABASE_CREATE);
		}
		
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.e(TAG, "Upgrading database from " + oldVersion + " to " + newVersion);

			// get all manga from old database and from backup file
			ArrayList<Manga> allMangaFromOldDB = getAllMangas(db, true);	
			final String BACKUP_FILE = Utilities.BASE_DIRECTORY + "/" + Utilities.READ_LIST_DATABASE;
			String allBackup = IOUtilities.readFileAsString(BACKUP_FILE);
			try {
				JSONObject json = null;
				try {
					json = allBackup != null ? new JSONObject(allBackup) : new JSONObject();
				} catch (JSONException jEx) {
					json = new JSONObject();
				}

				for (int i = 0, n = allMangaFromOldDB.size(); i < n; ++i) {
					final Manga manga = allMangaFromOldDB.get(i);

					json.put(manga.getURL(), manga.toJSON());
				}

				// put all manga to new database
				IOUtilities.writeToFileAsString(BACKUP_FILE, json.toString());
			} catch (JSONException jEx) {

			}

			// delete old table and create new one
			db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
			onCreate(db);
			
			// put all manga to new database
			allBackup = IOUtilities.readFileAsString(BACKUP_FILE);
			try {
				JSONObject jsonFav = new JSONObject(allBackup);
				JSONArray array = jsonFav.names();

				for (int i = 0; i < jsonFav.length(); ++i) {
					// get manga JSON from here
					JSONObject mangaJSON = jsonFav.getJSONObject(array.getString(i));
					insertOrUpdateManga(db, new Manga(mangaJSON));
				}
			} catch (JSONException jEx) {
				
			}
		}
	}
	
	private SQLiteDatabase mDatabase;
	private final DatabaseHelper mHelper;

	private static MangaDatabaseAdapter sInstance;
}
