package net.gogo.mobile.allmanga.helper;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/*
 * This input stream is used to accommodate slow-connection user
 * Even fast-connection user will find this input stream useful
 */
public class FlushedInputStream extends FilterInputStream {
	public FlushedInputStream(InputStream inputStream) {
		super(inputStream);
	}

	@Override
	public long skip(long count) throws IOException {
		long totalBytesSkipped = 0L;
		
		while (totalBytesSkipped < count) {
			long bytesSkipped = in.skip(count - totalBytesSkipped);
			
			if (bytesSkipped == 0L) {
				int byteRead = read();
				if (byteRead < 0) {
					// eof
					break;
				} else {
					bytesSkipped = 1;
				}
			}
			
			totalBytesSkipped += bytesSkipped;
		}
		
		return totalBytesSkipped;
	}
}
