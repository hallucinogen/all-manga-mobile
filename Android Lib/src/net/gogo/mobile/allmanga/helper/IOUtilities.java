package net.gogo.mobile.allmanga.helper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.Closeable;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

public final class IOUtilities {
    private static final String LOG_TAG = "IOUtilities";

    public static final int IO_BUFFER_SIZE = 4 * 1024;

    public static File getExternalFile(String file) {
        return new File(Environment.getExternalStorageDirectory(), file);
    }

    /**
     * Copy the content of the input stream into the output stream, using a temporary
     * byte array buffer whose size is defined by {@link #IO_BUFFER_SIZE}.
     *
     * @param in The input stream to copy from.
     * @param out The output stream to copy to.
     *
     * @throws java.io.IOException If any error occurs during the copy.
     */
    public static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        int read;
        while ((read = in.read(b)) != -1) {
            out.write(b, 0, read);
        }
    }

    /**
     * Closes the specified stream.
     *
     * @param stream The stream to close.
     */
    public static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                android.util.Log.e(LOG_TAG, "Could not close stream", e);
            }
        }
    }
    
    /*
	 * download image
	 */
	public static Bitmap downloadImage(String urlStr){
		boolean gagal = true;
		int attempt = 0;
		
		Bitmap result = null;
		
		FlushedInputStream fis = null;
		HttpURLConnection connection = null;

		// do this so it get the bitmap multiple times
		while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
			try {
				URL url = new URL(urlStr);
				connection = (HttpURLConnection)url.openConnection();
				connection.setDoInput(true);
				connection.connect();

				fis = new FlushedInputStream(connection.getInputStream());
				gagal = false;

				//Log.e("IOUtilities", "Done getting image stream of " + urlStr);
				result = BitmapFactory.decodeStream(fis);				
			} catch (Exception ex) {
				// fail? try again
				++attempt;
			} catch (OutOfMemoryError oom) {
				//Log.e("IOUtilities", "Out of Memory!");
				BitmapCacheManager.clearCache();
				System.gc();
				++attempt;
			} finally {
				// make sure everything is clean after downloading, whether it fails or not
				IOUtilities.closeStream(fis);

				if (connection != null) {
					connection.disconnect();
				}
			}
		}

		return result;
	}
	
	/*
	 * download image and immediately save downloaded image to storage
	 */
	public static void downloadImageAndSave(String urlStr, String cacheStr) {
		Bitmap bitmap = downloadImage(urlStr);
		cacheImage(bitmap, cacheStr);
	}

	// cache image but don't recycle it
	public static void expensiveCacheImage(Bitmap bitmap, String cacheStr) {
		BufferedOutputStream bos = null;
		
		new File(new File(cacheStr).getParent()).mkdirs();

		try {
			bos = new BufferedOutputStream(new FileOutputStream(cacheStr));
			if (bitmap != null && !bitmap.isRecycled() && bos != null) {
				// if cached here, assume it will not be used by other
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
			}
			bos.close();
			System.gc();
		} catch (IOException iEx) {
			
		} finally {
			IOUtilities.closeStream(bos);
		}
	}

	public static void cacheImage(Bitmap bitmap, String cacheStr) {
		BufferedOutputStream bos = null;
		
		new File(new File(cacheStr).getParent()).mkdirs();

		try {
			bos = new BufferedOutputStream(new FileOutputStream(cacheStr));
			if (bitmap != null && !bitmap.isRecycled() && bos != null) {
				// if cached here, assume it will not be used by other
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
				bitmap.recycle();
			}
			bos.close();
			System.gc();
		} catch (IOException iEx) {
			
		} finally {
			IOUtilities.closeStream(bos);
		}
	}
	
	/**
	 * download image and cache asynchronously
	 */
	public static void downloadImageAndSaveAsync(String url, String cacheURL, Context context) {
		downloadImageAndSaveAsync(url, cacheURL, context, false);
	}
	
	/**
	 * download image and cache asynchronously, prioritized
	 */
	public static void downloadImageAndSaveAsync(String url, String cacheURL, Context context, boolean prioritized) {
		Intent intent = new Intent(context, BitmapDownloaderIntentService.class);
		intent.putExtra(Utilities.INTENT_URL, url);
		intent.putExtra(Utilities.INTENT_CACHE_URL, cacheURL);
		intent.putExtra(BitmapDownloaderIntentService.INTENT_PRIORITY, prioritized);

		context.startService(intent);
	}
	
	public static void stopDownloadingAsync(String url, Context context) {
		Intent intent = new Intent(context, BitmapDownloaderIntentService.class);
		intent.putExtra(Utilities.INTENT_URL, url);
		intent.putExtra(BitmapDownloaderIntentService.INTENT_PRIORITY, true);
		
		context.stopService(intent);
	}

	public static String readFileAsString(String filePath) {
        StringBuilder builder = new StringBuilder();

        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(filePath));
            int chr = 0;
            while ((chr = bis.read()) != -1) {
                builder.append((char) chr);
            }

            bis.close();
        } catch (IOException iEx) {
        }

        return builder.toString();
    }

    public static void writeToFileAsString(String filePath, String data) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));

            final byte[] byteData = IOUtilities.toByteArray(data);

            bos.write(byteData);

            bos.close();
        } catch (IOException iEx) {
            
        }
    }

    public static byte[] toByteArray(String data) {
        byte[] retval = new byte[data.length()];
        for (int i = 0; i < data.length(); ++i) {
            retval[i] = (byte) data.charAt(i);
        }
        return retval;
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        // Calculate ratios of height and width to requested height and width
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio = Math.round((float) width / (float) reqWidth);
	
	        // Choose the smallest ratio as inSampleSize value, this will guarantee
	        // a final image with both dimensions larger than or equal to the
	        // requested height and width.
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }
	
	    return inSampleSize;
	}

	// HACK: this is random number
	public static final long FILE_MINIMAL_SIZE = 124;
}
