package net.gogo.mobile.allmanga.helper;

import java.io.File;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v4.util.LruCache;
import android.util.Log;

public class BitmapCacheManager {
	private static final String TAG = BitmapCacheManager.class.getSimpleName();
	
	private BitmapCacheManager(int cacheSize) {
		mCache = new LruCache<String, Bitmap>(cacheSize) {
	        @Override
	        protected int sizeOf(String key, Bitmap bitmap) {
	            // The cache size will be measured in bytes rather than number of items.
	        	if(Build.VERSION.SDK_INT >= 12) {
	                return bitmap.getByteCount();
	            } else {
	                return bitmap.getRowBytes() * bitmap.getHeight();
	            }
	        }
	    };

	    mThumbnailCache = new LruCache<String, Bitmap>(cacheSize / 16) {
	        @Override
	        protected int sizeOf(String key, Bitmap bitmap) {
	            // The cache size will be measured in bytes rather than number of items.
	        	if(Build.VERSION.SDK_INT >= 12) {
	                return bitmap.getByteCount();
	            } else {
	                return bitmap.getRowBytes() * bitmap.getHeight();
	            }
	        }
	    };
	}

	/**
	 * This function must be called FIRST to let the bitmap cache works
	 * Else the whole bitmap cache will crash
	 * @param context
	 */
	public static void prepareBitmapCache(Context context) {
		// Get memory class of this device, exceeding this amount will throw an
	    // OutOfMemory exception.
		final int memClass = ((ActivityManager) context.getSystemService(
	            Context.ACTIVITY_SERVICE)).getMemoryClass();
		
		// Use 1/8th of the available memory for this memory cache.
	    final int cacheSize = 1024 * 1024 * memClass / 8;

	    sInstance = new BitmapCacheManager(cacheSize);
	}

	private static BitmapCacheManager getInstance() {
		if (sInstance == null) {
			final int cacheSize = 1024 * 1024 * 32 / 8;
			sInstance = new BitmapCacheManager(cacheSize);
		}
		return sInstance;
	}

    /**
     * Adds this bitmap to the cache.
     * @param bitmap The newly downloaded bitmap.
     */
    public static void addBitmapToCache(String url, Bitmap bitmap) {
        if (url != null && bitmap != null) {
            getInstance().mCache.put(url, bitmap);
        }
    }

    /**
     * Adds this thumbnail to the cache.
     * @param bitmap The newly downloaded bitmap.
     */
    public static void addThumbnailToCache(String url, Bitmap bitmap) {
        if (url != null && bitmap != null) {
            getInstance().mThumbnailCache.put(url, bitmap);
        }
    }
    
    /**
     * @param url The URL of the image that will be retrieved from the cache.
     * @return The cached bitmap or null if it was not found.
     */
    public static Bitmap getBitmapFromCache(String url) {
    	if (url == null) {
    		return null;
    	}
    	return getInstance().mCache.get(url);
    }

    /**
     * @param url The URL of the image that will be retrieved from the cache.
     * @return The cached bitmap or null if it was not found.
     */
    public static Bitmap getThumbnailFromCache(String url) {
    	if (url == null) {
    		return null;
    	}
    	return getInstance().mThumbnailCache.get(url);
    }

    /**
     * @param url The URL of the image that will be retrieved from the cache.
     * @param cacheURL The URL where the image is saved in local file were getting from cache failed
     * @return The cached bitmap or null if it was not found.
     */
    public static Bitmap getBitmapFromCache(String url, String cacheURL) {
    	Bitmap bitmap = getBitmapFromCache(url);

    	if (bitmap == null) {
    		// check from local file
    		File file = new File(cacheURL);
    		// it is in file, but not on cache, let us decode and cache it
    		if (file.length() > IOUtilities.FILE_MINIMAL_SIZE) {
    			try {
    				bitmap = BitmapFactory.decodeFile(cacheURL);
    				BitmapCacheManager.addBitmapToCache(url, bitmap);
    			} catch (OutOfMemoryError oom) {
    				Log.e(TAG, "Out of memory exception!");
    				BitmapCacheManager.clearCache();
    			}
    		}
    	}

    	return bitmap;
    }

    /**
     * @param url The URL of the image that will be retrieved from the cache.
     * @param cacheURL The URL where the image is saved in local file were getting from cache failed
     * @return The cached bitmap or null if it was not found.
     */
    public static Bitmap getThumbnailFromCache(String url, String cacheURL) {
    	Bitmap bitmap = getThumbnailFromCache(url);

    	if (bitmap == null) {
    		// check from local file
    		File file = new File(cacheURL);
    		// it is in file, but not on cache, let us decode and cache it
    		if (file.length() > IOUtilities.FILE_MINIMAL_SIZE) {
    			try {
    				final BitmapFactory.Options options = new BitmapFactory.Options();
    			    options.inJustDecodeBounds = true;
    			    BitmapFactory.decodeFile(cacheURL, options);

    			    // Calculate inSampleSize
    			    options.inSampleSize = IOUtilities.calculateInSampleSize(options, Utilities.THUMBNAIL_WIDTH, Utilities.THUMBNAIL_HEIGHT);

    			    // Decode bitmap with inSampleSize set
    			    options.inJustDecodeBounds = false;
    				bitmap = BitmapFactory.decodeFile(cacheURL, options);
    				BitmapCacheManager.addThumbnailToCache(url, bitmap);
    			} catch (OutOfMemoryError oom) {
    				Log.e(TAG, "Out of memory exception!");
    				BitmapCacheManager.clearCache();
    			}
    		}
    	}

    	return bitmap;
    }

    /**
     * Clears the image cache used internally to improve performance. Note that for memory
     * efficiency reasons, the cache will automatically be cleared after a certain inactivity delay.
     */
    public static void clearCache() {
    	getInstance().mCache.evictAll();
    	getInstance().mThumbnailCache.evictAll();
        System.gc();
    }

    // singleton
    private static BitmapCacheManager sInstance;
    private LruCache<String, Bitmap> mCache, mThumbnailCache;
}
