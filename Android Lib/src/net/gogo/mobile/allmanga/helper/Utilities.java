package net.gogo.mobile.allmanga.helper;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Random;

import net.gogo.mobile.allmanga.notification.MangaMobileNotification;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.view.View;

public class Utilities {
	public static void initializeApps(final Context context, Class mangaDetailAct) {
		refreshBaseDirectory(context);
		BitmapCacheManager.prepareBitmapCache(context);
		MangaMobileNotification.makeNotificationSchedule(context, mangaDetailAct);
		MangaMobileServiceStub.initializeServiceStub();
		// initialize ads

		SharedPreferences pref = context.getSharedPreferences(Utilities.SETTING_PREFERENCES, 0);
		MINIMIZE_DOWNLOAD = pref.getBoolean("minimizeDownload", true);

		// backup file and initialzie ads!
		new Thread() {
			public void run() {
				SharedPreferences newPref = context.getSharedPreferences(Utilities.PREFERENCES, 0);
				MangaMobileStorageStub.backupReadListToFile(newPref);

				MangaMobileStorageStub.ADS_ENABLED = newPref.getBoolean(Utilities.ADS_ENABLED, false);
			}
		}.start();
	}
	
	public static void disableOverscroll(View view) {
        Class<?> viewCls = view.getClass();
        try {
            Method m = viewCls.getMethod("setOverScrollMode",
                    new Class[] { int.class });
            int OVER_SCROLL_NEVER = (Integer) viewCls.getField(
                    "OVER_SCROLL_NEVER").get(view);
            m.invoke(view, OVER_SCROLL_NEVER);
        } catch (Exception e) {
            // swallow
        }
    }
	
	public static void refreshBaseDirectory(Context context) {
		File baseDir = new File(Environment.getExternalStorageDirectory() + "/MangaMobile");
		baseDir.mkdirs();
		if (!baseDir.exists())
			BASE_DIRECTORY = context.getFilesDir().toString();
		else
			BASE_DIRECTORY = baseDir.getAbsolutePath();
	}
		
	public static String getRandomTips() {
		return TIPS[Math.abs(RANDOMER.nextInt()) % TIPS.length];
	}
	
	// intents
	public static final String INTENT_TITLE			= "title";
	public static final String INTENT_URL			= "url";
	public static final String INTENT_GENRE			= "genre";
	public static final String INTENT_CACHE_URL		= "cacheURL";
	public static final String INTENT_MANGA			= "manga";
	public static final String INTENT_CHAPTER		= "chapter";
	public static final String INTENT_CHAPTER_NUM	= "chapterNum";
	public static final String INTENT_CHAPTER_NEXT	= "chapterNext";
	public static final String INTENT_CHAPTER_PREV	= "chapterPrev";
	public static final String INTENT_RECEIVER		= "receiver";
	public static final String INTENT_RESULT		= "result";
	public static final String INTENT_DETAIL_CLASS	= "mangaDetailClass";

	public static final int DOWNLOAD_ATTEMPT		= 3;
	public static final int SPLASH_TIME				= 1000;
	// the edge factor in which we should trigger next/prev page when a touch happen at the edge of the screen
	public static final float EDGE_FACTOR			= 0.3f;
	// threshold in which must be passed for a fling event to trigger next/prev page
    public final static int FLING_THRESHOLD = 1500;

	public static final String RATING_STRING			= "Bayesian Average:";
	public static final String PREFERENCES 				= "PREFERENCES";
	//public static final String REQUEST_PREFERENCES	= "REQUEST_PREFERENCS";
	public static final String CHAPTER_PREFERENCES		= "CHAPTER_PREFERENCS";
	public static final String SETTING_PREFERENCES		= "net.gogo.mobile.allmanga_preferences";
	public static final String MANGA_DATABASE 			= "mangaDatabase";
	public static final String READ_LIST_DATABASE		= "readListDatabase";
	public static final String DATABASE_VERSION_PREF	= "databaseVersion";

	// database stuffs
	public static final String DATABASE_NAME 	= "manga_mobile";
	public static final int DATABASE_VERSION	= 6;

	// manga source url
	public static final String SOURCE_MANGAREADER	= "mangareader";
	public static final String SOURCE_GOODMANGA		= "goodmanga";
	public static final String SOURCE_BATOTO		= "batoto";
	public static final String SOURCE_ANIMEA		= "animea";
	public static final String SOURCE_MANGAPANDA	= "mangapanda";
	public static final String SOURCE_MANGAHERE		= "mangahere";
	public static final String SOURCE_MANGAFOX		= "mangafox";

	public static final String GENRE_INCLUDE	= "include";
	public static final String GENRE_EXCLUDE	= "exclude";
	public static final String MANGA_SOURCES	= "sources";

	public static final String CHAPTER_TITLE		= "title";
	public static final String CHAPTER_URL			= "url";
	public static final String CHAPTER_MANGA		= "manga";
	public static final String CHAPTER_IMAGE_URL	= "imageURLs";
	public static final String CHAPTER_RESOLVED_URL	= "resolvedImageURLs";
	public static final String CHAPTER_CURRENT		= "currentSelection";

	public static final String ACHIEVEMENT			= "achievement";
	public static final String ACHIEVEMENT_CHAPTER	= "chapterRead";
	public static final String ACHIEVEMENT_MANGA	= "mangaRead";
	public static final String ACHIEVEMENT_ADS		= "adsPressed";

	// ads stuff
	public static final String ADS_ENABLED			= "adsEnabled";

	public static int BASE_READ_LIMIT				= 7;
	public static int INFINITE_READ_LIMIT			= 1000000;

	public static final String MANGA_TITLE			= "title";
	public static final String MANGA_URL			= "url";
	public static final String MANGA_DESCRIPTION	= "description";
	public static final String MANGA_RATING			= "rating";
	public static final String MANGA_STATUS_ORIGIN	= "status_origin";
	public static final String MANGA_STATUS_SCAN	= "status_scanlation";
	public static final String MANGA_ARTIST			= "artist";
	public static final String MANGA_SOURCE			= "mangaSource";
	public static final String MANGA_GENRE			= "genre";
	public static final String MANGA_RECOMMENDATION	= "recommended";
	public static final String MANGA_CHAPTER_LIST	= "chapters";
	public static final String MANGA_BOOKMARK		= "bookmark";
	public static final String MANGA_LANGUAGE		= "lang";
	public static final String MANGA_COVER_PATH		= "coverImagePath";
	public static final String NEXT_CHAPTER 		= "nextChapter";
	public static final Random RANDOMER				= new Random();

	public static String BASE_DIRECTORY;
	public static boolean MINIMIZE_DOWNLOAD		 	= false;

	public static int THUMBNAIL_WIDTH				= 280;
	public static int THUMBNAIL_HEIGHT				= 280;

	public static final String[] TIPS = {
		"All Manga Mobile is equipped with automatic bookmark",
		"Minimize your manga sources selection to minimize bandwidth download",
		"You can manually bookmark a chapter by tapping the bookmark checkbox on the right side of the list",
		"Have suggestion and bugs to report? Just mail them to developer at hallucinogenplus@gmail.com or go to Setting and Give Feedback",
		"You can reduce the freeze time when downloading a page by waiting for the application to download the entire chapter first",
		"You can use other application while All Manga Mobile is downloading something",
		"Swipe the screen to go to next or previous page",
		"Tap the center of the screen to show reading navigation bar",
		"Tap on the right side or left side to the screen to go to next page or previous page",
		"Have manga source you want to add to All Manga Mobile? Just mail to developer at hallucinogenplus@gmail.com or go to Setting and Give Feedback",
		"Want your language localized in All Manga Mobile? Just mail to developer at hallucinogenplus@gmail.com or go to Setting and Give Feedback",
		"Achievement will increase your read list quota",
		"Feel the apps is going too slow? Try clearing cache and check Minimize image download option on Setting",
		"[Joke] Do you know what KFC stands for? Kenpachi Fried Chicken"
	};
}
