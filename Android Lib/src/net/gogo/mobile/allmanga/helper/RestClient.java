package net.gogo.mobile.allmanga.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
 
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;
 
import android.util.Log;
 
public class RestClient {
 
    private static String convertStreamToString(InputStream is) {
    	BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
        	reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        	int read;
        	final char[] buffer = new char[4096];

            while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
                sb.append(buffer, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError err) {
        	Log.e("Rest Client", "Out ot memory error!");
        	// return empty string
        	return "";
        } finally {
            // don't forget to close!
            IOUtilities.closeStream(reader);
            IOUtilities.closeStream(is);
        }
        return sb.toString();
    }

    public static String get(String url) throws IOException
    {
    	DefaultHttpClient httpclient = getApplicationHttpClient();
        HttpGet httpget = new HttpGet(url);
        httpget.addHeader("Accept-Encoding", "gzip");
        InputStream instream = null;
 
        HttpResponse response;
        String retval = null;
        
        try {
            response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            
            if (entity != null) {
                instream = entity.getContent();
            	Header contentEncoding = response.getFirstHeader("Content-Encoding");
            	if (contentEncoding != null && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
            	    instream = new GZIPInputStream(instream);
            	}

                retval = convertStreamToString(instream);
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IllegalStateException iEx) {
        	
        } finally {
        	IOUtilities.closeStream(instream);
        }
        
        return retval;
    }
    
	public static DefaultHttpClient getApplicationHttpClient() {
		DefaultHttpClient client = new DefaultHttpClient();
        ClientConnectionManager mgr = client.getConnectionManager();
        HttpParams params = client.getParams();
        //HttpConnectionParams.setConnectionTimeout(params, 10000);
        //HttpConnectionParams.setSoTimeout(params, 12000);
        client = new DefaultHttpClient(new ThreadSafeClientConnManager(params, mgr.getSchemeRegistry()), params);
        return client;
	}
}
