package net.gogo.mobile.allmanga.helper;

import java.io.File;
import java.util.HashMap;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class BitmapDownloaderIntentService extends CancelableIntentService {
	private final static String TAG = BitmapDownloaderIntentService.class.getSimpleName();

	// public no name constructor for making standard service
	public BitmapDownloaderIntentService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		sRequestStatuses = getRequestStatuses();
		String url = intent.getStringExtra(Utilities.INTENT_URL);
		String cacheURL = intent.getStringExtra(Utilities.INTENT_CACHE_URL);
		final Integer reqStatus = sRequestStatuses.get(url);

		// it is still in cache, and is needed
		if (BitmapCacheManager.getBitmapFromCache(url, cacheURL) != null) {
			Log.i(TAG, "Bitmap exist in cache " + url);
			sRequestStatuses.put(url, 90);
			return;
		}

		Log.i(TAG, "Requesting to download " + url);
		// request is error or pending. If pending, let's allow the preceding process to do it
		// now let's handle nothingness and error (and even finished if there's no result file there)
		if (reqStatus == null || reqStatus != 100) {
			// make other intent service know we're handling this intent
			sRequestStatuses.put(url, 30);

			// cache first (because anything that use cache will never use from bitmap cache manager)
			Bitmap bitmap = IOUtilities.downloadImage(url);
			Log.i(TAG, "Done downloading, adding to cache " + url);
			sRequestStatuses.put(url, 70);
			BitmapCacheManager.addBitmapToCache(url, bitmap);
			sRequestStatuses.put(url, 80);
			if (cacheURL != null) {
				Log.i(TAG, "Expensive cache to file " + url);
				IOUtilities.expensiveCacheImage(bitmap, cacheURL);
			}
			Log.i(TAG, "Bitmap added to cache " + url);

			// yes! the query completed, let's send success intent
			sRequestStatuses.put(url, 90);
		}
	}
	
	public static HashMap<String, Integer> getRequestStatuses() {
		if (sRequestStatuses == null) {
			sRequestStatuses = new HashMap<String, Integer>();
		}
		
		return sRequestStatuses;
	}

	@Override
	protected void onCanceledIntent(Intent intent) {
		//String url = intent.getStringExtra(Utilities.INTENT_URL);
	}

	public static HashMap<String, Integer> sRequestStatuses;
}
