package net.gogo.mobile.allmanga.helper;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import net.gogo.mobile.allmanga.model.Chapter;
import net.gogo.mobile.allmanga.model.Manga;
import net.gogo.mobile.allmanga.notification.MangaMobileNotification;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

public class AllChapterDownloaderIntentService extends CancelableIntentService {
	private final static String TAG = AllChapterDownloaderIntentService.class.getSimpleName();
	
	public AllChapterDownloaderIntentService() {
		super(TAG);
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {
		try {
			final Manga manga = new Manga(new JSONObject(intent.getStringExtra(Utilities.INTENT_MANGA)));
			final Class mangaDetailClass = (Class)intent.getSerializableExtra(Utilities.INTENT_DETAIL_CLASS);
			final ArrayList<Chapter> chapters = manga.getChapters();

			MangaMobileNotification.makeDownloadAllNotification(this, manga, 0, mangaDetailClass);

			// prepare to save
			SharedPreferences chapterPref = getSharedPreferences(Utilities.CHAPTER_PREFERENCES, 0);

			for (int i = 0, n = chapters.size(); i < n; ++i) {
				final Chapter chapter = chapters.get(i);
				chapter.downloadImageURLs(false);
				
				new File(chapter.getChapterDirectoryPath()).mkdirs();
				
				// start batch downloader
				for (int j = 0, m = chapter.getPageCount(); j < m; ++j) {
					// only download if it should be downloaded (since resolver is not persisted in database

					final String cacheURL = chapter.getLocalImagePath(j);
					// let us check whether the file is already downloaded or are there any broken file
					if (new File(cacheURL).length() < IOUtilities.FILE_MINIMAL_SIZE) {
						// assume the file will never corrupt
						final String url = chapter.resolvePage(j);

						IOUtilities.downloadImageAndSave(url, cacheURL);
						final int progress = 100 * (m * i + j + 1) / (m * n);
						Log.e(TAG, "progress = " + progress);
						MangaMobileNotification.makeDownloadAllNotification(this, manga, progress, mangaDetailClass);
					}
				}
				final int progress = 100 * (i + 1) / n;
				MangaMobileStorageStub.saveChapterToPreferences(chapterPref, chapter);
				MangaMobileNotification.makeDownloadAllNotification(this, manga, progress, mangaDetailClass);
			}
			MangaMobileStorageStub.saveMangaToReadList(this, manga);
			MangaMobileNotification.makeDownloadAllNotification(this, manga, 100, mangaDetailClass);
		} catch (JSONException jEx) {
			
		}	
	}
	
	@Override
	protected void onCanceledIntent(Intent intent) {
		// TODO Auto-generated method stub
		
	}
}
