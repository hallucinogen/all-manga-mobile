package net.gogo.mobile.allmanga.model;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.helper.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class Achievement {
	private final static String TAG = Achievement.class.getSimpleName();
	
	public Achievement(JSONObject json) {
		initializeByJSON(json);
	}
	
	public Achievement(String jsonString) {
		try {
			initializeByJSON(new JSONObject(jsonString));
		} catch (JSONException jEx) {
			mChapterRead 	= 0;
			mMangaRead 		= 0;
			mAdsPressed 	= 0;
		}
	}

	private final void initializeByJSON(JSONObject json) {
		try {
			mChapterRead 	= json.has(Utilities.ACHIEVEMENT_CHAPTER) 	? json.getInt(Utilities.ACHIEVEMENT_CHAPTER) : 0;
			mMangaRead 		= json.has(Utilities.ACHIEVEMENT_MANGA) 	? json.getInt(Utilities.ACHIEVEMENT_MANGA) : 0;
			mAdsPressed 	= json.has(Utilities.ACHIEVEMENT_ADS) 		? json.getInt(Utilities.ACHIEVEMENT_ADS) : 0;
		} catch (JSONException jEx) {
			mChapterRead 	= 0;
			mMangaRead 		= 0;
			mAdsPressed 	= 0;
		}
	}

	public JSONObject toJSON() {
		try {
			JSONObject json = new JSONObject();

			json.put(Utilities.ACHIEVEMENT_CHAPTER, mChapterRead);
			json.put(Utilities.ACHIEVEMENT_MANGA, mMangaRead);
			json.put(Utilities.ACHIEVEMENT_ADS, mAdsPressed);

			return json;
		} catch (JSONException jEx) {
			
		}
		
		return null;
	}

	public int getReadListLimit() {
		int val = 0;
		int chapterScore 	= (val = getChapterScoreIndex()) == 0 ? 0 : CUMULATIVE_ACHIEVEMENT_POINT[val - 1];
		int mangaScore 		= (val = getMangaScoreIndex()) == 8 ? 0 : CUMULATIVE_ACHIEVEMENT_POINT[val - 1];
		int adsScore		= (val = getAdsScoreIndex()) == 16 ? 0 : CUMULATIVE_ACHIEVEMENT_POINT[val - 1];

		return Utilities.BASE_READ_LIMIT + chapterScore + mangaScore + adsScore;
	}

	public int getAchievementPoint() {
		int val = 0;
		int chapterScore 	= (val = getChapterScoreIndex()) == 0 ? 0 : CUMULATIVE_ACHIEVEMENT_POINT[val - 1];
		int mangaScore 		= (val = getMangaScoreIndex()) == 8 ? 0 : CUMULATIVE_ACHIEVEMENT_POINT[val - 1];
		int adsScore		= (val = getAdsScoreIndex()) == 16 ? 0 : CUMULATIVE_ACHIEVEMENT_POINT[val - 1];

		return chapterScore + mangaScore + adsScore;
	}

	public int getChapterScoreIndex() {
		// find out about chapter score
		int chapterIndex = 0;
		while (chapterIndex < 8 && mChapterRead >= ACHIEVEMENT_THRESHOLD[chapterIndex]) {
			++chapterIndex;
		}
		return chapterIndex;
	}
	
	public int getMangaScoreIndex() {
		// find out about manga score
		int mangaIndex = 8;
		while (mangaIndex < 16 && mMangaRead >= ACHIEVEMENT_THRESHOLD[mangaIndex]) {
			++mangaIndex;
		}
		return mangaIndex;
	}
	
	public int getAdsScoreIndex() {
		// find out about ads score
		int adsIndex = 16;
		while (adsIndex < 21 && mAdsPressed >= ACHIEVEMENT_THRESHOLD[adsIndex]) {
			++adsIndex;
		}
		return adsIndex;
	}
	
	public String[] getAchievementTitles(Context context) {
		return context.getResources().getStringArray(R.array.achievementTitle);
	}

	public String[] getAchievementDescriptions(Context context) {
		return context.getResources().getStringArray(R.array.achievementDesc);
	}
	
	public void incrementChapterRead()	{	mChapterRead++;	}
	public void incrementMangaRead()	{	mMangaRead++;	}
	public void incrementAdsPressed()	{	mAdsPressed++;	}

	private int mChapterRead;
	private int mMangaRead;
	private int mAdsPressed;

	public final static int[] ACHIEVEMENT_THRESHOLD = {	
		1, 10, 100, 500, 1000, 2500, 5000, 10000, // chapter to read
		1, 5, 10, 20, 50, 100, 500, 1000, // manga to read
		1, 2, 5, 10, 25 // ads to pressed
	};

	public final static int[] ACHIEVEMENT_POINT = {
		1, 1, 1, 1, 1, 5, 10, 50, 
		1, 1, 1, 5, 10, 25, 50, 99,
		1, 1, 2, 5, 20
	};
	
	public final static int[] CUMULATIVE_ACHIEVEMENT_POINT = {
		1, 2, 3, 4, 5, 10, 20, 70, 
		1, 2, 3, 5, 15, 40, 90, 189,
		1, 2, 4, 9, 29
	};
}
