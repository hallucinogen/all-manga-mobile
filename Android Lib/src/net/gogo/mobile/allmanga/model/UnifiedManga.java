package net.gogo.mobile.allmanga.model;

import java.util.ArrayList;

import android.graphics.Bitmap;

public class UnifiedManga {
	public UnifiedManga(String title) {
		mMangas = new ArrayList<Manga>();
		mTitle	= title;
	}
	
	public void addManga(Manga manga) {
		mMangas.add(manga);
	}
	
	// getter
	public String getCoverImagePath()	{	return mMangas.get(0).getCoverImagePath();	}
	public String getTitle()			{	return mTitle;								}
	public float getRating()			{	return mMangas.get(0).getRating();			}
	public ArrayList<Manga> getItems()	{	return mMangas;								}
	
	public Bitmap getCoverImage(boolean persistOnDownload)	{	
		return mMangas.get(0).getCoverImage(persistOnDownload);
	}

	/**
	 * group a list of manga by title so manga with the same title will be regarded as one
	 * @param list of manga
	 * @return list of unified manga
	 */
	public static ArrayList<UnifiedManga> groupMangaByTitle(ArrayList<Manga> mangas) {
		ArrayList<UnifiedManga> retval = new ArrayList<UnifiedManga>();
		// make copy of the list so we can manipulate that
		ArrayList<Manga> tempManga = new ArrayList<Manga>(mangas);
		
		// while there's still manga in the list
		while (!tempManga.isEmpty()) {
			// get first manga as reference
			final Manga referenceManga = tempManga.get(0);
			final String referenceTitle = referenceManga.getTitle().toLowerCase();
			
			// make unified manga
			UnifiedManga unifiedManga = new UnifiedManga(referenceManga.getTitle());
			unifiedManga.addManga(referenceManga);

			// find all title having same title as reference
			for (int i = 1, n = tempManga.size(); i < n; ++i) {
				final Manga manga = tempManga.get(i);

				// if having the same title, than add it to unified manga
				if (manga.getTitle().toLowerCase().equals(referenceTitle)) {
					unifiedManga.addManga(manga);
				}
			}

			// for all manga in unified manga result, remove it from manga in the list
			for (int i = 0, n = unifiedManga.mMangas.size(); i < n; ++i) {
				tempManga.remove(unifiedManga.mMangas.get(i));
			}
			
			retval.add(unifiedManga);
		}
		
		return retval;
	}
	
	private String mTitle;
	private ArrayList<Manga> mMangas;
}
