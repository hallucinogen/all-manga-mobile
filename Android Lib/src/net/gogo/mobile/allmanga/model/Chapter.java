
package net.gogo.mobile.allmanga.model;

import java.io.File;
import java.util.ArrayList;

import net.gogo.mobile.allmanga.helper.MangaMobileServiceStub;
import net.gogo.mobile.allmanga.helper.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Chapter {
	public Chapter(String rawURL, Manga manga, String chapterTitle){
		mCurrentSelection 	= 0;
		mImageURLs 			= new ArrayList<String>();
		mManga 				= manga;
		mTitle				= chapterTitle;
		mURL 				= rawURL;
		
		init();
	}
	
	public Chapter(JSONObject json) {
		try {
			mTitle	= json.has(Utilities.CHAPTER_TITLE)		? json.getString(Utilities.CHAPTER_TITLE) : null;
			mURL	= json.has(Utilities.CHAPTER_URL) 		? json.getString(Utilities.CHAPTER_URL) : null;
			mManga	= json.has(Utilities.CHAPTER_MANGA) 	? new Manga(json.getJSONObject(Utilities.CHAPTER_MANGA)) : null;

			mCurrentSelection 	= json.has(Utilities.CHAPTER_CURRENT) ? json.getInt(Utilities.CHAPTER_CURRENT) : 0;
			mImageURLs 			= new ArrayList<String>();

			JSONArray imageURLJSON = json.has(Utilities.CHAPTER_IMAGE_URL) ? json.getJSONArray(Utilities.CHAPTER_IMAGE_URL) : null;
			if (imageURLJSON != null) {
				for (int i = 0, n = imageURLJSON.length(); i < n; ++i) {
					mImageURLs.add(imageURLJSON.getString(i));
				}
			}

			mResolvedImageURLs = new String[mImageURLs.size()];
			JSONArray resolvedURLJSON = json.has(Utilities.CHAPTER_RESOLVED_URL) ? json.getJSONArray(Utilities.CHAPTER_RESOLVED_URL) : null;
			if (resolvedURLJSON != null) {
				for (int i = 0, n = resolvedURLJSON.length(); i < n; ++i) {
					mResolvedImageURLs[i] = resolvedURLJSON.getString(i);
				}
			}

			init();
		} catch (JSONException jEx) {
			Log.e("Chapter", "error while decoding chapter " + jEx);
		}
	}

	private void init() {
		StringBuilder sb = new StringBuilder();
		sb.append(Utilities.BASE_DIRECTORY).append("/").append(mManga.getTitle()).append("/").append(mTitle);

		mChapterDirectoryPath = sb.toString();

		// make default strategy
		mStrategy = new LocalImageStrategyResolver() {

			@Override
			public String getLocalImage(int index) {
				return new StringBuilder().append(mChapterDirectoryPath).append(File.separator).append(index).append(".amm").toString();
			}
		};
		// make folder to save image!
		//new File(mChapterDirectoryPath).mkdirs();
	}

	public void downloadImageURLs(boolean force) {
		if (force || mImageURLs.size() == 0) {
			mImageURLs.clear();

			MangaMobileServiceStub.downloadChapterMetadata(this);
		}
	}
	
	public int getSelection()					{	return mCurrentSelection;			}
	public int getPageCount() 					{	return mImageURLs.size();			}
	public String getURL()						{	return mURL;						}
	public String getTitle()					{	return mTitle;						}
	public String getImageURL(int idx)			{	return mImageURLs.get(idx);			}		
	public Manga getManga()						{	return mManga;						}
	public void addChapterURL(String url)		{	mImageURLs.add(url);				}
	
	public void setStrategy(LocalImageStrategyResolver resolver) {
		mStrategy = resolver;
	}

	public String getResolvedPage(int index) {
		if (mResolvedImageURLs == null || mResolvedImageURLs.length == 0 || mResolvedImageURLs.length < mImageURLs.size()) return null;
		return mResolvedImageURLs[index];
	}

	public String resolvePage(int index) {
		if (mResolvedImageURLs == null || mResolvedImageURLs.length == 0 || mResolvedImageURLs.length < mImageURLs.size()) {
			mResolvedImageURLs = new String[mImageURLs.size()];
		}

		// what if still 0?
		if (mResolvedImageURLs.length == 0) {
			return null;
		}

		// check if the URL valid (is online or is from storage, not some random string)
		if (mResolvedImageURLs[index] == null || mResolvedImageURLs[index].trim().equals("") ||
				(!mResolvedImageURLs[index].trim().startsWith("http") && !mResolvedImageURLs[index].trim().startsWith(getURL()))) {
			mResolvedImageURLs[index] = MangaMobileServiceStub.downloadImageURLByPageURL(mImageURLs.get(index));
		}
		return mResolvedImageURLs[index];
	}
	
	public String resolvePage(int index, String forcedResource) {
		if (mResolvedImageURLs == null || mResolvedImageURLs.length == 0) {
			mResolvedImageURLs = new String[mImageURLs.size()];
		}

		mResolvedImageURLs[index] = forcedResource;

		return mResolvedImageURLs[index];
	}

	public JSONObject toJSON() {
		try {
			JSONObject json = new JSONObject();

			json.put(Utilities.CHAPTER_TITLE, mTitle);
			json.put(Utilities.CHAPTER_URL, mURL);
			json.put(Utilities.CHAPTER_MANGA, mManga.toJSON());
			// if at the end of manga, set it to 0 again (prolly he want to re-read it)
			if (mCurrentSelection >= mImageURLs.size()) {
				mCurrentSelection = 0;
			}
			json.put(Utilities.CHAPTER_CURRENT, mCurrentSelection);

			JSONArray array = new JSONArray();
			for (int i = 0, n = mImageURLs.size(); i < n; ++i) {
				array.put(mImageURLs.get(i));
			}
			json.put(Utilities.CHAPTER_IMAGE_URL, array);

			if (mResolvedImageURLs != null) {
				array = new JSONArray();
				for (int i = 0, n = mResolvedImageURLs.length; i < n; ++i) {
					array.put(mResolvedImageURLs[i] != null ? mResolvedImageURLs[i] : "");
				}
				json.put(Utilities.CHAPTER_RESOLVED_URL, array);
			}

			return json;
		} catch (JSONException jEx) {
			
		}

		return null;
	}

	public String getLocalImagePath(int index)	{
		return mStrategy.getLocalImage(index);
	}
	
	public String getChapterDirectoryPath() {
		return mChapterDirectoryPath;
	}
	
	public void goToPage(int page) {
		if (page < 0 || page >= mImageURLs.size()) return;
		
		mCurrentSelection = page;
	}
	
	public interface LocalImageStrategyResolver {
		public String getLocalImage(int index);
	}

	// data
	ArrayList<String> mImageURLs;
	private String[] mResolvedImageURLs;
	private String mURL;
	private Manga mManga;
	private String mTitle;
	private String mChapterDirectoryPath;
	
	// navigation stuffs
	private int mCurrentSelection;
	
	// image resolver
	private LocalImageStrategyResolver mStrategy;
}
