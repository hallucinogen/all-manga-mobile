package net.gogo.mobile.allmanga.model;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.helper.Utilities;

public class MangaSource {
	public MangaSource(int icon, int name, String urlValue) {
		this(icon, name, urlValue, null);
	}
	
	public MangaSource(int icon, int name, String urlValue, String language) {
		mIcon = icon;
		mName = name;
		mURLValue = urlValue;
		mLanguage = language;
	}
	
	public static MangaSource determineMangaSourceByURL(String url) {
		for (int i = 0; i < sMangaSources.length; ++i) {
			final MangaSource source = sMangaSources[i];
			if (url.contains(source.mURLValue)) {
				if (source.mLanguage != null) {
					if (url.contains("#" + source.mLanguage.toLowerCase()) || url.contains("#" + source.mLanguage)) {
						return source;
					}
				} else {
					return source;
				}
			}
		}

		return sLocalSource;
	}

	public int getIcon()		{	return mIcon;		}
	public int getName()		{	return mName;		}
	public String getURLValue()	{	return mURLValue;	}
	public String getLanguage()	{	return mLanguage;	}
	
	public String getRequestString() {
		if (mLanguage != null) {
			return mURLValue + "__" + mLanguage.toLowerCase();
		} else {
			return mURLValue;
		}
	}

	private int mIcon;
	private int mName;
	private String mURLValue;
	private String mLanguage;
	
	// add manga here by detailed item (more detailed on top). Because the first one that satisfy the condition will be returned
	private static final MangaSource[] sMangaSources = new MangaSource[] {
		new MangaSource(R.drawable.animea, R.string.animea, Utilities.SOURCE_ANIMEA),
		new MangaSource(R.drawable.batoto_french, R.string.batoto_french, Utilities.SOURCE_BATOTO, "French"),
		new MangaSource(R.drawable.batoto_german, R.string.batoto_german, Utilities.SOURCE_BATOTO, "German"),
		new MangaSource(R.drawable.batoto_indonesian, R.string.batoto_indonesian, Utilities.SOURCE_BATOTO, "Indonesian"),
		new MangaSource(R.drawable.batoto_italian, R.string.batoto_italian, Utilities.SOURCE_BATOTO, "Italian"),
		new MangaSource(R.drawable.batoto_malay, R.string.batoto_malay, Utilities.SOURCE_BATOTO, "Malay"),
		new MangaSource(R.drawable.batoto_spanish, R.string.batoto_spanish, Utilities.SOURCE_BATOTO, "Spanish"),
		new MangaSource(R.drawable.batoto, R.string.batoto, Utilities.SOURCE_BATOTO),
		new MangaSource(R.drawable.goodmanga, R.string.goodmanga, Utilities.SOURCE_GOODMANGA),
		new MangaSource(R.drawable.mangapanda, R.string.mangapanda, Utilities.SOURCE_MANGAPANDA),
		new MangaSource(R.drawable.mangahere, R.string.mangahere, Utilities.SOURCE_MANGAHERE),
		new MangaSource(R.drawable.mangareader, R.string.mangareader, Utilities.SOURCE_MANGAREADER),
		new MangaSource(R.drawable.mangafox, R.string.mangafox, Utilities.SOURCE_MANGAFOX),
	};

	private static final MangaSource sLocalSource = 
		new MangaSource(R.drawable.sdcard, R.string.sdcard, "");
}
