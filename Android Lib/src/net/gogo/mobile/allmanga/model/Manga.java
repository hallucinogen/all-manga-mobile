package net.gogo.mobile.allmanga.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;

import net.gogo.mobile.allmanga.helper.BitmapCacheManager;
import net.gogo.mobile.allmanga.helper.IOUtilities;
import net.gogo.mobile.allmanga.helper.MangaMobileServiceStub;
import net.gogo.mobile.allmanga.helper.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Log;

public class Manga implements Comparable<Manga> {
	private static String TAG = "MANGA";
	public Manga(String title, String url) {
		mTitle 		= title;
		mURL		= url;
		mChapters	= new ArrayList<Chapter>();
		mGenre		= new ArrayList<String>();
		mArtist		= "";
		mRecommendation = new ArrayList<Manga>();
		mStatusAtOrigin = "";
		mStatusScanlation = "";

		//mCoverImagePath = Utilities.BASE_DIRECTORY + "/" + mTitle + "/" + "cover.jpg";
	}
	
	// make manga out of json object from Manga Mobile Service
	public Manga(JSONObject json) {
		try {
			mTitle				= json.has(Utilities.MANGA_TITLE)			? json.getString(Utilities.MANGA_TITLE) : null;
			mURL				= json.has(Utilities.MANGA_URL) 			? json.getString(Utilities.MANGA_URL) : null;
			mDescription		= json.has(Utilities.MANGA_DESCRIPTION) 	? json.getString(Utilities.MANGA_DESCRIPTION) : null;
			mCoverImagePath		= json.has(Utilities.MANGA_COVER_PATH)		? json.getString(Utilities.MANGA_COVER_PATH) : null;
			mArtist				= json.has(Utilities.MANGA_ARTIST)			? json.getString(Utilities.MANGA_ARTIST) : "";
			mStatusAtOrigin 	= json.has(Utilities.MANGA_STATUS_ORIGIN)	? json.getString(Utilities.MANGA_STATUS_ORIGIN) : "";
			mStatusScanlation	= json.has(Utilities.MANGA_STATUS_SCAN)		? json.getString(Utilities.MANGA_STATUS_SCAN) : "";
			mRating				= json.has(Utilities.MANGA_RATING) 			? (float)json.getDouble(Utilities.MANGA_RATING) : 0;
			
			final JSONArray genreJSON = json.has(Utilities.MANGA_GENRE) ? json.getJSONArray(Utilities.MANGA_GENRE) : new JSONArray();
			setGenreFromJSON(genreJSON);
			
			final JSONArray chapterJSON = json.has(Utilities.MANGA_CHAPTER_LIST) ? json.getJSONArray(Utilities.MANGA_CHAPTER_LIST) : new JSONArray();
			setChapterFromJSON(chapterJSON);
			
			final JSONArray recommendationJSON = json.has(Utilities.MANGA_RECOMMENDATION) ? json.getJSONArray(Utilities.MANGA_RECOMMENDATION) : new JSONArray();
			setRecommendationFromJSON(recommendationJSON);

			mBookmark	= json.has(Utilities.MANGA_BOOKMARK)	? json.getInt(Utilities.MANGA_BOOKMARK) : mChapters.size() - 1;
		} catch (JSONException jEx) {
			Log.e("Manga", "error while decoding manga " + jEx);
		}
		//mCoverImagePath = Utilities.BASE_DIRECTORY + "/" + mTitle + "/" + "cover.jpg";
	}

	public void downloadMangaMetadata(){
		MangaMobileServiceStub.downloadMangaMetadata(this);
	}
	
	public JSONObject toJSON() {
		try {
			JSONObject json = new JSONObject();
			JSONArray jsonChapter = getJSONChapter();
			JSONArray jsonGenre = getJSONGenre();
			JSONArray jsonRecommendation = getJSONRecommendation();

			json.put(Utilities.MANGA_TITLE, mTitle);
			json.put(Utilities.MANGA_URL, mURL);
			json.put(Utilities.MANGA_COVER_PATH, mCoverImagePath);
			json.put(Utilities.MANGA_ARTIST, mArtist);
			json.put(Utilities.MANGA_CHAPTER_LIST, jsonChapter);
			json.put(Utilities.MANGA_DESCRIPTION, mDescription);
			json.put(Utilities.MANGA_RATING, mRating);
			json.put(Utilities.MANGA_GENRE, jsonGenre);
			json.put(Utilities.MANGA_RECOMMENDATION, jsonRecommendation);
			json.put(Utilities.MANGA_BOOKMARK, mBookmark);

			return json;
		} catch (JSONException jEx) {
			
		}

		return null;
	}
	
	/**
	 * basically the same as toJSON, but it create ContentValue for DB
	 * @return a content value of the manga, ready to put on database
	 */
	public ContentValues toContentValue() {
		ContentValues contentValue = new ContentValues();
		
		try {
			JSONArray jsonChapter = getJSONChapter();
			JSONArray jsonGenre = getJSONGenre();
			JSONArray jsonRecommendation = getJSONRecommendation();
			
			contentValue.put(Utilities.MANGA_TITLE, mTitle);
			contentValue.put(Utilities.MANGA_URL, mURL);
			contentValue.put(Utilities.MANGA_COVER_PATH, mCoverImagePath);
			contentValue.put(Utilities.MANGA_ARTIST, mArtist);
			contentValue.put(Utilities.MANGA_CHAPTER_LIST, jsonChapter.toString());
			contentValue.put(Utilities.MANGA_DESCRIPTION, mDescription);
			contentValue.put(Utilities.MANGA_RATING, mRating);
			contentValue.put(Utilities.MANGA_GENRE, jsonGenre.toString());
			contentValue.put(Utilities.MANGA_RECOMMENDATION, jsonRecommendation.toString());
			contentValue.put(Utilities.MANGA_BOOKMARK, mBookmark);
			
			return contentValue;
		} catch (JSONException jEx) {
			
		}
		
		return null;
	}
	
	public JSONArray getJSONChapter() throws JSONException {
		JSONArray jsonChapter = new JSONArray();
		// put the chapter to json array reversed
		for (int i = mChapters.size() - 1; i >= 0 ; --i) {
			JSONObject tempJSON = new JSONObject();
			String chapterTitle = mChapters.get(i).getTitle();
			String chapterURL	= mChapters.get(i).getURL();
			
			tempJSON.put(Utilities.CHAPTER_TITLE, chapterTitle);
			tempJSON.put(Utilities.CHAPTER_URL, chapterURL);
			
			jsonChapter.put(tempJSON.toString());
		}
		
		return jsonChapter;
	}
	
	public JSONArray getJSONGenre() throws JSONException {
		JSONArray jsonGenre = new JSONArray();
		for (int i = 0; i < mGenre.size(); ++i) {
			jsonGenre.put(mGenre.get(i));
		}
		
		return jsonGenre;
	}
	
	public JSONArray getJSONRecommendation() throws JSONException {
		JSONArray jsonRecommendation = new JSONArray();
		if (mRecommendation != null) {
			for (int i = 0; i < mRecommendation.size(); ++i) {
				jsonRecommendation.put(mRecommendation.get(i).toJSON());
			}
		}
		
		return jsonRecommendation;
	}
	
	public void setChapterFromJSON(JSONArray chapterJSON) throws JSONException {
		mChapters = new ArrayList<Chapter>();
		for (int i = 0, n = chapterJSON.length(); i < n; ++i) {
			JSONObject tempJSON 	= null;
			try {
				tempJSON = chapterJSON.getJSONObject(i);
			} catch (JSONException jEx) {
				tempJSON = new JSONObject(chapterJSON.getString(i));
			}
			final String chapterTitle 	= tempJSON.getString(Utilities.CHAPTER_TITLE);
			final String chapterURL		= tempJSON.getString(Utilities.CHAPTER_URL);

			mChapters.add(0, new Chapter(chapterURL, this, chapterTitle));
		}
	}
	
	public void setGenreFromJSON(JSONArray genreJSON) throws JSONException {
		mGenre = new ArrayList<String>();
		for (int i = 0, n = genreJSON.length(); i < n; ++i) {
			mGenre.add(genreJSON.getString(i));
		}
	}
	
	public void setRecommendationFromJSON(JSONArray recommendationJSON) throws JSONException {
		mRecommendation = new ArrayList<Manga>();
		for (int i = 0, n = recommendationJSON.length(); i < n; ++i) {
			mRecommendation.add(new Manga(recommendationJSON.getJSONObject(i)));
		}
	}

	public ArrayList<Chapter> getChapters()		{	return mChapters;			}
	public String getTitle()					{	return mTitle;				}
	public String getURL() 						{	return mURL;				}
	public String getDescription() 				{	return mDescription;		}
	public String getStatusAtOrigin() 			{	return mStatusAtOrigin;		}
	public String getStatusScanlation() 		{	return mStatusScanlation;	}
	public float getRating() 					{	return mRating;				}
	public String getCoverImagePath() 			{	return mCoverImagePath;		}
	public String getArtist()					{	return mArtist;				}
	public ArrayList<String> getGenre() 		{	return mGenre;				}
	public int getBookmark()					{	return mBookmark;			}
	public ArrayList<Manga> getRecommendation()	{	return mRecommendation;		}

	public String getLocalCoverImagePath() {
		return Utilities.BASE_DIRECTORY + "/" + mTitle + "/" + "cover.jpg";
	}

	public Bitmap getCoverImage(boolean persistOnDownload) {
		return getCoverImage(persistOnDownload, false);
	}
	
	public Bitmap getCoverImage(boolean persistOnDownload, boolean forceDownloadNoMatterWhat) {
		if (mCoverImage == null) {
			// try getting from cache
			mCoverImage = BitmapCacheManager.getBitmapFromCache(mCoverImagePath, getLocalCoverImagePath());

			// try getting from file
			if ((mCoverImage == null || mCoverImage.isRecycled())) {
				if (new File(getLocalCoverImagePath()).exists()) {
					// this file size is relatively low so I think it's ok to put it in main thread
					mCoverImage = BitmapFactory.decodeFile(getLocalCoverImagePath());
					BitmapCacheManager.addBitmapToCache(mCoverImagePath, mCoverImage);
				} else {
					// try to download
					if (!Utilities.MINIMIZE_DOWNLOAD || forceDownloadNoMatterWhat) {
						// if it isn't minimized, download!
						mCoverImage = IOUtilities.downloadImage(mCoverImagePath);
						if (persistOnDownload) {
							IOUtilities.expensiveCacheImage(mCoverImage, getLocalCoverImagePath());
						}
						BitmapCacheManager.addBitmapToCache(mCoverImagePath, mCoverImage);
					}
				}
			}
		}

		return mCoverImage;
	}

	public void addChapter(String title, String url)		{	addChapter(new Chapter(url, this, title));	}
	public void addChapter(Chapter chapter)					{	mChapters.add(chapter);						}
	public void addChapterAtFirst(Chapter chapter)			{	mChapters.add(0, chapter);					}
	public void addGenre(String genre)						{	mGenre.add(genre);							}
	public void addRecommendation(Manga recomManga)			{	mRecommendation.add(recomManga);			}
	
	public void setRating(float rating) 					{	mRating = rating;							}
	public void setCoverImagePath(String coverImagePath)	{	mCoverImagePath = coverImagePath;			}
	public void setBookmark(int bookmark)					{	mBookmark = bookmark;						}
	public void setArtist(String artist)					{	mArtist = artist;							}
	
	// set description and make sure the description use right encoding
	public void setDescription(String desc) {
		if (desc == null) desc = "";
		mDescription = Html.fromHtml(desc).toString();
	}

	public int getBookmark(Context context) {
		SharedPreferences pref = context.getSharedPreferences(Utilities.PREFERENCES, 0);
		return pref.getInt(mTitle + "_BOOKMARK", 0);
	}
	
	public String getGenreString() {
		StringBuilder builder = new StringBuilder();
		
		if (mGenre != null && mGenre.size() > 0) {
			builder.append(mGenre.get(0));
			for (int i = 1, n = mGenre.size(); i < n; ++i) {
				builder.append(", ");
				builder.append(mGenre.get(i));
			}
		}
		
		return builder.toString();
	}
	
	@Override
	public int compareTo(Manga o) {
		return mTitle.compareToIgnoreCase(o.mTitle);
	}
	
	// different kind of comparator
	public static class RatingComparator implements Comparator<Manga> {
		@Override
		public int compare(Manga m1, Manga m2) {
			return m1.mRating < m2.mRating ? 1 : -1;
		}
	}
	
	// data
	String mDescription;
	String mCoverImagePath;
	private String mTitle;
	private String mURL;
	private float mRating;
	private Bitmap mCoverImage;
	private String mArtist;
	private String mStatusAtOrigin, mStatusScanlation;
	ArrayList<String> mGenre;
	ArrayList<Chapter> mChapters;
	ArrayList<Manga> mRecommendation;
	
	// bookmark
	private int mBookmark;
}
