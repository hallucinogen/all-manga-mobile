package net.gogo.mobile.allmanga;

import java.io.File;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.helper.Utilities;

import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceScreen;

public class SettingAct extends SherlockPreferenceActivity implements OnPreferenceClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		addPreferencesFromResource(R.xml.settings);
		
		final PreferenceScreen preferenceScreen = getPreferenceScreen();

		// get the preferences
		mAchievement 	= preferenceScreen.findPreference("achievement");
		mRate 			= preferenceScreen.findPreference("rate");
		mLikeUs 		= preferenceScreen.findPreference("like");
		mFeedback 		= preferenceScreen.findPreference("feedback");
		mClearCache		= preferenceScreen.findPreference("clearCache");
		//mInterface 		= preferenceScreen.findPreference("interface");

		// set the listener
		mAchievement.setOnPreferenceClickListener(this);
		mRate.setOnPreferenceClickListener(this);
		mLikeUs.setOnPreferenceClickListener(this);
		mFeedback.setOnPreferenceClickListener(this);
		mClearCache.setOnPreferenceClickListener(this);
		//mInterface.setOnPreferenceClickListener(this);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		final int itemId = item.getItemId();
		
		switch (itemId) {
		case android.R.id.home:
			finish();
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		if (preference == mAchievement) {
			goToAchievement();
		} else if (preference == mRate) {
			goToMarket();
		} else if (preference == mLikeUs) {
			goToFacebook();
		} else if (preference == mFeedback) {
			sendFeedback();
		} else if (preference == mClearCache) {
			// show confirmation dialog
			new AlertDialog.Builder(this)
	        .setIcon(android.R.drawable.ic_dialog_alert)
	        .setTitle(R.string.clear_cache)
	        .setMessage(R.string.clear_cache_confirmation_message)
	        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	            	clearCache();
	            }
	        })
	        .setNegativeButton(R.string.no, null)
	        .show();
		} /*else if (preference == mInterface) {
			Toast.makeText(this, "Coming soon!", 1000).show();
		}*/
		
		return false;
	}

	private void goToMarket() {
		String appPackageName=getResources().getString(R.string.app_package_name);
		Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+appPackageName));
		marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		startActivity(marketIntent);
	}
	
	private void goToFacebook() {
		try {
			// try to open using native API
			String uri = "fb://page/" + Long.toString(447072175305938L);
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			startActivity(intent);
		} catch (ActivityNotFoundException aEx) {
			// open normal browser
			String uri = "http://touch.facebook.com/pages/x/447072175305938";
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			startActivity(intent);
		}
	}

	private void goToAchievement() {
		Intent achievmentIntent = new Intent(this, AchievementAct.class);
		startActivity(achievmentIntent);
	}

	private void sendFeedback() {
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[All Manga Mobile] Feedback/Bug Report");
		emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {
		    "hallucinogenplus@gmail.com"
		});
		emailIntent.setType("message/rfc822");  // i.setType("text/plain"); can also be used but then a lot of non-email apps will show up in the chooser.

		Intent choose = Intent.createChooser(emailIntent, "Select email app:");
		startActivity(choose);
	}
	
	private void clearCache() {
		new Thread() {
			public void run() {
				SharedPreferences pref = getSharedPreferences(Utilities.CHAPTER_PREFERENCES, 0);
				SharedPreferences.Editor editor = pref.edit();
				
				editor.clear();
				
				editor.commit();
				
				deleteRecursive(Utilities.BASE_DIRECTORY);
			}
		}.start();
	}

	private void deleteRecursive(String fileName) {
		File file = new File(fileName);
		if (file.isDirectory()) {
			String[] children = file.list();
			for (int i = 0; i < children.length; ++i) {
				deleteRecursive(fileName + "/" + children[i]);
			}
		}
		file.delete();
	}

	private Preference mAchievement, mRate, mFeedback/*, mInterface */, mClearCache, mLikeUs;
}
