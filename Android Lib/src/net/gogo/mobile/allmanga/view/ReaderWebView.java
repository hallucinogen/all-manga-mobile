package net.gogo.mobile.allmanga.view;

import java.lang.reflect.Method;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;

public class ReaderWebView extends WebView {
	public ReaderWebView(Context context) {
        super(context);
        init();
    }

    public ReaderWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ReaderWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    
    private void init() {
    	mScaleMode = 1;
    	getSettings().setBuiltInZoomControls(true);
    	getSettings().setLoadWithOverviewMode(true);
    	getSettings().setUseWideViewPort(true);
    	getSettings().setRenderPriority(RenderPriority.HIGH);
    	hideDisplayZoomControl();
    	
    	setWebChromeClient(new WebChromeClient() {
    		@Override
    		public void onProgressChanged(WebView view, int newProgress) {
    			// TODO: make progress
    		}
    	});
    }
    
    public void setScaleMode(int scaleMode)	{ mScaleMode = scaleMode;	}

    /*public void loadImage(String url) {
    	//loadUrl(url);
    	loadData("<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} img{width:100%25;} </style></head><body><img src='" + url + "'/></body></html>" ,"text/html",  "UTF-8");
    }*/

    public void loadLocalImage(String local, String url) {
    	switch (mScaleMode) {
    	case 2:
    		loadDataWithBaseURL(local, "<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} </style></head><body><img height=100%25; src='" + url + "'/></body></html>" ,"text/html",  "UTF-8", null);
    		break;
    	case 1:
    	default:
    		loadDataWithBaseURL(local, "<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} </style></head><body><img width=98%25; src='" + url + "'/></body></html>" ,"text/html",  "UTF-8", null);
    		break;	
    	}
    }
    
    public void loadRemoteImage(String url) {
    	switch (mScaleMode) {
    	case 2:
    		loadData("<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} </style></head><body><img height=100%25; src='" + url + "'/></body></html>" ,"text/html",  "UTF-8");
    		break;
    	case 1:
    	default:
    		loadData("<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} </style></head><body><img width=98%25; src='" + url + "'/></body></html>" ,"text/html",  "UTF-8");
    		break;	
    	}
    }
    
    public void setGestureDetector(GestureDetector gestureDetector) {
    	mGestureDetector = gestureDetector;
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
    	return mGestureDetector.onTouchEvent(ev) || super.onTouchEvent(ev);
    }
    
    public void hideDisplayZoomControl() {
        Class<?> settingClass = getSettings().getClass();
        try {
            Method m = settingClass.getMethod("setDisplayZoomControls",
                    new Class[] { boolean.class });
            m.invoke(getSettings(), false);
        } catch (Exception e) {
            // swallow
        }
    }
    
    private int mScaleMode;
    private GestureDetector mGestureDetector;
}
