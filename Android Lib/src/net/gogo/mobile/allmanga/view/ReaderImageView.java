package net.gogo.mobile.allmanga.view;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;

// TODO: do zoom and page fit
public class ReaderImageView extends ImageView {
	private final static String TAG = ReaderImageView.class.getSimpleName();
	
	public ReaderImageView(Context context) {
		super(context);
		init();
	}

	public ReaderImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ReaderImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    
    private void init() {
    	//Matrix matrix = new Matrix();
    	//setImageMatrix(matrix);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        final Drawable drawable = getDrawable();
        
        if (drawable != null && drawable.getIntrinsicWidth() != 0) {
        	height = width * drawable.getIntrinsicHeight() / drawable.getIntrinsicWidth();
        }
        setMeasuredDimension(width, height);
    }
    
    public void setScaleMode(int scaleMode)	{ mScaleMode = scaleMode;	}
    
    public void setGestureDetector(GestureDetector gestureDetector) {
    	mGestureDetector = gestureDetector;
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
    	return mGestureDetector.onTouchEvent(ev) || super.onTouchEvent(ev);
    }

    private int mScaleMode;
    private GestureDetector mGestureDetector;
}
