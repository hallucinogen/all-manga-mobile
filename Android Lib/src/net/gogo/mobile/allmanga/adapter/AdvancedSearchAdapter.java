package net.gogo.mobile.allmanga.adapter;

import java.util.ArrayList;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.model.MangaSource;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Gallery;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class AdvancedSearchAdapter extends BaseExpandableListAdapter implements OnCheckedChangeListener {
	public AdvancedSearchAdapter(Context context) {
		// get genre from R.res
		mGenre = context.getResources().getStringArray(R.array.genre);
		mGenreCheckedValue = new int[mGenre.length];

		mInflater = LayoutInflater.from(context);

		mMangaSourceAdapter = new AllMangaSourceGalleryAdapter(context);
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getGroupCount() {
		// two group: genre and manga source
		return 2;
	}
	
	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}
	
	@Override
	public int getChildrenCount(int groupPosition) {
		switch (groupPosition) {
		case GENRE_GROUP:
			return mGenre.length;
		case MANGASOURCE_GROUP:
			return 1;
		}
		
		return 0;
	}
	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		switch (groupPosition) {
		case GENRE_GROUP:
			return mGenre[childPosition];
		case MANGASOURCE_GROUP:
			return mMangaSourceAdapter.getItem(childPosition);
		}
		
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}
	
	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		View view = null;

		switch (groupPosition) {
		case GENRE_GROUP:
			TextView genreText = null;
			view = convertView;
			if (convertView == null || !(convertView instanceof LinearLayout) || (genreText = (TextView) view.findViewById(R.id.genre)) == null) {
				view = mInflater.inflate(R.layout.genre_search_item, parent, false);
				genreText = (TextView) view.findViewById(R.id.genre);
			} 
			// from here onward genreText is guaranteed defined
			genreText.setText(mGenre[childPosition]);

			// make check listener
			RadioGroup radioGroup	= (RadioGroup) view.findViewById(R.id.genre_radio_group);
			radioGroup.setTag(childPosition);
			radioGroup.setOnCheckedChangeListener(null);
			switch (mGenreCheckedValue[childPosition]) {
			case 0:
				radioGroup.check(R.id.doesnt_matter);
				break;
			case 1:
				radioGroup.check(R.id.include);
				break;
			case 2:
				radioGroup.check(R.id.exclude);
				break;
			}
			radioGroup.setOnCheckedChangeListener(this);
			break;
		case MANGASOURCE_GROUP:
			if (convertView == null || !(convertView instanceof Gallery)) {
				view = mInflater.inflate(R.layout.manga_source_gallery, parent, false);
			} else {
				view = convertView;
			}
			((Gallery)view).setAdapter(mMangaSourceAdapter);
			((Gallery)view).setOnItemClickListener(mMangaSourceAdapter);
			break;
		}

		return view;
	}
	
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		TextView view = null;
		if (convertView == null) {
			view = (TextView)mInflater.inflate(R.layout.headline_textview, parent, false);
		} else {
			view = (TextView)convertView;
		}

		switch (groupPosition) {
		case GENRE_GROUP:
			view.setText(R.string.genre);
			break;
		case MANGASOURCE_GROUP:
			view.setText(R.string.manga_source);
			break;
		}

		return view;
	}
	
	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO: do other handling in case genre is not the only radio group
		int genrePosition = ((Integer)group.getTag()).intValue();
		
		if (checkedId == R.id.include) {
			mGenreCheckedValue[genrePosition] = 1;
		} else if (checkedId == R.id.exclude) {
			mGenreCheckedValue[genrePosition] = 2;
		} else if (checkedId == R.id.doesnt_matter) {
			mGenreCheckedValue[genrePosition] = 0;
		}
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
	
	public int[] getGenreCheckedValue() {
		return mGenreCheckedValue;
	}
	
	public ArrayList<MangaSource> getSelectedSources() {
		return mMangaSourceAdapter.getSelectedSources();
	}
	
	private LayoutInflater mInflater;
	private String[] mGenre;
	private int[] mGenreCheckedValue;
	
	// manga source thingy
	private AllMangaSourceGalleryAdapter mMangaSourceAdapter;

	// constants to make things consistent
	public final static int GENRE_GROUP = 0;
	public final static int MANGASOURCE_GROUP = 1;
}
