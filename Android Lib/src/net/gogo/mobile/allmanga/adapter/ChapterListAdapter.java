package net.gogo.mobile.allmanga.adapter;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.MangaDetailAct;
import net.gogo.mobile.allmanga.helper.AllChapterDownloaderIntentService;
import net.gogo.mobile.allmanga.helper.SingleChapterDownloaderIntentService;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Chapter;
import net.gogo.mobile.allmanga.model.Manga;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class ChapterListAdapter extends BaseAdapter implements OnClickListener {
	public ChapterListAdapter(Context context, Manga manga) {
		mManga = manga;
		mChapterCount = mManga.getChapters().size();

		mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return mChapterCount > 0 ? Integer.MAX_VALUE : 0;
	}

	@Override
	public Object getItem(int position) {
		return mManga.getChapters().get(position % mChapterCount);
	}

	@Override
	public long getItemId(int position) {
		return position % mChapterCount;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;

		if (convertView != null) {
			view = convertView;
		} else {
			view = mInflater.inflate(R.layout.chapter_list_item, parent, false);
		}

		final Chapter item = (Chapter) getItem(position);
		final int id = (int) getItemId(position);

		TextView chapterTitle = (TextView) view
				.findViewById(R.id.chapter_title);
		ImageButton bookmarked = (ImageButton) view
				.findViewById(R.id.bookmarked);
		ImageButton download = (ImageButton) view
				.findViewById(R.id.download);

		// set checkbox to give listener value to identify
		bookmarked.setTag(Integer.valueOf(id));
		download.setTag(Integer.valueOf(id));

		// set the title
		chapterTitle.setText(item.getTitle());

		// set checked if bookmarked
		// make sure to remove the checked listener to prevent infinite loop
		bookmarked.setOnClickListener(null);
		download.setOnClickListener(null);
		if (id == mManga.getBookmark()) {
			bookmarked.setEnabled(false);
		} else {
			bookmarked.setEnabled(true);
		}
		bookmarked.setOnClickListener(this);
		download.setOnClickListener(this);

		view.setTag(item);

		return view;
	}

	@Override
	public void onClick(View v) {
		int id = (Integer) v.getTag();

		if (v.getId() == R.id.bookmarked) {
			// set bookmark
			mManga.setBookmark(id);
		} else if (v.getId() == R.id.download) {
			// download chapter
			final Chapter chapter = (Chapter)getItem(id);
			final Context context = mInflater.getContext();

			Intent intent = new Intent(context, SingleChapterDownloaderIntentService.class);
			intent.putExtra(Utilities.INTENT_CHAPTER, chapter.toJSON().toString());
			context.startService(intent);
			Toast.makeText(context, context.getString(R.string.start_download_chapter, chapter.getTitle()), Toast.LENGTH_SHORT).show();
		}

		// notify changed
		notifyDataSetChanged();
	}

	private LayoutInflater mInflater;

	// cache stuffs
	private int mChapterCount;
	private Manga mManga;
}
