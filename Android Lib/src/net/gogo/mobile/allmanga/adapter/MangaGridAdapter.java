package net.gogo.mobile.allmanga.adapter;

import java.util.ArrayList;
import java.util.concurrent.RejectedExecutionException;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.model.Manga;
import net.gogo.mobile.allmanga.model.UnifiedManga;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MangaGridAdapter extends BaseAdapter {
	public MangaGridAdapter(Context context, ArrayList<Manga> mangas, boolean persistCoverImageOnDownload) {
		mMangas 	= UnifiedManga.groupMangaByTitle(mangas);
		mInflater	= LayoutInflater.from(context);
		mPersistCoverImage = persistCoverImageOnDownload;
		
		//mDefaultCoverDrawable	= new MangaCoverBitmapDrawable(
		//		BitmapFactory.decodeResource(context.getResources(), R.drawable.unknown_cover));
		//mDefaultCoverBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.unknown_cover);
	}
	
	@Override
	public int getCount() {
		return mMangas.size();
	}

	@Override
	public Object getItem(int position) {
		return mMangas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;

		// make / reuse View
		if (convertView == null) {
			view = mInflater.inflate(R.layout.shelf_book, parent, false);
		} else {
			view = convertView;
		}

		// associate with model
		final UnifiedManga manga = mMangas.get(position);
		view.setTag(manga);

		final View finalView = view;
		try {
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... params) {
					mCover = manga.getCoverImage(mPersistCoverImage);

					return null;
				}

				protected void onPostExecute(Void result) {
					if (mCover != null) {
						((ImageView)finalView.findViewById(R.id.cover_image)).setImageBitmap(mCover);
					} else {
						((ImageView)finalView.findViewById(R.id.cover_image)).setImageBitmap(null);
					}
				}
	
				private Bitmap mCover;
			}.execute();
		} catch (RejectedExecutionException rEx) {
			// something might happen because too many thread
		}

		((TextView)finalView.findViewById(R.id.title)).setText(manga.getTitle());
		((Button)finalView.findViewById(R.id.rating_text)).setText(Float.toString(manga.getRating()));

		return finalView;
	}

	private LayoutInflater mInflater;
	private ArrayList<UnifiedManga> mMangas;
	private boolean mPersistCoverImage;
	//private Bitmap mDefaultCoverBitmap;
	//private MangaCoverBitmapDrawable mDefaultCoverDrawable;
}
