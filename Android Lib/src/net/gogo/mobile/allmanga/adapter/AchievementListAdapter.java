package net.gogo.mobile.allmanga.adapter;


import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Achievement;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AchievementListAdapter extends BaseAdapter {
	public AchievementListAdapter(Context context) {
		mInflater = LayoutInflater.from(context);
		// get achievement
		SharedPreferences pref = context.getSharedPreferences(Utilities.PREFERENCES, 0);
		
		mAchievement = new Achievement(pref.getString(Utilities.ACHIEVEMENT, ""));
		
		constAchievementTitle 	= context.getResources().getStringArray(R.array.achievementTitle);
		constAchievementDesc	= context.getResources().getStringArray(R.array.achievementDesc);
		
		mChapterScore 	= mAchievement.getChapterScoreIndex() - 1;
		mMangaScore 	= mAchievement.getMangaScoreIndex() - 1;
		mAdsScore 		= mAchievement.getAdsScoreIndex() - 1;
	}
	
	@Override
	public int getCount() {
		return Achievement.ACHIEVEMENT_THRESHOLD.length;
	}
	
	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;
		
		if (convertView != null) {
			view = convertView;
		} else {
			view = mInflater.inflate(R.layout.achievement_list_item, parent, false);
		}
		
		// TODO: very hardcoded... how to fix this >.<
		boolean isActive = false;
		if (position < 8) {
			if (mChapterScore >= position) 	isActive = true;
		} else if (position < 16) {
			if (mMangaScore >= position)  	isActive = true;
		} else if (position < 21) {
			if (mAdsScore >= position) 		isActive = true;
		}

		TextView achievementTitle 	= (TextView) view.findViewById(R.id.achievement_title);
		TextView achievementDesc 	= (TextView) view.findViewById(R.id.achievement_desc);
		Button achievementBanner	= (Button) view.findViewById(R.id.achievement_banner);
		ImageView achievementCloud	= (ImageView) view.findViewById(R.id.achievement_cloud);

		achievementTitle.setText(constAchievementTitle[position]);
		achievementDesc.setText(constAchievementDesc[position]);
		achievementBanner.setText(Integer.toString(Achievement.ACHIEVEMENT_POINT[position]));
		
		if (isActive) {
			achievementCloud.setVisibility(View.GONE);
		} else {
			achievementCloud.setVisibility(View.VISIBLE);
		}
		
		return view;
	}

	
	private LayoutInflater mInflater;
	private int mChapterScore, mMangaScore, mAdsScore;
	private Achievement mAchievement;
	
	private String[] constAchievementTitle;
	private String[] constAchievementDesc;
}
