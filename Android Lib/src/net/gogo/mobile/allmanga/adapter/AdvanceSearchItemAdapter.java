package net.gogo.mobile.allmanga.adapter;

import net.gogo.mobile.allmangalib.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

public class AdvanceSearchItemAdapter extends BaseAdapter implements OnCheckedChangeListener {
	public AdvanceSearchItemAdapter(Context context) {
		// get genre from R.res
		mGenre = context.getResources().getStringArray(R.array.genre);
		mCheckValue = new int[mGenre.length];
		
		mInflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return mGenre.length;
	}
	
	public Object getItem(int position) {
		return mGenre[position];
	}
	
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;
		if (convertView == null) {
			view = mInflater.inflate(R.layout.genre_search_item, parent, false);
		} else {
			view = convertView;
		}
		
		TextView genreText 		= (TextView) view.findViewById(R.id.genre);
		genreText.setText(mGenre[position]);
		
		RadioGroup radioGroup	= (RadioGroup) view.findViewById(R.id.genre_radio_group);
		radioGroup.setTag(position);
		radioGroup.setOnCheckedChangeListener(null);
		switch (mCheckValue[position]) {
		case 0:
			radioGroup.check(R.id.doesnt_matter);
			break;
		case 1:
			radioGroup.check(R.id.include);
			break;
		case 2:
			radioGroup.check(R.id.exclude);
			break;
		}
		radioGroup.setOnCheckedChangeListener(this);

		
		return view;
	}
	
	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		int genrePosition = ((Integer)group.getTag()).intValue();
		
		if (checkedId == R.id.include) {
			mCheckValue[genrePosition] = 1;
		} else if (checkedId == R.id.exclude) {
			mCheckValue[genrePosition] = 2;
		} else if (checkedId == R.id.doesnt_matter) {
			mCheckValue[genrePosition] = 0;
		}
	}
	
	public int[] getCheckedValue() {
		return mCheckValue;
	}
	
	private LayoutInflater mInflater;
	private String[] mGenre;
	private int[] mCheckValue;
}
