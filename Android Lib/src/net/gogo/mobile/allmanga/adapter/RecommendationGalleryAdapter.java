package net.gogo.mobile.allmanga.adapter;

import java.util.ArrayList;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.model.Manga;
import net.gogo.mobile.allmanga.model.UnifiedManga;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class RecommendationGalleryAdapter extends BaseAdapter {
	public RecommendationGalleryAdapter(Context context, ArrayList<Manga> mangas) {
		mMangas 		= UnifiedManga.groupMangaByTitle(mangas);
		mSourceCount 	= mMangas.size();
        
        mDensity 	= context.getResources().getDisplayMetrics().density;
        mInflater	= LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return mSourceCount;
	}
	
	@Override
	public Object getItem(int position) {
		return mMangas.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;
		
		if (convertView == null) {
			view = mInflater.inflate(R.layout.shelf_book, parent, false);
		} else {
			view = convertView;
		}
		
		// associate with model
		final UnifiedManga manga = mMangas.get(position);
		view.setTag(manga);
		view.setBackgroundColor(0x22ffffff);

		final View finalView = view;
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				mCover = manga.getCoverImage(false);

				return null;
			}

			protected void onPostExecute(Void result) {
				if (mCover != null) {
					((ImageView)finalView.findViewById(R.id.cover_image)).setImageBitmap(manga.getCoverImage(false));
				} else {
					((ImageView)finalView.findViewById(R.id.cover_image)).setImageBitmap(null);
				}
			}

			private Bitmap mCover;
		}.execute();

		((TextView)finalView.findViewById(R.id.title)).setText(manga.getTitle());
		((Button)finalView.findViewById(R.id.rating_text)).setText(Float.toString(manga.getRating()));

		return finalView;
	}

	private LayoutInflater mInflater;
	private ArrayList<UnifiedManga> mMangas;
	
	// cache for performance
	private int mSourceCount;
	private float mDensity;
}
