package net.gogo.mobile.allmanga.adapter;

import java.util.ArrayList;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Manga;
import net.gogo.mobile.allmanga.model.MangaSource;
import net.gogo.mobile.allmanga.model.UnifiedManga;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MangaSourceGalleryAdapter extends BaseAdapter {
	public MangaSourceGalleryAdapter(Context context, UnifiedManga manga) {
		mUnifiedManga 	= manga;
		mItems 			= mUnifiedManga.getItems();
		mSourceCount 	= mItems.size();
        
        mInflater	= LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return mSourceCount;
	}
	
	@Override
	public Object getItem(int position) {
		return mItems.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;
		
		if (convertView == null) {
			view = mInflater.inflate(R.layout.manga_source_item, parent, false);
		} else {
			view = convertView;
		}
		
		final Manga item = mItems.get(position);
		
		// set data
		view.setTag(item);

		// get view of the complex view
		TextView text = (TextView) view.findViewById(R.id.text);
		
		final MangaSource source = MangaSource.determineMangaSourceByURL(item.getURL());
		
		// set image based on URL
		text.setCompoundDrawablesWithIntrinsicBounds(0, source.getIcon(), 0, 0);

		// set text based on URL
		text.setText(source.getName());

		// have to set background to make selection alpha works
		view.setBackgroundColor(0x22ffffff);
		
		return view;
	}

	private LayoutInflater mInflater;
	private UnifiedManga mUnifiedManga;
	
	// cache for performance
	private int mSourceCount;
	private ArrayList<Manga> mItems;
}
