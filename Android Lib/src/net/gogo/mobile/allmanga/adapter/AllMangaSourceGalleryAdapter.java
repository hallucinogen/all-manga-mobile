package net.gogo.mobile.allmanga.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.MangaSource;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class AllMangaSourceGalleryAdapter extends BaseAdapter implements OnItemClickListener {
	public AllMangaSourceGalleryAdapter(Context context) {
		// add all manga sources
		mItems = new ArrayList<String>();
		mItems.add(Utilities.SOURCE_ANIMEA);
		mItems.add(Utilities.SOURCE_BATOTO);
		mItems.add(Utilities.SOURCE_BATOTO + "#french");
		mItems.add(Utilities.SOURCE_BATOTO + "#german");
		mItems.add(Utilities.SOURCE_BATOTO + "#indonesian");
		mItems.add(Utilities.SOURCE_BATOTO + "#italian");
		mItems.add(Utilities.SOURCE_BATOTO + "#malay");
		mItems.add(Utilities.SOURCE_BATOTO + "#spanish");
		mItems.add(Utilities.SOURCE_GOODMANGA);
		mItems.add(Utilities.SOURCE_MANGAFOX);
		mItems.add(Utilities.SOURCE_MANGAHERE);
		mItems.add(Utilities.SOURCE_MANGAPANDA);
		mItems.add(Utilities.SOURCE_MANGAREADER);
		
        mInflater	= LayoutInflater.from(context);
		mSelectedSources = new ArrayList<MangaSource>();
        
        // TODO: probably wrap this somewhere
        SharedPreferences pref = context.getSharedPreferences(Utilities.PREFERENCES, 0);
        String mangaSources = pref.getString(Utilities.MANGA_SOURCES, null);
        if (mangaSources == null) {
        	for (int i = 0; i < mItems.size(); ++i) {
        		final MangaSource source = MangaSource.determineMangaSourceByURL(mItems.get(i));
        		// add english language as default
        		if (source.getLanguage() == null || source.getLanguage().equals("") || source.getLanguage().equalsIgnoreCase("english")) {
        			mSelectedSources.add(source);
        		}
        	}
        } else {
        	try {
        		JSONArray sources = new JSONArray(mangaSources);
        		for (int i = 0, n = sources.length(); i < n; ++i) {
        			mSelectedSources.add(MangaSource.determineMangaSourceByURL(sources.getString(i)));
        		}
        	} catch (JSONException jEx) {
        		
        	}
        }

        mSourceCount = mItems.size();
	}
	
	@Override
	public int getCount() {
		return mSourceCount;
	}
	
	@Override
	public Object getItem(int position) {
		return mItems.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public ArrayList<String> getItems() {
		return mItems;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;
		
		if (convertView == null) {
			view = mInflater.inflate(R.layout.manga_source_item, parent, false);
		} else {
			view = convertView;
		}
		
		final String item = mItems.get(position);

		// get view of the complex view
		TextView text = (TextView) view.findViewById(R.id.text);
		
		final MangaSource source = MangaSource.determineMangaSourceByURL(item);
		text.setTag(source);

		// set image based on URL
		text.setCompoundDrawablesWithIntrinsicBounds(0, source.getIcon(), 0, 0);

		// set text based on URL
		text.setText(source.getName());

		// have to set background to make selection alpha works
		view.setBackgroundColor(0x22ffffff);
		
		if (mSelectedSources.contains(source)) {
			setAlpha(view, 1.0f);
		} else {
			setAlpha(view, 0.3f);
		}

		return view;
	}
	
	@Override
	public void onItemClick(AdapterView<?> parentView, View convertView, int position, long id) {
		// TODO: do other handling in case manga sources is not the only view with child interaction
		final MangaSource source = (MangaSource) convertView.getTag();
		
		// if it exist, means it is selected
		if (mSelectedSources.contains(source)) {
			// mark it as unselected
			setAlpha(convertView, 0.3f);
			// remove from selected list
			mSelectedSources.remove(source);
		} else {
			setAlpha(convertView, 1.0f);
			mSelectedSources.add(source);
		}
	}

	public ArrayList<MangaSource> getSelectedSources() {
		return mSelectedSources;
	}

	private void setAlpha(View view, float alpha) {
		if (Build.VERSION.SDK_INT >= 11) {
			view.setAlpha(alpha);
		} else {
			AlphaAnimation aa = new AlphaAnimation(1.3f - alpha,alpha);
		    aa.setDuration(500);
		    aa.setFillAfter(true);
		    view.startAnimation(aa);
		}
	}

	private LayoutInflater mInflater;
	private ArrayList<String> mItems;
	private ArrayList<MangaSource> mSelectedSources;

	// cache for performance
	private int mSourceCount;
}
