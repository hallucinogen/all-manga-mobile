package net.gogo.mobile.allmanga.adapter;

import net.gogo.mobile.allmanga.fragment.MangaChapterListFrag;
import net.gogo.mobile.allmanga.fragment.MangaMetadataFrag;
import net.gogo.mobile.allmanga.model.Manga;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class MangaDetailPagerAdapter extends FragmentStatePagerAdapter {
	public MangaDetailPagerAdapter(FragmentManager manager, Manga manga, String[] titles) {
		super(manager);
		mManga 	= manga;
		mTitles = titles;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return mTitles[position];
	}
	
	@Override
	public int getCount() {
		return mTitles.length;
	}
	
	@Override
	public Fragment getItem(int position) {
		switch (position) {
		case 0:
			mMangaMetadataFrag = new MangaMetadataFrag();
			mMangaMetadataFrag.setManga(mManga);
			
			return mMangaMetadataFrag;
		case 1:
			MangaChapterListFrag chapterList = new MangaChapterListFrag();
			chapterList.setManga(mManga);
			
			return chapterList;
		}
		
		return null;
	}
	
	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

	public MangaMetadataFrag getMangaMetadataFragment() {
		return mMangaMetadataFrag;
	}

	private MangaMetadataFrag mMangaMetadataFrag;
	private Manga mManga;
	private String[] mTitles;
}
