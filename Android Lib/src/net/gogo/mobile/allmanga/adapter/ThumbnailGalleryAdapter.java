package net.gogo.mobile.allmanga.adapter;

import java.util.ArrayList;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.helper.BitmapCacheManager;
import net.gogo.mobile.allmanga.model.Chapter;
import net.gogo.mobile.allmanga.model.Manga;
import net.gogo.mobile.allmanga.model.UnifiedManga;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ThumbnailGalleryAdapter extends BaseAdapter {
	public ThumbnailGalleryAdapter(Context context, Chapter chapter) {
		mChapter		= chapter;
		mSourceCount 	= mChapter.getPageCount();

        mInflater	= LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return mSourceCount;
	}
	
	@Override
	public Object getItem(int position) {
		return mChapter.getResolvedPage(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;
		
		if (convertView == null) {
			view = mInflater.inflate(R.layout.thumbnail, parent, false);
		} else {
			view = convertView;
		}

		// associate with model
		final String resolvedPage 	= (String)getItem(position);
		final String cacheURL		= mChapter.getLocalImagePath(position);
		view.setTag(resolvedPage);
		view.setBackgroundColor(0x22ffffff);
		((TextView)view.findViewById(R.id.page_number)).setText(Integer.toString(position + 1));

		final View finalView = view;
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				mCover = BitmapCacheManager.getThumbnailFromCache(resolvedPage, cacheURL);

				return null;
			}

			protected void onPostExecute(Void result) {
				if (mCover != null) {
					((ImageView)finalView.findViewById(R.id.thumbnail)).setImageBitmap(mCover);
				} else {
					((ImageView)finalView.findViewById(R.id.thumbnail)).setImageBitmap(null);
				}
			}

			private Bitmap mCover;
		}.execute();

		return finalView;
	}

	private LayoutInflater mInflater;
	private Chapter mChapter;

	// cache for performance
	private int mSourceCount;
}
