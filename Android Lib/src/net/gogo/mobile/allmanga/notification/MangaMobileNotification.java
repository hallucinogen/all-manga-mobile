package net.gogo.mobile.allmanga.notification;

import net.gogo.mobile.allmanga.MangaDetailAct;
import net.gogo.mobile.allmanga.MangaShelvesAct;
import net.gogo.mobile.allmanga.ReadAct;
import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Chapter;
import net.gogo.mobile.allmanga.model.Manga;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class MangaMobileNotification {
	private final static String TAG = MangaMobileNotification.class.getSimpleName();
	public static void makeNotificationSchedule(Context context, Class mangaDetailAct) {
		AlarmManager manager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

		// make intent
		Intent intent = new Intent(context, NotificationActivator.class);
		intent.putExtra(Utilities.INTENT_DETAIL_CLASS, mangaDetailAct);
		PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		// get update frequency from preferences
		SharedPreferences pref = context.getSharedPreferences(Utilities.SETTING_PREFERENCES, 0);
		// default update frequency is 1 hour
		final int repeatNum = Integer.parseInt(pref.getString("updateFrequency", "2"));
		final boolean notify = pref.getBoolean("notify", true);

		// alarm!!
		if (notify) {
			manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + HALF_AN_HOUR, REPEAT_IN_MILLIS[repeatNum] , sender);
		} else {
			manager.cancel(sender);
		}
	}

	public static void makeMangaNotification(Context context, Manga manga, Class mangaDetailAct) {
		Intent notifIntent 		= new Intent(context, mangaDetailAct);
		notifIntent.putExtra(Utilities.INTENT_MANGA, manga.toJSON().toString());
		PendingIntent intent 	= PendingIntent.getActivity(context, manga.getURL().hashCode(), notifIntent, 0);
		// set notification
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder
			.setSmallIcon(R.drawable.sdcard)
			.setAutoCancel(true)
			.setDefaults(Notification.FLAG_AUTO_CANCEL)
			.setTicker(context.getString(R.string.new_chapter_notification, manga.getTitle()))
			.setContentTitle(manga.getTitle())
			.setContentText(context.getString(R.string.new_chapter_notification_content, manga.getTitle(), manga.getChapters().get(manga.getChapters().size() - 1).getTitle()))
			.setWhen(System.currentTimeMillis())
			.setContentIntent(intent);
		// set big icon
		Bitmap largeIcon = BitmapFactory.decodeFile(manga.getLocalCoverImagePath());
		if (largeIcon != null) {
			builder.setLargeIcon(largeIcon);
		} else {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.unknown_cover));
		}
		Notification notification = builder.getNotification();

		notificationManager.notify(manga.getURL().hashCode(), notification);

		// refresh directory
		Utilities.refreshBaseDirectory(context);
	}
	
	public static void makeDownloadAllNotification(Context context, Manga manga, int progress, Class mangaDetailAct) {
		Intent notifIntent 		= new Intent(context, mangaDetailAct);
		notifIntent.putExtra(Utilities.INTENT_MANGA, manga.toJSON().toString());
		PendingIntent intent 	= PendingIntent.getActivity(context, manga.getURL().hashCode(), notifIntent, 0);

		// set notification
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder
			.setSmallIcon(R.drawable.sdcard)
			.setAutoCancel(true)
			.setContentTitle(manga.getTitle())
			.setWhen(System.currentTimeMillis())
			.setContentIntent(intent);

		// set progress or completed
		if (progress >= 100) {
			builder.setTicker(context.getString(R.string.download_complete_notification, manga.getTitle()));
			builder.setContentText(context.getString(R.string.download_complete_notification_content, manga.getChapters().size()));
			builder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
			builder.setOngoing(false);
			builder.setAutoCancel(true);
		} else {
			final int estimatedDownloadedChapter = progress * manga.getChapters().size() / 100;
			builder.setTicker(context.getString(R.string.start_download_all_chapter, manga.getTitle()));
			builder.setContentText(context.getString(R.string.download_complete_notification_content, estimatedDownloadedChapter));
			builder.setDefaults(0);
			builder.setProgress(100, progress, false);
			builder.setOngoing(true);
		}

		// set big icon
		Bitmap largeIcon = BitmapFactory.decodeFile(manga.getLocalCoverImagePath());
		if (largeIcon != null) {
			builder.setLargeIcon(largeIcon);
		} else {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.unknown_cover));
		}
		Notification notification = builder.getNotification();

		notificationManager.notify(manga.getURL().hashCode(), notification);

		// refresh directory
		Utilities.refreshBaseDirectory(context);
	}

	public static void makeDownloadChapterNotification(Context context, int progress, Chapter chapter) {
		Intent notifIntent 		= new Intent(context, ReadAct.class);
		notifIntent.putExtra(Utilities.INTENT_CHAPTER, chapter.toJSON().toString());
		PendingIntent intent 	= PendingIntent.getActivity(context, chapter.getURL().hashCode(), notifIntent, 0);

		// set notification
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder
			.setSmallIcon(R.drawable.sdcard)
			.setContentTitle(chapter.getTitle())
			.setWhen(System.currentTimeMillis())
			.setContentIntent(intent);

		// set progress or completed
		if (progress >= 100) {
			builder.setTicker(context.getString(R.string.download_chapter_complete_notification, chapter.getTitle()));
			builder.setContentText(context.getString(R.string.download_chapter_complete_notification_content, chapter.getPageCount()));
			builder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
			builder.setOngoing(false);
			builder.setAutoCancel(true);
		} else {
			final int estimatedDownloadedChapter = progress * chapter.getPageCount() / 100;
			builder.setTicker(context.getString(R.string.start_download_chapter, chapter.getTitle()));
			builder.setContentText(context.getString(R.string.download_chapter_complete_notification_content, estimatedDownloadedChapter));
			builder.setDefaults(0);
			builder.setProgress(100, progress, false);
			builder.setOngoing(true);
		}

		// set big icon
		Bitmap largeIcon = BitmapFactory.decodeFile(chapter.getManga().getLocalCoverImagePath());
		if (largeIcon != null) {
			builder.setLargeIcon(largeIcon);
		} else {
			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.unknown_cover));
		}
		Notification notification = builder.getNotification();

		notificationManager.notify(chapter.getURL().hashCode(), notification);

		// refresh directory
		Utilities.refreshBaseDirectory(context);
	}

	private final static int HALF_AN_HOUR = 30 * 60 * 1000;
	private final static int[] REPEAT_IN_MILLIS = {0, 30 * 60 * 1000, 60 * 60 * 1000, 3 * 60 * 60 * 1000, 12 * 60 * 60 * 1000, 24 * 60 * 60 * 1000};
}
