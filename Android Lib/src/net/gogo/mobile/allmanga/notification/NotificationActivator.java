package net.gogo.mobile.allmanga.notification;

import java.util.ArrayList;

import net.gogo.mobile.allmanga.helper.MangaMobileStorageStub;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Manga;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

public class NotificationActivator extends BroadcastReceiver {
	private final static String TAG = NotificationActivator.class.getSimpleName();
	@Override
	public void onReceive(Context context, Intent intent) {
		mMangaDetailAct = (Class)intent.getSerializableExtra(Utilities.INTENT_DETAIL_CLASS);
		Utilities.initializeApps(context, mMangaDetailAct);
		mManga = new ArrayList<Manga>();

		// load schedule from preferences
		loadModelsFromStorage(context);
		
		for (int i = 0, n = mManga.size(); i < n; ++i) {
			final Manga manga = mManga.get(i);
			
			new MangaDownloaderAsyncTask(context).execute(manga);
		}
	}

	private void loadModelsFromStorage(Context context) {
    	mManga = MangaMobileStorageStub.getReadListFromStorage(context);
    }
	
	private class MangaDownloaderAsyncTask extends AsyncTask<Manga, Void, Void> {
		public MangaDownloaderAsyncTask(Context context) {
			mContext = context;
		}

		@Override
		protected Void doInBackground(Manga... params) {
			mManga = params[0];
			// get detailed manga from database
			Manga manga = MangaMobileStorageStub.getDetailedManga(mContext, mManga.getURL());
			if (manga != null) {
				mManga = manga;
			}

			mPrevChapterCount = mManga.getChapters().size();			
			mManga.downloadMangaMetadata();
			mNewChapterCount = mManga.getChapters().size();
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// yes! there's new chapter in the manga! Make notification
			if (mPrevChapterCount < mNewChapterCount) {
				MangaMobileNotification.makeMangaNotification(mContext, mManga, mMangaDetailAct);
				// save it to database
				MangaMobileStorageStub.saveMangaToReadList(mContext, mManga);
			}
		}

		private Manga mManga;
		private Context mContext;
		private int mPrevChapterCount, mNewChapterCount;
	}

	private ArrayList<Manga> mManga;
	private Class mMangaDetailAct;
}
