package net.gogo.mobile.allmanga.fragment;

import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockFragment;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.ReadAct;
import net.gogo.mobile.allmanga.adapter.ChapterListAdapter;
import net.gogo.mobile.allmanga.helper.MangaMobileStorageStub;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Achievement;
import net.gogo.mobile.allmanga.model.Chapter;
import net.gogo.mobile.allmanga.model.Manga;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * @author Hallucinogen
 * Fragment which is responsible to make a list of available chapter in a manga
 * The chapter will be shown as a circular list
 */
public class MangaChapterListFrag extends SherlockFragment implements OnItemClickListener {
	private final static String TAG = MangaChapterListFrag.class.getSimpleName();
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View result = inflater.inflate(R.layout.manga_chapter_list, container, false);

		mChapterList	= (ListView) result.findViewById(R.id.chapter_list);
		mLoadingBar		= (LinearLayout) result.findViewById(R.id.loading_bar);
		
		mSharedPreferences = getActivity().getSharedPreferences(Utilities.PREFERENCES, 0);

		if (mManga != null) {
			setViewByData();
		}

		return result;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mManga != null) {
			outState.putString(Utilities.INTENT_MANGA, mManga.toJSON().toString());
		}
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			String mangaJSON = savedInstanceState.getString(Utilities.INTENT_MANGA);
			if (mangaJSON != null) {
				try {
					final Manga manga = new Manga(new JSONObject(mangaJSON));
					setManga(manga);
				} catch (JSONException jEx) {
					
				}
			}
		}
	}
	
	public void setManga(Manga manga) {
		mManga = manga;

		if (mLoadingBar != null) {
			setViewByData();
		}
	}
	
	@Override
	public void onResume() {
		ChapterListAdapter adapter = (ChapterListAdapter)mChapterList.getAdapter();
		
		// update the list to show manga's bookmark!
		if (adapter != null) {
			adapter.notifyDataSetChanged();
			
			// move selection to bookmark, and place it at the center of the list
			final int chapterSize = mManga.getChapters().size();
			
			if (chapterSize > 0) {
				mChapterList.post(new Runnable() {
					// we should do it in a runnable since the position will always return 0 otherwise
				    public void run() {
				    	final int chapterSize = mManga.getChapters().size();
				    	
				    	if (chapterSize > 0) {
							final int visibleChildCount = (mChapterList.getLastVisiblePosition() - mChapterList.getFirstVisiblePosition()) + 1;
							final int jumpTo = (Integer.MAX_VALUE / 2) / chapterSize * chapterSize + mManga.getBookmark() - visibleChildCount / 2;
							mChapterList.setSelection(jumpTo);
				    	}
				    }
				});
			}
		}
		super.onResume();
	}
	
	private void setViewByData() {
		// download manga list!
		//new ChapterListDownloaderTask().execute();
		
		// hide chapter list and show progress bar
		mLoadingBar.setVisibility(View.VISIBLE);
		mChapterList.setVisibility(View.GONE);
		
		// if nothing to show, then don't continue
		if (mManga.getChapters().isEmpty()) return;
		
		final int bookmarkedChapter = mManga.getBookmark();

		mChapterList.setAdapter(new ChapterListAdapter(getActivity(), mManga));

		// hide progress bar and show chapter list
		mLoadingBar.setVisibility(View.GONE);
		mChapterList.setVisibility(View.VISIBLE);
		
		// set the listener
		mChapterList.setOnItemClickListener(MangaChapterListFrag.this);
		
		// set the bookmarked to middle
		mChapterList.post(new Runnable() {
			// we should do it in a runnable since the position will always return 0 otherwise
		    public void run() {
		    	final int chapterSize = mManga.getChapters().size();
		    	
		    	if (chapterSize > 0) {
					final int visibleChildCount = (mChapterList.getLastVisiblePosition() - mChapterList.getFirstVisiblePosition()) + 1;
					final int jumpTo = (Integer.MAX_VALUE / 2) / chapterSize * chapterSize + bookmarkedChapter - visibleChildCount / 2;
					mChapterList.setSelection(jumpTo);
		    	}
		    }
		});
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
		final Chapter item = (Chapter) view.getTag();
		
		// user is reading this manga! We should save this manga to read state
		bookmarkManga((int)id);
		
		// instantiate reader activity based on selected chapter
		final Intent intent = new Intent(getActivity().getApplicationContext(), ReadAct.class);
		intent.putExtra(Utilities.INTENT_CHAPTER, item.toJSON().toString());
		intent.putExtra(Utilities.INTENT_CHAPTER_NUM, (int)id);

		// start reader activity
		startActivityForResult(intent, ReadAct.REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// check if from Read Act
		if (requestCode == ReadAct.REQUEST_CODE) {
			// check if result is ok
			if (resultCode == Activity.RESULT_OK) {
				// then get current chapter
				final int currentChapter = data.getIntExtra(Utilities.INTENT_CHAPTER_NUM, Integer.MIN_VALUE);
				if (currentChapter != Integer.MIN_VALUE) {
					// go to next/prev chapter
					int nextChapter = currentChapter;
					final boolean isNextChapter = data.getBooleanExtra(Utilities.INTENT_CHAPTER_NEXT, false);
					final boolean isPrevChapter = data.getBooleanExtra(Utilities.INTENT_CHAPTER_PREV, false);
					int mangaSize = mManga != null && mManga.getChapters() != null ? mManga.getChapters().size() : 10000000;

					// check is it going to next chapter or previous chapter?
					if (isNextChapter) {
						nextChapter = (currentChapter + 1) % mangaSize;
					} else if (isPrevChapter) {
						nextChapter = (currentChapter - 1 + mangaSize) % mangaSize;
					}
					
					if (mManga == null && getActivity() != null) {
						try {
							Toast.makeText(getActivity(), R.string.connection_error, Toast.LENGTH_SHORT).show();
						} catch (NullPointerException nEx) {
							Toast.makeText(getActivity(), "Connection error", 1000).show();
						}
						getActivity().finish();

						return;
					}

					// set bookmark to next chapter
					bookmarkManga(nextChapter);
					
					// instantiate reader activity based on selected chapter
					final Intent newIntent = new Intent(getActivity().getApplicationContext(), ReadAct.class);
					newIntent.putExtra(Utilities.INTENT_CHAPTER, mManga.getChapters().get(nextChapter).toJSON().toString());
					newIntent.putExtra(Utilities.INTENT_CHAPTER_NUM, nextChapter);

					// start reader activity
					startActivityForResult(newIntent, ReadAct.REQUEST_CODE);

					// check first, can we add this manga to read list?
					new MangaMobileStorageStub.CheckBookmarkTask(getActivity(), mManga) {
						protected void onPostExecute(Void result) {
							if (isBookmared()) {
								MangaMobileStorageStub.saveMangaToReadList(getActivity().getApplicationContext(), mManga);
							} else {
								// not bookmarked already. We should check whether this is allowed or not
								Achievement achievement = new Achievement(mSharedPreferences.getString(Utilities.ACHIEVEMENT, ""));
								int mangaInReadList = MangaMobileStorageStub.getUsedReadLimit(getActivity().getApplicationContext());

								if (mangaInReadList < achievement.getReadListLimit()) {
									MangaMobileStorageStub.saveMangaToReadList(getActivity().getApplicationContext(), mManga);
								} else {
									Toast.makeText(getActivity(), R.string.cannot_add_manga_because_limit, Toast.LENGTH_SHORT).show();
								}
							}
						};
					}.execute();		
				}
			}
		}
	}
	
	private void bookmarkManga(int bookmark) {
		// guard
		if (mManga != null) {
			mManga.setBookmark(bookmark);
		}
	}

	private SharedPreferences mSharedPreferences;
	private Manga mManga;
	private ListView mChapterList;
	private LinearLayout mLoadingBar;
	
}
