package net.gogo.mobile.allmanga.fragment;

import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockFragment;

import net.gogo.mobile.allmanga.MangaDetailAct;
import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.adapter.MangaSourceGalleryAdapter;
import net.gogo.mobile.allmanga.adapter.RecommendationGalleryAdapter;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Manga;
import net.gogo.mobile.allmanga.model.UnifiedManga;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class MangaMetadataFrag extends SherlockFragment implements OnItemClickListener, OnTouchListener {
	private final static String TAG = MangaMetadataFrag.class.getSimpleName();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View result = inflater.inflate(R.layout.manga_metadata, container, false);

		mTitle 			= (TextView) result.findViewById(R.id.title);
		mRatingText		= (TextView) result.findViewById(R.id.rating_text);
		mGenre			= (TextView) result.findViewById(R.id.genre);
		mDescription	= (TextView) result.findViewById(R.id.description);
		mArtist			= (TextView) result.findViewById(R.id.artist);
		mStatusAtOrigin	= (TextView) result.findViewById(R.id.status_origin);
		mStatusScan		= (TextView) result.findViewById(R.id.status_scan);
		mRatingBar		= (RatingBar) result.findViewById(R.id.rating);
		mCoverImage		= (ImageView) result.findViewById(R.id.cover_image);
		mRecommendation	= (Gallery) result.findViewById(R.id.recommendation);
		mParentScrollView = (ScrollView) result.findViewById(R.id.parent_scroll_view);

		// get density
		final float density = getResources().getDisplayMetrics().density;

		// make popup
		mMangaSourcePopup = (RelativeLayout) inflater.inflate(R.layout.shelf_selection_popup, null);
		mMangaSourcePopupWindow = new PopupWindow(mMangaSourcePopup, WindowManager.LayoutParams.WRAP_CONTENT, (int)(120 * density));
		mRecommendation.setOnItemClickListener(this);

		// assign popup view variables
		mMangaSources = (Gallery) mMangaSourcePopup.findViewById(R.id.manga_sources);

		mParentScrollView.setOnTouchListener(this);

		if (mManga != null) {
			setViewByData();
		}

		return result;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mManga != null) {
			outState.putString(Utilities.INTENT_MANGA, mManga.toJSON().toString());
		}
		if (mMangaDetailClass != null) {
			outState.putSerializable(Utilities.INTENT_DETAIL_CLASS, mMangaDetailClass);
		}
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			String mangaJSON = savedInstanceState.getString(Utilities.INTENT_MANGA);
			if (mangaJSON != null) {
				try {
					final Manga manga = new Manga(new JSONObject(mangaJSON));
					setManga(manga);
				} catch (JSONException jEx) {
					
				}
			}
			setMangaDetailClass((Class)savedInstanceState.getSerializable(Utilities.INTENT_DETAIL_CLASS));
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int parent, long id) {
		final UnifiedManga unifiedManga = (UnifiedManga)view.getTag();

		// dismiss popup
		dismissAllPopup();

		showMangaSourcePopup(view);

		// let's stop the marquee
		if (mLastSelectedView != null) {
			mLastSelectedView.findViewById(R.id.title).setSelected(false);
		}
		// start marquee on new view
		view.findViewById(R.id.title).setSelected(true);
		mLastSelectedView = view;

		// make manga sources
		mMangaSources.setAdapter(new MangaSourceGalleryAdapter(getActivity(), unifiedManga));
		mMangaSources.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position,
					long id) {
				final Manga manga = (Manga)view.getTag();

				final Intent intent = new Intent(getActivity().getApplicationContext(), mMangaDetailClass);
				intent.putExtra(Utilities.INTENT_MANGA, manga.toJSON().toString());

				startActivity(intent);
			}
		});
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v == mMangaSourcePopup) return mMangaSourcePopup.onTouchEvent(event) || true;
		if (v != mRecommendation) {
			dismissAllPopup();
			return v.onTouchEvent(event);
		}

		// we must hide the popup when the touch points to nothing
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			// detect what item is being touched by our hand
			int id = mRecommendation.pointToPosition((int)event.getX(), (int)event.getY());
			if (id == -1) {
				// nothing is selected, then hide it!!
				dismissAllPopup();
			} else {
				v.onTouchEvent(event);
			}
		}

		return false;
	}

	private void showMangaSourcePopup(View anchorView) {
		mMangaSourcePopupWindow.showAsDropDown(anchorView);
	}
	
	private void dismissAllPopup() {
		mMangaSourcePopupWindow.dismiss();
	}
	
	public void setManga(Manga manga) {
		mManga = manga;
		
		if (mTitle != null) {
			setViewByData();
		}
	}

	public void setMangaDetailClass(Class mangaDetail) {
		mMangaDetailClass = mangaDetail;
	}
	
	private void setViewByData() {
		
		// set the view by model
		mTitle.setText(mManga.getTitle());
		mRatingText.setText(Float.toString(mManga.getRating()));
		mRatingBar.setRating(mManga.getRating() / 2);
		mGenre.setText(mManga.getGenreString());
		mDescription.setText(mManga.getDescription());
		mStatusAtOrigin.setText(mManga.getStatusAtOrigin());
		mStatusScan.setText(mManga.getStatusScanlation());
		mArtist.setText(mManga.getArtist());
		
		if (mRecommendation != null) {
			mRecommendation.setAdapter(new RecommendationGalleryAdapter(getActivity(), mManga.getRecommendation()));
		}

		// download cover!!
		new CoverImageDownloader().execute();
	}
	
	private class CoverImageDownloader extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			// HACK: wait until attached to activity
			while (getActivity() == null);
			//IOUtilities.downloadImageAndSaveAsync(mManga.getCoverImagePath(), mManga.getLocalCoverImagePath(), getActivity().getApplicationContext());

			// wait for cover image to be there
			mManga.getCoverImage(true, true);

			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			Bitmap cover = mManga.getCoverImage(true);
			if (cover != null && mCoverImage != null) {
				try {
					// try to attach the bitmap to view
					mCoverImage.setImageBitmap(cover);
				} catch (IllegalArgumentException iEx) {
					// view is locked, cannot attach
					Log.w(TAG, "View is locked but succesfully handled");
				}
			}
		}
	}

	// popup stuffs
	private PopupWindow mMangaSourcePopupWindow;
	private RelativeLayout mMangaSourcePopup;
	private Gallery mMangaSources;
	private View mLastSelectedView;
	private ScrollView mParentScrollView;

	private TextView mTitle, mRatingText, mGenre, mDescription, mArtist, mStatusAtOrigin, mStatusScan;
	private ImageView mCoverImage;
	private RatingBar mRatingBar;
	private Gallery mRecommendation;

	private Manga mManga;
	protected Class mMangaDetailClass;
}
