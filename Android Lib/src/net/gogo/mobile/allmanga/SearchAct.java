package net.gogo.mobile.allmanga;

import java.util.ArrayList;

import net.gogo.mobile.allmanga.adapter.AdvancedSearchAdapter;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.MangaSource;
import net.gogo.mobile.allmangalib.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public abstract class SearchAct extends Activity implements OnClickListener, OnKeyListener {
	private final static String TAG = SearchAct.class.getSimpleName();

	@Override
	protected void onPause() {
		super.onPause();
		final SharedPreferences pref = getSharedPreferences(Utilities.PREFERENCES, 0);
		final AdvancedSearchAdapter adapter = (AdvancedSearchAdapter)mAdvanced.getExpandableListAdapter();
		new Thread() {
			public void run() {
				ArrayList<MangaSource> source = adapter.getSelectedSources();
				SharedPreferences.Editor editor = pref.edit();
				
				JSONArray array = new JSONArray();
				
				for (int i = 0; i < source.size(); ++i) {
					array.put(source.get(i).getRequestString());
				}
				
				editor.putString(Utilities.MANGA_SOURCES, array.toString());
				editor.commit();
			}
		}.start();
	}

	@Override
	public void onClick(View v) {
		if (v == mSearchButton) {
			doSearch();
		} else if (v == mAdvanceButton) {
			// re-layout search group
			RelativeLayout parent = (RelativeLayout) mSearchButtonGroup.getParent();
			RelativeLayout.LayoutParams buttonParam = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
			buttonParam.addRule(RelativeLayout.CENTER_HORIZONTAL, 1);
			parent.removeView(mSearchButtonGroup);

			// re-layout title and search bar
			RelativeLayout.LayoutParams titleParam = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
			parent.removeView(mSearchMain);

			// remove focus
			mSearchBar.clearFocus();

			if (mAdvanced.getVisibility() == View.VISIBLE) {
				// is visible, let's hide it
				buttonParam.addRule(RelativeLayout.BELOW, R.id.search_main);
				titleParam.addRule(RelativeLayout.CENTER_IN_PARENT, 1);

				// re-add to view
				parent.addView(mSearchButtonGroup, buttonParam);
				parent.addView(mSearchMain, titleParam);

				mAdvanceButton.setText(R.string.advance_button_text);

				mAdvanced.setVisibility(View.GONE);

				if (mShouldHideTitleImage) {
					mTitleImage.setVisibility(View.VISIBLE);
				}
			} else {
				// is gone, let's show it
				buttonParam.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 1);
				titleParam.addRule(RelativeLayout.ALIGN_PARENT_TOP, 1);

				// re-add to view
				parent.addView(mSearchButtonGroup, buttonParam);
				parent.addView(mSearchMain, titleParam);

				mAdvanceButton.setText(R.string.simplify_button_text);

				mAdvanced.startAnimation(mGenreEnterAnimation);
				mSearchMain.startAnimation(mGenreEnterAnimation);
				mSearchButtonGroup.startAnimation(mGenreOutAnimation);
				mAdvanced.setVisibility(View.VISIBLE);
				if (mShouldHideTitleImage) {
					mTitleImage.setVisibility(View.GONE);
				}
			}
		}
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (v != mSearchBar) return false;

		if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
			// hide the keyboard
			InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
			inputManager.hideSoftInputFromWindow(mSearchBar.getWindowToken(), 0);

			doSearch();

			return true;
		}
		return false;
	}

	protected abstract void doSearch();

	protected void putSearchIntentExtras(Intent intent) {
		// get checked value from adapter
		final AdvancedSearchAdapter adapter = (AdvancedSearchAdapter)mAdvanced.getExpandableListAdapter();
		final int[] checkedValue = adapter.getGenreCheckedValue();
		final ArrayList<MangaSource> sources = adapter.getSelectedSources();

		try {
			JSONObject json = new JSONObject();
			
			JSONArray includeArray = new JSONArray();
			JSONArray excludeArray = new JSONArray();
			JSONArray sourcesArray = new JSONArray();
			
			// get all genre from R.res
			// TODO: maybe want to cache it somewhere and use it everywhere
			String[] genre = getResources().getStringArray(R.array.genre);
			
			for (int i = 0; i < checkedValue.length; ++i) {
				final int val = checkedValue[i];
				switch (val) {
				case 1:
					includeArray.put(genre[i]);
					break;
				case 2:
					excludeArray.put(genre[i]);
					break;
				}
			}
			
			for (int i = 0, n = sources.size(); i < n; ++i) {
				sourcesArray.put(sources.get(i).getRequestString());
			}
			
			// if nothing is included/excluded
			if (includeArray.length() == 0 && excludeArray.length() == 0) {
				// add doujinshi as excluded
				excludeArray.put("Doujinshi");
			}

			json.put(Utilities.GENRE_INCLUDE, includeArray);
			json.put(Utilities.GENRE_EXCLUDE, excludeArray);
			json.put(Utilities.MANGA_SOURCES, sourcesArray);

			intent.putExtra(Utilities.INTENT_GENRE, json.toString());
		} catch (JSONException jEx) {
			Log.e(TAG, "Error while decoding JSON");
		}
	}

	protected LinearLayout mSearchButtonGroup, mSearchMain;

	protected EditText mSearchBar;
	protected ExpandableListView mAdvanced;
	protected Button mSearchButton, mAdvanceButton;
	
	// title image hide stuff
	protected ImageView mTitleImage;
	protected boolean mShouldHideTitleImage;

	// advanced search animation
	protected Animation mGenreEnterAnimation, mGenreOutAnimation;
}
