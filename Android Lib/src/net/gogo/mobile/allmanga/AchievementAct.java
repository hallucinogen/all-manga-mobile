package net.gogo.mobile.allmanga;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.adapter.AchievementListAdapter;
import net.gogo.mobile.allmanga.helper.MangaMobileStorageStub;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Achievement;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class AchievementAct extends SherlockActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.achievement);

		ListView list = (ListView) findViewById(android.R.id.list);
		
		list.setAdapter(new AchievementListAdapter(this));
		SharedPreferences pref = getSharedPreferences(Utilities.PREFERENCES, 0);
		
		Achievement achievement = new Achievement(pref.getString(Utilities.ACHIEVEMENT, ""));
		
		if (getSupportActionBar() != null) {
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			final int readListLimit = achievement.getReadListLimit();
			if (readListLimit >= Utilities.INFINITE_READ_LIMIT) {
				getSupportActionBar().setTitle(getString(R.string.readlist_nolimit));
			} else {
				getSupportActionBar().setTitle(getString(R.string.readlist_limit, readListLimit));
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		final int itemId = item.getItemId();
		switch (itemId) {
		case android.R.id.home:
			finish();
			break;
		}

		return super.onOptionsItemSelected(item);
	}
}
