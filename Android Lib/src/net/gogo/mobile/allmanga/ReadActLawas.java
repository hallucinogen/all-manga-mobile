package net.gogo.mobile.allmanga;

import java.io.File;

import net.gogo.mobile.allmanga.adapter.ThumbnailGalleryAdapter;
import net.gogo.mobile.allmanga.helper.BitmapCacheManager;
import net.gogo.mobile.allmanga.helper.BitmapDownloaderIntentService;
import net.gogo.mobile.allmanga.helper.IOUtilities;
import net.gogo.mobile.allmanga.helper.MangaMobileStorageStub;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Achievement;
import net.gogo.mobile.allmanga.model.Chapter;
import net.gogo.mobile.allmangalib.R;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.sonyericsson.zoom.DynamicZoomControl;
import com.sonyericsson.zoom.ImageViewTouchListener;
import com.sonyericsson.zoom.ImageZoomView;

/**
 * @author Hallucinogen
 * General workflow of read activity is as following
 * 1. first it get the views
 * 2. then it process the intent from previous activity to get Chapter model of this activity
 * 3. then it download page list of the chapter
 * 4. then it start 2 asynchronous task, the first one being the pager opening page 0
 * 5. the second one is batch page downloading, which will start a lot of service
 * 6. for every time the pager trying to open a page, it will start the service
 * 7. where the service will check whether the following request has been made or not
 * 8. then the pager will listen to its request URL from preference
 * 9. completed service will change the preference so the pager will be notified automatically
 */
public class ReadActLawas extends SherlockFragmentActivity implements OnClickListener {
	protected final static String TAG = ReadActLawas.class.getSimpleName();
	public final static int REQUEST_CODE = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reader);
		
		// assign screen width and height
		mScreenWidth	= ((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth();

		// get the views
		//mReadMetadata		= (RelativeLayout) findViewById(R.id.read_metadata);
		mReadNavigation		= (RelativeLayout) findViewById(R.id.read_navigation);
		mPrevPageButton		= (ImageButton) findViewById(R.id.prev_page_button);
		mPrevChapterButton	= (ImageButton) findViewById(R.id.prev_chapter_button);
		mNextPageButton		= (ImageButton) findViewById(R.id.next_page_button);
		mNextChapterButton	= (ImageButton) findViewById(R.id.next_chapter_button);
		mProgress			= (ProgressBar) findViewById(R.id.progress_bar);

		// assign listener
		mPrevPageButton.setOnClickListener(this);
		mPrevChapterButton.setOnClickListener(this);
		mNextPageButton.setOnClickListener(this);
		mNextChapterButton.setOnClickListener(this);

		// get the reader from layout, then assign its listener
		mReaderImageView = (ImageZoomView) findViewById(R.id.reader);
		mZoomControl = new DynamicZoomControl();
		mZoomListener = new ImageViewTouchListener(getApplicationContext());
		mZoomListener.setZoomControl(mZoomControl);
		mZoomListener.setOnGestureListener(mGestureListener);
		mReaderImageView.setZoomState(mZoomControl.getZoomState());
        mReaderImageView.setOnTouchListener(mZoomListener);
        mZoomControl.setAspectQuotient(mReaderImageView.getAspectQuotient());
        resetZoomControl();
		
		// assign actionbar!
		mActionBar = getSupportActionBar();
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.hide();

		// get the reader data, which is the chapter
		Bundle extra = getIntent().getExtras();
		
		mChapter = (Chapter) getLastCustomNonConfigurationInstance();

		if (mChapter == null) {
			String stringExtra = null;

			if (extra != null && (stringExtra = extra.getString(Utilities.INTENT_CHAPTER)) != null) {
				try {
					mChapter = new Chapter(new JSONObject(stringExtra));

					// load more chapter detail from its specialized preferences
					SharedPreferences chapterPref = getSharedPreferences(Utilities.CHAPTER_PREFERENCES, 0);
					
					String chapterJSONString = chapterPref.getString(mChapter.getURL(), null);

					if (chapterJSONString != null) {
						mChapter = new Chapter(new JSONObject(chapterJSONString));
					}
				} catch (JSONException jEx) {
					Log.e(TAG, "Error while decoding json " + jEx);
				}
		
				// try to download the chapter data
				downloadChapterImageList();

				// get current chapter number
				mCurrentChapterNum = extra.getInt(Utilities.INTENT_CHAPTER_NUM);
				mActionBar.setTitle(String.format("%s (%d / %d)",
						mChapter.getTitle(),
						mChapter.getSelection() + 1,
						mChapter.getPageCount()));
			}
		} else {
			startViewAndBatchDownloader();
		}

		// make toast
		mNextPageToast = Toast.makeText(ReadActLawas.this, R.string.next_page, Toast.LENGTH_SHORT);
		mPrevPageToast = Toast.makeText(ReadActLawas.this, R.string.prev_page, Toast.LENGTH_SHORT);

		// make fling threshold
		final ViewConfiguration vc = ViewConfiguration.get(this);
		mFlingMinDistance = vc.getScaledTouchSlop() * 3;
		mFlingThresholdVelocity = vc.getScaledMaximumFlingVelocity();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (mBatchPageDownloaderTask != null) {
			mBatchPageDownloaderTask.cancel(true);
			mBatchPageDownloaderTask.onCancelled();
		}
		SharedPreferences chapterPref = getSharedPreferences(Utilities.CHAPTER_PREFERENCES, 0);
		MangaMobileStorageStub.saveChapterToPreferences(chapterPref, mChapter);
		mReaderImageView.setOnTouchListener(null);
		mZoomListener.setOnGestureListener(null);
        mZoomControl.getZoomState().deleteObservers();
	}

	@Override
	public Object onRetainCustomNonConfigurationInstance() {
		return mChapter;
	}
	
	@Override
	protected void onResume() {
		// let's get required preferences for this activity
		SharedPreferences pref = getSharedPreferences(Utilities.SETTING_PREFERENCES, 0);

		mKeepAlive = pref.getBoolean("screenOn", true);
		//mShowChapterData = pref.getBoolean("showChapterData", true);
		mScreenOrientation = Integer.parseInt(pref.getString("lockScreen", "1"));
		mScaleMode = Integer.parseInt(pref.getString("scaleMode","4"));

		if (mKeepAlive) {
			// if keep alive then we should keep alive (YOU DONT SAY!!)
			getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		// change screen orientation
		switch (mScreenOrientation) {
		case 2:
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			break;
		case 3:
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			break;
		}
		
		//mReaderWebView.setScaleMode(mScaleMode);
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		// whether it's keep-alive or not, we should clear the flag!
		getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		if (mChapter != null && mChapter.getManga() != null) {
			// on activity pause, save the manga to database (to save chapter bookmark)
			new MangaMobileStorageStub.CheckBookmarkTask(this, mChapter.getManga()) {
				protected void onPostExecute(Void result) {
					if (isBookmared()) {
						MangaMobileStorageStub.saveMangaToReadList(getApplicationContext(), mManga);
					} else {
						SharedPreferences pref = getSharedPreferences(Utilities.PREFERENCES, 0);
						
						Achievement achievement = new Achievement(pref.getString(Utilities.ACHIEVEMENT, ""));
						int mangaInReadList = MangaMobileStorageStub.getUsedReadLimit(getApplicationContext());
						
						// save it to DB if manga is available and doesn't start with http
						if (mangaInReadList < achievement.getReadListLimit()) {
							MangaMobileStorageStub.saveMangaToReadList(getApplicationContext(), mManga);
						}
					}
				};
			}.execute();
		}
		super.onPause();
	}
	
	protected void downloadChapterImageList() {
		new Thread() {
			public void run() {
				Thread.currentThread().setPriority(MAX_PRIORITY);
				int retry = 0;
				while (mChapter.getPageCount() == 0 && retry < 3) {
					mChapter.downloadImageURLs(false);
					++retry;
				}

				Log.e(TAG, "Download image list complete!");

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// download image urls fail. We should let the user retry
						if (mChapter.getPageCount() == 0) {
							Toast.makeText(ReadActLawas.this, R.string.connection_error, Toast.LENGTH_SHORT).show();
							finish();
						}

						startViewAndBatchDownloader();
					}
				});
			}
		}.start();
	}

	protected void startViewAndBatchDownloader() {
		goToPage(mChapter.getSelection());
		mBatchPageDownloaderTask = new BatchPageDownloaderTask();
		mBatchPageDownloaderTask.execute();
	}

	protected BatchPageDownloaderTask mBatchPageDownloaderTask;
	protected class BatchPageDownloaderTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			// resolve all chapter page first
			for (int i = 0, n = mChapter.getPageCount(); i < n; ++i) {
				if (isCancelled()) break;
				// if in selection, not batch downloader business :(
				if (i == mChapter.getSelection()) continue;
				mChapter.resolvePage(i);
			}

			// start batch downloader
			for (int i = 0, n = mChapter.getPageCount(); i < n; ++i) {
				// if in selection, not batch downloader business :(
				if (i == mChapter.getSelection()) continue;

				// only download if it should be downloaded (since resolver is not persisted in database)

				final String cacheURL = mChapter.getLocalImagePath(i);
				// let us check whether the file is already downloaded or are there any broken file
				if (new File(cacheURL).length() < IOUtilities.FILE_MINIMAL_SIZE) {
					// assume the file will never corrupt
					if (isCancelled()) break;
					final String url = mChapter.getResolvedPage(i);

					if (isCancelled()) break;
					//Log.e(TAG, "done resolving URL " + url + " for " + cacheURL);
					
					if (BitmapCacheManager.getBitmapFromCache(url, cacheURL) == null) {
						IOUtilities.downloadImageAndSaveAsync(url, cacheURL, getApplicationContext());
					}
				}
			}

			if (isCancelled()) return null;
			// save chapter data!
			SharedPreferences chapterPref = getSharedPreferences(Utilities.CHAPTER_PREFERENCES, 0);
			MangaMobileStorageStub.saveChapterToPreferences(chapterPref, mChapter);
			//Log.e(TAG, "Batch download task is complete");

			return null;
		}
		
		@Override
		protected void onCancelled() {
			// stop the service
			for (int i = 0, n = mChapter.getPageCount(); i < n; ++i) {
				final String cacheURL = mChapter.getLocalImagePath(i);
				// if the file is already downloaded, don't care about it!
				if (new File(cacheURL).length() < IOUtilities.FILE_MINIMAL_SIZE && mChapter.getResolvedPage(i) != null) {
					// assume the file will never corrupt
					final String url = mChapter.getResolvedPage(i);
					IOUtilities.stopDownloadingAsync(url, getApplicationContext());
				}
			}
		}
	}

	@Override
	public void onClick(View v) {
		if (v == mPrevPageButton) {
			goToPrevPage();
		} else if (v == mNextPageButton) {
			goToNextPage();
		} else if (v == mPrevChapterButton) {
			goToPrevChapter();
		} else if (v == mNextChapterButton) {
			goToNextChapter();
		}
	}

	protected ImageViewTouchListener.OnGestureListener mGestureListener = new ImageViewTouchListener.OnGestureListener() {
    	@Override
    	public void onFling(boolean isLeft) {
    		if (!isLeft) {
    			goToNextPage();
    		} else {
    			goToPrevPage();
    		}
    	}

    	public void onSingleTap(android.view.MotionEvent e) {
    		boolean processed = false;
    		
    		final float x = e.getX();

    		// if in edge range
    		if (x < mScreenWidth * Utilities.EDGE_FACTOR) {
    			// left edge
    			goToPrevPage();
    			processed = true;
    		} else if ((1.0f - Utilities.EDGE_FACTOR) * mScreenWidth < x) {
    			// right edge
    			goToNextPage();
    			processed = true;
    		}
    		
    		if (!processed) {
    			// show navigation UI
    			if (mReadNavigation.getVisibility() == View.GONE) {
    				// show it
    				mReadNavigation.setVisibility(View.VISIBLE);
    				mActionBar.show();
    			} else {
    				mReadNavigation.setVisibility(View.GONE);
    				mActionBar.hide();
    			}
    		}
    	}
    	
    	@Override
    	public void onTripleFling(boolean isLeft) {
    		if (!isLeft) {
    			goToNextChapter();
    		} else {
    			goToPrevChapter();
    		}
    	}
    };

    protected void goToNextPage() {
    	goToPage(mChapter.getSelection() + 1);
		
    	//mNextPageToast.show();
    }

    protected void goToPrevPage() {
    	goToPage(mChapter.getSelection() - 1);

		//mPrevPageToast.show();
    }

    protected void goToPage(final int page) {
    	// show progress bar
    	
    	mProgress.setVisibility(View.VISIBLE);
		new Thread() {
			@Override
			public void run() {
				Thread.currentThread().setPriority(MAX_PRIORITY);
				int pageNumber = page;
	
				boolean mIsNextChapter = false;
				boolean mIsPreviousChapter = false;
				int mSelection = 0;
				// check whether going to next chapter or not
				if (pageNumber > mChapter.getSelection() && mChapter.getSelection() == mChapter.getPageCount() - 1) {
					// is going next chapter
					mIsNextChapter = true;
				} else if (pageNumber < mChapter.getSelection() && mChapter.getSelection() == 0) {
					// is going previous chapter
					mIsPreviousChapter = true;
				} else {
					// set current selection to page (we need it to save user last page)
					mChapter.goToPage(pageNumber);
					// this variable is used to make sure we're still dealing with same page selection
					mSelection = mChapter.getSelection();

					// let us check whether the file already exist or not
					final String cacheURL = mChapter.getLocalImagePath(mChapter.getSelection());
					postProgress(10);
					// try decode manually from 
					if (BitmapCacheManager.getBitmapFromCache(mChapter.resolvePage(mChapter.getSelection()), cacheURL) == null) {
						// prioritize this image download
						IOUtilities.downloadImageAndSaveAsync(
								mChapter.getResolvedPage(mChapter.getSelection()), cacheURL, getApplicationContext(), true);
					}
					Log.e(TAG, "URL = " + mChapter.getResolvedPage(mChapter.getSelection()));
					Thread.currentThread().setPriority(Thread.NORM_PRIORITY);
					while (BitmapCacheManager.getBitmapFromCache(mChapter.getResolvedPage(mChapter.getSelection()), cacheURL) == null) {
						Integer progress = BitmapDownloaderIntentService.getRequestStatuses().get(mChapter.getResolvedPage(mChapter.getSelection()));
						if (progress != null) {
							postProgress(progress);
						}
						try {
							Thread.sleep(100);
						} catch (InterruptedException inEx) {
							// swallow
						}
					}
					Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
				}
				
				final boolean finalIsNextChapter = mIsNextChapter;
				final boolean finalIsPreviousChapter = mIsPreviousChapter;
				final int finalSelection = mSelection;

				// post execute
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (finalIsNextChapter || finalIsPreviousChapter) {
							if (finalIsNextChapter) {
								// go to next chapter
								goToNextChapter();
							} else {
								// go to prev chapter
								goToPrevChapter();
							}
						} else {
							// we only want to read!! Show next page!
							if (finalSelection == mChapter.getSelection()) {
								final String cacheURL = mChapter.getLocalImagePath(mChapter.getSelection());
								mReaderImageView.setImage(BitmapCacheManager.getBitmapFromCache(mChapter.getResolvedPage(mChapter.getSelection()), cacheURL));

								mActionBar.setTitle(String.format("%s (%d / %d)",
										mChapter.getTitle(),
										mChapter.getSelection() + 1,
										mChapter.getPageCount()));

								resetZoomControl();
							}
						}
						
						mProgress.setVisibility(View.GONE);
					}
				});
			}
		}.start();
	}
    
    protected void goToNextChapter() {
    	// first let's check whether it is circling or not
		if (mCurrentChapterNum != mChapter.getManga().getChapters().size() - 1) {
			// it is really going to another chapter
			final Intent resultIntent = new Intent();
			resultIntent.putExtra(Utilities.INTENT_CHAPTER_NUM, mCurrentChapterNum);
			resultIntent.putExtra(Utilities.INTENT_CHAPTER_NEXT, true);
			setResult(RESULT_OK, resultIntent);

		} else {
			// it is circling. let us say that this is the end of chapter
			Toast.makeText(ReadActLawas.this, R.string.no_more_chapter, Toast.LENGTH_SHORT).show();

			MangaMobileStorageStub.addMangaScoreInPreference(ReadActLawas.this);
		}
		
		// add chapter
		MangaMobileStorageStub.addChapterScoreInPreference(ReadActLawas.this);
		finish();
    }
    
    protected void goToPrevChapter() {
    	// first let's check whether it is circling or not
		if (mCurrentChapterNum != 0) {
			// it is really going to another chapter
			final Intent resultIntent = new Intent();
			resultIntent.putExtra(Utilities.INTENT_CHAPTER_NUM, mCurrentChapterNum);
			resultIntent.putExtra(Utilities.INTENT_CHAPTER_PREV, true);
			setResult(RESULT_OK, resultIntent);
		} else {
			// it is circling. let us say that this is the end of chapter
			Toast.makeText(ReadActLawas.this, R.string.no_more_chapter, Toast.LENGTH_SHORT).show();
		}
		finish();
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.reader_menu, menu);

		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		final int itemId = item.getItemId();
		Intent intent = null;
		
		if (itemId == android.R.id.home) {
			finish();
		} else if (itemId == R.id.setting) {
			intent = new Intent(this, SettingAct.class);
			startActivity(intent);
		} else if (itemId == R.id.refresh) {
			final String cacheURL = mChapter.getLocalImagePath(mChapter.getSelection());
			File file = new File(cacheURL);
			final boolean fileExist = file.length() > IOUtilities.FILE_MINIMAL_SIZE;
			if (fileExist) {
				mReaderImageView.setImage(BitmapFactory.decodeFile(file.getAbsolutePath()));
			} else {
				IOUtilities.downloadImageAndSaveAsync(
						mChapter.getResolvedPage(mChapter.getSelection()), cacheURL, getApplicationContext(), true);
			}
		}
		
		return true;
	}
	
	protected void resetZoomControl() {
		// reset position and zoom
        mZoomControl.getZoomState().setZoom(1f);
		mZoomControl.getZoomState().setPanX(0.5f);
        mZoomControl.getZoomState().setPanY(0.5f);
        // experiment to fit to width
        final float aspectQuotient = mReaderImageView.getAspectQuotient().get();
        final float zoomX = mZoomControl.getZoomState().getZoomX(aspectQuotient);
        final float zoomY = mZoomControl.getZoomState().getZoomY(aspectQuotient);

        // suit scale mode
        switch (mScaleMode) {
        case 1:
        	// 1 means fit to width
        	if (zoomX < zoomY) {
        		// fit to width
        		mZoomControl.zoom(zoomY / zoomX, 0.5f, 0.5f);
            	// scroll image to top
                //mZoomControl.getZoomState().setPanY(0.25f);
        	}
        	break;
        case 2:
        	// 2 means fit to height
        	if (zoomY < zoomX) {
        		// fit to height
        		mZoomControl.zoom(zoomX / zoomY, 0.5f, 0.5f);
            	// scroll image to left
                //mZoomControl.getZoomState().setPanX(0.5f);
                //mZoomControl.getZoomState().setPanY(0.5f);
        	}
        	break;
        case 3:
        	// 3 means fit to page
        	break;
        case 4:
        	// 4 means fit to screen size
        	if (zoomX < zoomY) {
                //mZoomControl.getZoomState().setPanY(0.25f);

        		// fit to width
        		mZoomControl.getZoomState().setZoom(zoomY / zoomX);
        		//mZoomControl.zoom(zoomY / zoomX, 0.5f, 0.5f);
        	} else if (zoomY < zoomX) {
                //mZoomControl.getZoomState().setPanX(0.5f);
                //mZoomControl.getZoomState().setPanY(0.5f);
        		// fit to height
        		mZoomControl.getZoomState().setZoom(zoomX / zoomY);
        		//mZoomControl.zoom(zoomX / zoomY, 0.5f, 0.5f);
        	}
        	break;
        }
        mZoomControl.getZoomState().notifyObservers();
	}
	
	private void postProgress(final int progress) {
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				mProgress.setProgress(progress);
			}
		});
	}
    
    protected ActionBar mActionBar;

    // preference stuffs
    protected boolean mKeepAlive;
    //protected boolean mShowChapterData;
    protected int mScreenOrientation;
    protected int mScaleMode;
    
    protected int mCurrentChapterNum;
    protected RelativeLayout mReadNavigation;
	protected Chapter mChapter;
	protected ImageButton mPrevPageButton, mPrevChapterButton, mNextPageButton, mNextChapterButton;
	
	// reading view
    //protected ReaderWebView mReaderWebView;
    protected ImageZoomView mReaderImageView;
	
	protected Toast mNextPageToast, mPrevPageToast;
	
	protected int mScreenWidth;
	
	protected int mFlingMinDistance;
	protected int mFlingThresholdVelocity;
	
    /** Zoom control */
    private DynamicZoomControl mZoomControl;

    /** On touch listener for zoom view */
    private ImageViewTouchListener mZoomListener;
    
    /** Progress bar */
    private ProgressBar mProgress;
}
