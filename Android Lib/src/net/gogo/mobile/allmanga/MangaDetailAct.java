package net.gogo.mobile.allmanga;

import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.viewpagerindicator.TitlePageIndicator;

import net.gogo.mobile.allmangalib.R;
import net.gogo.mobile.allmanga.adapter.MangaDetailPagerAdapter;
import net.gogo.mobile.allmanga.fragment.MangaChapterListFrag;
import net.gogo.mobile.allmanga.fragment.MangaMetadataFrag;
import net.gogo.mobile.allmanga.helper.AllChapterDownloaderIntentService;
import net.gogo.mobile.allmanga.helper.MangaDatabaseAdapter;
import net.gogo.mobile.allmanga.helper.MangaMobileStorageStub;
import net.gogo.mobile.allmanga.helper.MangaMobileServiceStub;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Achievement;
import net.gogo.mobile.allmanga.model.Manga;
import net.gogo.mobile.allmanga.model.MangaSource;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.Toast;

public class MangaDetailAct extends SherlockFragmentActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mManga = (Manga) getLastCustomNonConfigurationInstance();
		if (mManga == null) {
			try {
				mManga = new Manga(new JSONObject(getIntent().getExtras().getString(Utilities.INTENT_MANGA)));
			} catch (JSONException jEx) {
		
			}
		}

		if (getSupportActionBar() != null) {
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setTitle(getString(R.string.manga_in_source,
				mManga.getTitle(),
				getString(MangaSource.determineMangaSourceByURL(mManga.getURL()).getName())
			));
		}

		System.gc();
	}
	
	@Override
	public Object onRetainCustomNonConfigurationInstance() {
		return mManga;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// on activity destroy, save the manga to database (to save chapter bookmark)
		new Thread() {
			public void run() {
				if (mManga == null) return;
				final boolean isBookmarked = MangaMobileStorageStub.isMangaBookmarked(getApplicationContext(), mManga);
				
				if (isBookmarked) {
					MangaMobileStorageStub.saveMangaToReadList(getApplicationContext(), mManga);
				}
			}
		}.start();
		MangaMobileStorageStub.changeAdsPolicy(getApplicationContext());
	}

	protected void setupView() {
		ViewPager pager = (ViewPager) findViewById(R.id.pager);

		// get fragment
		final FragmentManager manager = getSupportFragmentManager();
		
		if (pager != null) {
			// make title from resource
			String[] titles = {getString(R.string.manga_metadata), getString(R.string.manga_chapter_list)};
			// set pager
			try {
				MangaDetailPagerAdapter adapter = new MangaDetailPagerAdapter(manager, mManga, titles);
				pager.setAdapter(adapter);
				mMangaMetadataFrag = adapter.getMangaMetadataFragment();
			} catch (IllegalStateException iEx) {

			}

			TitlePageIndicator indicator = (TitlePageIndicator) findViewById(R.id.indicator);
			indicator.setViewPager(pager);
		} else {
			mMangaMetadataFrag = (MangaMetadataFrag)manager.findFragmentById(R.id.manga_metadata);
			MangaChapterListFrag chapterList = (MangaChapterListFrag)manager.findFragmentById(R.id.manga_chapter_list);
			
			// assign data to fragment
			if (mMangaMetadataFrag != null) mMangaMetadataFrag.setManga(mManga);
			if (chapterList != null) chapterList.setManga(mManga);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.manga_detail_menu, menu);
		
		// edit the add/remove manga to read list accordingly
		final MenuItem item = menu.findItem(R.id.remove_manga);
		// make bookmark checker on different thread
		new MangaMobileStorageStub.CheckBookmarkTask(this, mManga) {
			protected void onPostExecute(Void result) {
				// check whether this manga is bookmarked or not
				if (isBookmared()) {
					// this manga is bookmarked, let's show "want to remove manga?"
					item.setTitle(R.string.remove_manga);
				} else {
					// this manga is not bookmarked, let's show "want to bookmark?"
					item.setTitle(R.string.add_manga);
				}
			};
		}.execute();

		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		final int itemId = item.getItemId();

		// handle download all in implementation act :D
		if (itemId == android.R.id.home) {
			finish();
		} else if (itemId == R.id.refresh) {
			// clear chapter in manga to force the view to re-download the chapter list
			mManga.getChapters().clear();
			startMetadataDownload(true);
		} else if (itemId == R.id.remove_manga) {
			// check whether this manga is bookmarked or not
			new MangaMobileStorageStub.CheckBookmarkTask(this, mManga) {
				protected void onPostExecute(Void result) {
					if (isBookmared()) {
						// this manga is bookmarked, pressing the button means the manga will be removed. Let's show "want to bookmark?"
						MangaMobileStorageStub.removeMangaFromReadList(getApplicationContext(), mManga);
						item.setTitle(R.string.add_manga);
					} else {
						// this manga is not bookmarked, pressing the button means the manga will be added. Let's show "want to remove bookmark?"
						
						// check whether his achievement allow it
						SharedPreferences pref = getSharedPreferences(Utilities.PREFERENCES, 0);
						
						Achievement achievement = new Achievement(pref.getString(Utilities.ACHIEVEMENT, ""));
						int mangaInReadList = MangaMobileStorageStub.getUsedReadLimit(getApplicationContext());

						if (mangaInReadList < achievement.getReadListLimit()) {
							MangaMobileStorageStub.saveMangaToReadList(getApplicationContext(), mManga);
							item.setTitle(R.string.remove_manga);
							Toast.makeText(MangaDetailAct.this, getString(R.string.add_manga_to_read_list, mManga.getTitle()), Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(MangaDetailAct.this, R.string.cannot_add_manga_because_limit, Toast.LENGTH_SHORT).show();
						}
					}
				};
			}.execute();
		} else if (itemId == R.id.setting) {
			Intent intent = new Intent(this, SettingAct.class);
			startActivity(intent);
		}

		return true;
	}
	
	protected void startMetadataDownload(final boolean forced) {
		setupView();
		
		new Thread() {
			public void run() {
				if (!forced) {
					// try fetching data from database first
					Manga manga = MangaMobileStorageStub.getDetailedManga(getApplicationContext(), mManga.getURL());

					// if can use from DB instead, why not?
					if (manga != null) {
						mManga = manga;
					}
				}
				
				int retry = 0;
				while (mManga.getChapters().size() == 0 && retry < 3) {
					MangaMobileServiceStub.downloadMangaMetadata(mManga);
					++retry;
				}
				
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (mManga.getChapters().isEmpty()) {
							Toast.makeText(MangaDetailAct.this, R.string.connection_error, Toast.LENGTH_SHORT).show();
						}
						// finish up by updating view
						setupView();
					}
				});
			}
		}.start();
	}

	protected Manga mManga;
	// save it for free/paid version
	protected MangaMetadataFrag mMangaMetadataFrag;
}
