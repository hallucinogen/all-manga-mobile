package net.gogo.mobile.allmanga;

import java.util.ArrayList;
import java.util.Collections;

import net.gogo.mobile.allmanga.adapter.MangaGridAdapter;
import net.gogo.mobile.allmanga.adapter.MangaSourceGalleryAdapter;
import net.gogo.mobile.allmanga.helper.MangaMobileServiceStub;
import net.gogo.mobile.allmanga.helper.MangaMobileStorageStub;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Manga;
import net.gogo.mobile.allmanga.model.UnifiedManga;
import net.gogo.mobile.allmanga.view.ShelvesView;
import net.gogo.mobile.allmangalib.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public abstract class MangaShelvesAct extends SherlockFragmentActivity implements OnItemClickListener, OnTouchListener {
	private final static String TAG = MangaShelvesAct.class.getSimpleName();

	@Override
	protected void onResume() {
		super.onResume();
		// populate the view!
		// if it is read list, populate everytime
		if (mSearchQuery == null) {
			// want to show by read list
			initializeModelByReadList();
		}
	}

	protected void initializeModelBySearch() {
		mProgress.setMessage("Searching manga...\n\n" + Utilities.getRandomTips());
		mProgress.show();
		new Thread() {
			public void run() {
				Thread.currentThread().setPriority(Thread.NORM_PRIORITY);

				mMangas = MangaMobileServiceStub.searchManga(mSearchQuery, mIncludeGenre, mExcludeGenre, mSelectedSources);

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (mProgress != null && mProgress.isShowing()) {
							try {
								mProgress.dismiss();
							} catch (IllegalStateException iEx) {
								
							} catch (IllegalArgumentException iEx) {
								
							}
						}
						initializeView();
					}
				});
			};
		}.start();
	}
	
	protected void initializeModelByReadList() {
		// decode from database
		//final SharedPreferences pref = getSharedPreferences(Utilities.PREFERENCES, 0);
		
		mProgress.setMessage("Getting manga from your read list...\n\n" + Utilities.getRandomTips());
		mProgress.show();
		new Thread() {
			public void run() {
				Thread.currentThread().setPriority(Thread.NORM_PRIORITY);

				//mMangas = MangaMobilePreferenceStub.getReadListFromPreferences(pref);

				mMangas = MangaMobileStorageStub.getReadListFromStorage(getApplicationContext());

				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						try {
							if (mProgress != null && mProgress.isShowing()) {
								mProgress.dismiss();
							}
						} catch (IllegalArgumentException ilEx) {
							Log.w(TAG, "Some error occured but handled gracefully :3");
						}
						initializeView();
					}
				});
			};
		}.start();
	}

	protected void initializeView() {
		// don't persist search image, but persist in gallery
		mShelvesView.setAdapter(new MangaGridAdapter(this, mMangas, mSearchQuery == null));
		mShelvesView.setOnItemClickListener(this);
		mShelvesView.setOnTouchListener(this);
		
		mMangaSourcePopup.setOnTouchListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
		final UnifiedManga unifiedManga = (UnifiedManga)view.getTag();

		// dismiss popup
		dismissAllPopup();

		showMangaSourcePopup(view);

		// let's stop the marquee
		if (mLastSelectedView != null) {
			mLastSelectedView.findViewById(R.id.title).setSelected(false);
		}
		// start marquee on new view
		view.findViewById(R.id.title).setSelected(true);
		mLastSelectedView = view;

		// make manga sources
		mMangaSources.setAdapter(new MangaSourceGalleryAdapter(this, unifiedManga));
		mMangaSources.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position,
					long id) {
				Manga manga = (Manga)view.getTag();

				onMangaSourceClicked(manga);
			}
		});
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (v == mMangaSourcePopup) return mMangaSourcePopup.onTouchEvent(event) || true;
		
		// we must hide the popup when the touch points to nothing
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			// detect what item is being touched by our hand
			int id = mShelvesView.pointToPosition((int)event.getX(), (int)event.getY());
			if (id == -1) {
				// nothing is selected, then hide it!!
				dismissAllPopup();
			}
		}
		
		// prevent lags
		try {
			Thread.sleep(16);
		} catch (InterruptedException iEx) {
			
		}
		
		return v.onTouchEvent(event);
	}
	
	private void showMangaSourcePopup(View anchorView) {
		mMangaSourcePopupWindow.showAsDropDown(anchorView);
	}
	
	private void dismissAllPopup() {
		mMangaSourcePopupWindow.dismiss();
	}

	@Override
	public void onBackPressed() {
		// attempt to hide the popup first
		if (mMangaSourcePopupWindow.isShowing()) {
			dismissAllPopup();
		} else {
			super.onBackPressed();
		}
	}
	
	@Override
	protected void onPause() {
		dismissAllPopup();
		super.onPause();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.manga_shelves_menu, menu);

		return true;
	}

	public abstract void onMangaSourceClicked(Manga manga);

	// should implement onOptionsItemSelected(MenuItem)

	// popup stuffs
	protected PopupWindow mMangaSourcePopupWindow;
	protected RelativeLayout mMangaSourcePopup;
	protected Gallery mMangaSources;

	// search stuffs
	protected String mSearchQuery;
	protected ArrayList<String> mIncludeGenre, mExcludeGenre, mSelectedSources;
	
	protected ArrayList<Manga> mMangas;
	protected ShelvesView mShelvesView;
	
	protected View mLastSelectedView;
	
	protected ProgressDialog mProgress;
}
