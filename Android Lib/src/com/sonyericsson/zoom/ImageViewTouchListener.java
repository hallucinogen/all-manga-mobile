/*
 * Copyright (c) 2010, Sony Ericsson Mobile Communication AB. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright notice, this 
 *      list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *    * Neither the name of the Sony Ericsson Mobile Communication AB nor the names
 *      of its contributors may be used to endorse or promote products derived from
 *      this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.sonyericsson.zoom;

import android.content.Context;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;

/**
 * Listener for controlling zoom state through touch events
 */
public class ImageViewTouchListener implements View.OnTouchListener {

    /**
     * Enum defining listener modes. Before the view is touched the listener is
     * in the UNDEFINED mode. Once touch starts it can enter either one of the
     * other two modes: If the user scrolls over the view the listener will
     * enter PAN mode, if the user lets his finger rest and makes a longpress
     * the listener will enter ZOOM mode.
     */
    private enum Mode {
        UNDEFINED, PAN, ZOOM
    }

    /** Time of tactile feedback vibration when entering zoom mode */
    private static final long VIBRATE_TIME = 50;

    /** Current listener mode */
    private Mode mMode = Mode.UNDEFINED;

    /** Zoom control to manipulate */
    private DynamicZoomControl mZoomControl;

    /** X-coordinate of previously handled touch event */
    private float[] mX = new float[3];

    /** Y-coordinate of previously handled touch event */
    private float[] mY = new float[3];

    /** X-coordinate of latest down event */
    private float[] mDownX = new float[3];

    /** Y-coordinate of latest down event */
    private float[] mDownY = new float[3];
    
    /** Time of the first down event */
    private long mLastDownTime;
    
    /** boolean to determine whether we're still tapping or not */
    private boolean mTapping;

    /** Velocity tracker for touch events */
    private VelocityTracker mVelocityTracker;

    /** Distance touch can wander before we think it's scrolling */
    private final int mScaledTouchSlop;
    
    /** Duration in ms before a touch down doesn't count as a tap */
    private final int mTapThreshold;
    
    /** Duration in ms before a press turns into a long press */
    private final int mLongPressTimeout;

    /** Vibrator for tactile feedback */
    private final Vibrator mVibrator;

    /** Maximum velocity for fling */
    private final int mScaledMaximumFlingVelocity;
    
    /** Minimum velocity for fling */
    private final int mScaledMinimumFlingVelocity;
    
    /** Gesture listener */
    private OnGestureListener mGestureListener;
    
    /** HACK HACK HACK HACK */
    private boolean mHack;

    /**
     * Creates a new instance
     * 
     * @param context Application context
     */
    public ImageViewTouchListener(Context context) {
        mLongPressTimeout = ViewConfiguration.getLongPressTimeout();
        mScaledTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        mScaledMaximumFlingVelocity = ViewConfiguration.get(context)
                .getScaledMaximumFlingVelocity();
        mScaledMinimumFlingVelocity = ViewConfiguration.get(context)
        		.getScaledMinimumFlingVelocity();
        mVibrator = (Vibrator)context.getSystemService("vibrator");
        mTapThreshold = ViewConfiguration.getTapTimeout();
    }

    /**
     * Sets the zoom control to manipulate
     * 
     * @param control Zoom control
     */
    public void setZoomControl(DynamicZoomControl control) {
        mZoomControl = control;
    }

    /**
     * Runnable that enters zoom mode
     */
    private final Runnable mLongPressRunnable = new Runnable() {
        public void run() {
            mMode = Mode.ZOOM;
            mVibrator.vibrate(VIBRATE_TIME);
        }
    };

    // implements View.OnTouchListener
    public boolean onTouch(View v, MotionEvent event) {
        final int action = event.getAction();
        final float x = event.getX();
        final float y = event.getY();
        final int pointerCount = event.getPointerCount();
        final float viewWidth = v.getWidth();
        final float viewHeight = v.getHeight();

        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mZoomControl.stopFling();
                // 
                v.postDelayed(mLongPressRunnable, mLongPressTimeout);
                mDownX[0] = x;
                mDownY[0] = y;
                mX[0] = x;
                mY[0] = y;
                mLastDownTime = System.currentTimeMillis();
                mTapping = true;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
            	if (pointerCount <= 3) {
            		for (int i = 0; i < pointerCount; ++i) {
            			mX[i] = event.getX(i);
            			mY[i] = event.getY(i);
            			mDownX[i] = mX[i];
            			mDownY[i] = mY[i];
            		}
            	}
            	mTapping = mTapping && pointerCount > 1;
            	break;

            case MotionEvent.ACTION_MOVE: {
                float dx = (x - mX[0]) / viewWidth;
                float dy = (y - mY[0]) / viewHeight;

                switch (pointerCount) {
                case 3:
                case 2:
                	// get center
                	final float centerX = (mDownX[0] + mDownX[1]) / 2.0f;
                	final float centerY = (mDownY[0] + mDownY[1]) / 2.0f;

                	final float dX1 = mX[0] - mX[1];
                	final float dY1 = mY[0] - mY[1];
                	
                	for (int i = 0; i < pointerCount; ++i) {
                		mX[i] = event.getX(i);
                		mY[i] = event.getY(i);
                	}

                	final float dX2 = mX[0] - mX[1];
                	final float dY2 = mY[0] - mY[1];

                	// Zoom fail. Doesn't know why
                	final float dZoom =
                		((float)Math.sqrt(dX2 * dX2 + dY2 * dY2) - (float)Math.sqrt(dX1 * dX1 + dY1 * dY1)) /
                		(float)Math.sqrt(viewWidth * viewWidth + viewHeight * viewHeight);

                	//android.util.Log.e("Zoom", "Zoom = " + dZoom);
                	if (mHack) {
                		mHack = false;
                		break;
                	}
                	mZoomControl.zoom((float)Math.pow(20, dZoom), centerX / viewWidth, centerY
                            / viewHeight);
                	break;
                case 1:
                    final float scrollX = mDownX[0] - x;
                    final float scrollY = mDownY[0] - y;
	                if (mMode == Mode.ZOOM) {
	                	//android.util.Log.e("Zoom", "Zoom = " + -dy);
	                    mZoomControl.zoom((float)Math.pow(20, -dy), mDownX[0] / viewWidth, mDownY[0]
	                            / viewHeight);
	                } else if (mMode == Mode.PAN) {
	                	if (Math.abs(scrollX) >= mScaledTouchSlop) {
	                		mZoomControl.pan(-dx, 0);
	                	}
	                	if (Math.abs(scrollY) >= mScaledTouchSlop) {
	                		mZoomControl.pan(0, -dy);
	                	}
	                } else {
	
	                    final float dist = (float)Math.sqrt(scrollX * scrollX + scrollY * scrollY);
	
	                    if (dist >= mScaledTouchSlop) {
	                        v.removeCallbacks(mLongPressRunnable);
	                        mMode = Mode.PAN;
	                        mTapping = false;
	                    }
	                }
	
	                mX[0] = x;
	                mY[0] = y;
	                
	                break;
                }
                break;
            }

            case MotionEvent.ACTION_POINTER_UP:
            	

            	break;

            case MotionEvent.ACTION_UP:
            	mVelocityTracker.computeCurrentVelocity(1000, mScaledMaximumFlingVelocity);
            	final float vX = mVelocityTracker.getXVelocity();
            	final float vY = mVelocityTracker.getYVelocity();

            	if (System.currentTimeMillis() - mLastDownTime < mTapThreshold && mTapping) {
            		if (mGestureListener != null) {
            			mGestureListener.onSingleTap(event);
            		}
            	}
                if (mMode == Mode.PAN) {
                	mZoomControl.startFling(-vX / v.getWidth(), -vY / v.getHeight());

                	// fling when in threshold
                	if (Math.abs(vX) > (mScaledMinimumFlingVelocity + mScaledMaximumFlingVelocity) * 0.2f) {
                		// fling only when in maximum / minimum position
                		if (mGestureListener != null && !mZoomControl.isWithinLimitsX()) {
                			mGestureListener.onFling(vX > 0);
                			// return page to center
                			mZoomControl.startFling(0, 0);
                		}
                	}
                } else {
                    mZoomControl.startFling(0, 0);
                }
                mVelocityTracker.recycle();
                mVelocityTracker = null;
                v.removeCallbacks(mLongPressRunnable);
                mMode = Mode.UNDEFINED;
                mTapping = false;

                // HACK
                mHack = true;
                break;

            default:
                mVelocityTracker.recycle();
                mVelocityTracker = null;
                v.removeCallbacks(mLongPressRunnable);
                mMode = Mode.UNDEFINED;
                break;
        }

        return true;
    }

    public void setOnGestureListener(OnGestureListener listener) {
    	mGestureListener = listener;
    }

    public interface OnGestureListener {
    	public void onSingleTap(MotionEvent event);
    	public void onFling(boolean isLeft);
    	public void onTripleFling(boolean isLeft);
    }
}
