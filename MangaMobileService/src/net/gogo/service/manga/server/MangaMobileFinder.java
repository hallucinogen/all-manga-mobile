package net.gogo.service.manga.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.Expiration;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;

import net.gogo.service.manga.helper.Utilities;
import net.gogo.service.manga.model.Manga;
import net.gogo.service.manga.model.MangaSource;
import net.gogo.service.manga.model.MangaSourceManager;
import net.gogo.service.manga.model.SourceGoodManga;
import net.gogo.service.manga.model.SourceMangaReader;

public class MangaMobileFinder extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MangaMobileFinder() {
		
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		// get request URL
		final String requestURL = req.getQueryString();
		MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();
		syncCache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
		String resultString = (String)syncCache.get(requestURL);

		// if cache hit!
		if (resultString != null) {
			resp.getWriter().print(resultString);
			return;
		}

		// get Manga Source from Manga Reader
		final String title = req.getParameter("title");
		final String genre = req.getParameter("genre");
		final String exclude = req.getParameter("exclude");
		final String source = req.getParameter("source");

		final String[] genres = genre == null ? null : genre.split("_");
		final String[] excludes = exclude == null ? new String[0] : exclude.split("_");
		final String[] rawSources = source == null ? new String[0] : source.split("___");
		boolean saveInCache = true;
		if (genres != null || excludes != null) {
			saveInCache = false;
		}

		// filter empty sources
		ArrayList<String> filterSources = new ArrayList<String>();
		for (int i = 0; i < rawSources.length; ++i) {
			if (rawSources[i].trim().isEmpty()) continue;
			
			filterSources.add(rawSources[i]);
		}
		final String[] filteredSources = new String[filterSources.size()];
		filterSources.toArray(filteredSources);

		ArrayList<Manga> mangas = MangaDataStore.instance().searchManga(title, genres, excludes, filteredSources);

		try {
			System.out.println("Manga size = " + mangas.size());
			JSONObject json = new JSONObject();
			JSONArray array = new JSONArray();
			for (int i = 0; i < mangas.size(); ++i) {
				array.put(mangas.get(i).toJSON());
			}
			json.put("results", array);
			resultString = json.toString();
			resp.getWriter().print(resultString);

			if (saveInCache) {
				// set expiration to 1 week
				Expiration exp = Expiration.byDeltaSeconds(3600 * 24 * 7);
				syncCache.put(requestURL, resultString, exp);
			}
		} catch (JSONException jEx) {
			
		}
	}
}
