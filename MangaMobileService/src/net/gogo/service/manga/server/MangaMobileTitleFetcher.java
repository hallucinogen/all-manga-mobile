package net.gogo.service.manga.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.gogo.service.manga.model.Manga;
import net.gogo.service.manga.model.MangaSource;
import net.gogo.service.manga.model.MangaSourceManager;
import net.gogo.service.manga.model.SourceAnimeA;
import net.gogo.service.manga.model.SourceBatoto;
import net.gogo.service.manga.model.SourceBatotoFrench;
import net.gogo.service.manga.model.SourceBatotoGerman;
import net.gogo.service.manga.model.SourceBatotoIndonesian;
import net.gogo.service.manga.model.SourceBatotoItalian;
import net.gogo.service.manga.model.SourceBatotoMalay;
import net.gogo.service.manga.model.SourceBatotoSpanish;
import net.gogo.service.manga.model.SourceGoodManga;
import net.gogo.service.manga.model.SourceMangaHere;
import net.gogo.service.manga.model.SourceMangaPanda;
import net.gogo.service.manga.model.SourceMangaReader;
import net.gogo.service.manga.model.SourceMangaFox;

public class MangaMobileTitleFetcher extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MangaMobileTitleFetcher() {
		
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		final String source = req.getParameter("source");
		if (source.equals(new SourceMangaReader().mangaSourceIdentifier())) {
			new SourceMangaReader().downloadCompleteMangaList();
		} else if (source.equals(new SourceGoodManga().mangaSourceIdentifier())) {
			new SourceGoodManga().downloadCompleteMangaList();
		} else if (source.equals(new SourceBatoto().mangaSourceIdentifier())) {
			new SourceBatoto().downloadCompleteMangaList();
		} else if (source.equals(new SourceBatotoFrench().mangaSourceIdentifier())) {
			new SourceBatotoFrench().downloadCompleteMangaList();
		} else if (source.equals(new SourceBatotoGerman().mangaSourceIdentifier())) {
			new SourceBatotoGerman().downloadCompleteMangaList();
		} else if (source.equals(new SourceBatotoIndonesian().mangaSourceIdentifier())) {
			new SourceBatotoIndonesian().downloadCompleteMangaList();
		} else if (source.equals(new SourceBatotoItalian().mangaSourceIdentifier())) {
			new SourceBatotoItalian().downloadCompleteMangaList();
		} else if (source.equals(new SourceBatotoMalay().mangaSourceIdentifier())) {
			new SourceBatotoMalay().downloadCompleteMangaList();
		} else if (source.equals(new SourceBatotoSpanish().mangaSourceIdentifier())) {
			new SourceBatotoSpanish().downloadCompleteMangaList();
		} else if (source.equals(new SourceAnimeA().mangaSourceIdentifier())) {
			new SourceAnimeA().downloadCompleteMangaList();
		} else if (source.equals(new SourceMangaPanda().mangaSourceIdentifier())) {
			new SourceMangaPanda().downloadCompleteMangaList();
		} else if (source.equals(new SourceMangaHere().mangaSourceIdentifier())) {
			new SourceMangaHere().downloadCompleteMangaList();
		} else if (source.equals(new SourceMangaFox().mangaSourceIdentifier())) {
			new SourceMangaFox().downloadCompleteMangaList();
		} else if (source.equals("diablo-the-lord-of-terror")) {
			// download all
			ArrayList<MangaSource> allSources = MangaSourceManager.allInstance().getMangaSources();

			for (MangaSource mangaSource :allSources) {
				mangaSource.downloadCompleteMangaList();
			}
		}

		resp.getWriter().println("berhasil!");
	}
}
