package net.gogo.service.manga.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.gogo.service.manga.model.Manga;
import net.gogo.service.manga.model.MangaSource;
import net.gogo.service.manga.model.MangaSourceManager;
import net.gogo.service.manga.model.SourceMangaReader;

public class MangaMobileDeleter extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MangaMobileDeleter() {
		
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		MangaDataStore.instance().clearMangas();
		resp.getWriter().println("berhasil!");
	}
	
	private int mTestIncrement = 0;
}
