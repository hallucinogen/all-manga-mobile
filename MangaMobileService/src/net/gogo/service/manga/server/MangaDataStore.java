package net.gogo.service.manga.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import net.gogo.service.manga.model.Manga;

public class MangaDataStore {
	private MangaDataStore() {
		mPMF = JDOHelper.getPersistenceManagerFactory("transactions-optional");
	}
	
	public void clearMangas() {
		PersistenceManager em = mPMF.getPersistenceManager();

		ArrayList<Manga> mangas = getMangas();

		for (int i = 0, n = mangas.size(); i < n; ++i) {
			em.deletePersistent(mangas.get(i));
		}

		em.close();
	}
	
	public ArrayList<Manga> getMangas() {
		PersistenceManager em = mPMF.getPersistenceManager();
		
		Query query = em.newQuery(Manga.class);
		
		return new ArrayList<Manga>((List<Manga>)query.execute());
	}
	
	public ArrayList<Manga> searchManga(String title, final String[] genres, final String[] excludes, final String[] sources) {
		PersistenceManager em = mPMF.getPersistenceManager();
		ArrayList<Manga> allFilteredManga = new ArrayList<Manga>();


		for (int j = 0; sources != null && j < sources.length; ++j) {
			// check if sources contains language
			final String[] clearSource = sources[j].split("__");
			final String source = clearSource[0];
			final String language = clearSource.length > 1 ? clearSource[1] : null;
			final String sourceIdentifier = source + (language == null ? "" : "-" + language);

			// make query
			Query query = em.newQuery(Manga.class);
			String filter = "";

			// filter by title
			if (title != null) {
				title = title.toLowerCase();
				String[] tags = title.split("(?!^)\\b");
				filter += "mTags.contains(\""+ tags[0] +"\")";
				for (int i = 1; i < tags.length; ++i) {
					filter += "&& mTags.contains(\""+ tags[i] +"\")";
				}
			}

			// filter by genre
			if (genres != null) {
				if (!filter.equals("")) filter += "&& ";
				filter += "mGenre.contains(\""+ genres[0] +"\")";
				for (int i = 1; i < genres.length; ++i) {
					filter += "&& mGenre.contains(\""+ genres[i] +"\")";
				}
			}

			// filter by source
			if (!filter.equals("")) filter += "&& ";
			filter += "mMangaSource == \""+ sourceIdentifier +"\"";

			query.setFilter(filter);
			// get only 100 data
			//query.setRange(0, 100);
			ArrayList<Manga> filteredManga = !filter.isEmpty() ? new ArrayList<Manga>((List<Manga>)query.execute()) : new ArrayList<Manga>();
			// filter by exclude genre

			allFilteredManga.addAll(filteredManga);
		}

		// fallback version
		if (allFilteredManga.size() == 0) {
			// make query
			Query query = em.newQuery(Manga.class);
			String filter = "";

			// filter by title
			if (title != null) {
				title = title.toLowerCase();
				String[] tags = title.split("(?!^)\\b");
				filter += "mTags.contains(\""+ tags[0] +"\")";
				for (int i = 1; i < tags.length; ++i) {
					filter += "&& mTags.contains(\""+ tags[i] +"\")";
				}
			}

			// filter by genre
			if (genres != null) {
				if (!filter.equals("")) filter += "&& ";
				filter += "mGenre.contains(\""+ genres[0] +"\")";
				for (int i = 1; i < genres.length; ++i) {
					filter += "&& mGenre.contains(\""+ genres[i] +"\")";
				}
			}

			query.setFilter(filter);
			// get only 100 data
			//query.setRange(0, 100);
			ArrayList<Manga> filteredManga = !filter.isEmpty() ? new ArrayList<Manga>((List<Manga>)query.execute()) : new ArrayList<Manga>();
			allFilteredManga.addAll(filteredManga);
		}

		ArrayList<Manga> deletedManga = new ArrayList<Manga>();
		for (int i = 0, n = allFilteredManga.size(); i < n; ++i) {
			boolean deleted = false;
			final Manga manga = allFilteredManga.get(i);
			final ArrayList<String> mangaGenres = manga.getGenre(); 
			for (int j = 0; excludes != null && j < excludes.length; ++j) {
				if (mangaGenres.contains(excludes[j])) {
					deletedManga.add(manga);
					deleted = true;
					break;
				}
			}

			// filter by manga source, fallback version
			if (deleted) continue;
			boolean found = sources == null || sources.length == 0;
			for (int j = 0; sources != null && j < sources.length; ++j) {
				// check if sources contains language
				final String[] clearSource = sources[j].split("__");
				final String source = clearSource[0];
				final String language = clearSource.length > 1 ? clearSource[1] : null;
				final String mangaLanguage = manga.getLanguage() == null ? "" : manga.getLanguage();

				if (manga.getUrl().contains(source) && (language == null || (mangaLanguage.equalsIgnoreCase(language)))) {
					found = true;
					break;
				}
			}

			if (!found) {
				deletedManga.add(manga);
				deleted = true;
			}
		}
		
		for (int i = 0, n = deletedManga.size(); i < n; ++i) {
			allFilteredManga.remove(deletedManga.get(i));
		}

		// remove the description (to ease network)
		for (int i = 0, n = allFilteredManga.size(); i < n; ++i) {
			allFilteredManga.get(i).setDescription("");
		}

		// sort manga by title
		Collections.sort(allFilteredManga);

		return allFilteredManga;
	}
	
	public void addManga(Manga manga) {
		synchronized (this) {
			PersistenceManager em = mPMF.getPersistenceManager();
			
			try {
				em.makePersistent(manga);
			} finally {
				em.close();
			}
		}
	}
	
	public Manga getManga(String url) {
		Manga manga = null;
		synchronized (this) {
			PersistenceManager em = mPMF.getPersistenceManager();
			
			try {
				manga = em.getObjectById(Manga.class, url);
			} catch (Exception ex) {
				return null;
			} finally {
				em.close();
			}
		}

		return manga;
	}
	
	public void removeManga(String url) {

		PersistenceManager em = mPMF.getPersistenceManager();
		try {
			Manga manga = em.getObjectById(Manga.class, url);
			em.deletePersistent(manga);
		} finally {
			em.close();
		}
	}
	
	public static MangaDataStore instance() {
		if (sInstance == null) {
			sInstance = new MangaDataStore();
		}
		return sInstance;
	}
	
	private PersistenceManagerFactory mPMF;
	
	private static MangaDataStore sInstance;
	
}
