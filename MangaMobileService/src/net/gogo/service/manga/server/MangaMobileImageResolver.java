package net.gogo.service.manga.server;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.memcache.ErrorHandlers;
import com.google.appengine.api.memcache.Expiration;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;

import net.gogo.service.manga.model.Chapter;
import net.gogo.service.manga.model.Manga;
import net.gogo.service.manga.model.MangaSource;
import net.gogo.service.manga.model.MangaSourceManager;
import net.gogo.service.manga.model.SourceGoodManga;
import net.gogo.service.manga.model.SourceMangaReader;

public class MangaMobileImageResolver extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8677981330603364022L;

	public MangaMobileImageResolver() {
		
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// get request URL
		final String requestURL = req.getQueryString();
		MemcacheService syncCache = MemcacheServiceFactory.getMemcacheService();
		syncCache.setErrorHandler(ErrorHandlers.getConsistentLogAndContinue(Level.INFO));
		String resultString = (String)syncCache.get(requestURL);

		// if cache hit!
		if (resultString != null) {
			resp.getWriter().print(resultString);
			return;
		}

		String title = req.getParameter("title");
		if (title == null) title = "";
		String url 		= req.getParameter("url");
		if (url == null) {
			url = "";
		} else {
			url = URLDecoder.decode(url, "UTF-8");
		}
		final String pageURL 	= URLDecoder.decode(req.getParameter("page_url"), "UTF-8");
		
		Chapter chapter = new Chapter(url, null, title);

		resultString = chapter.downloadImage(pageURL);
		resp.getWriter().println(resultString);
		// set expiration to 1 week
		Expiration exp = Expiration.byDeltaSeconds(3600 * 24 * 7);
		syncCache.put(requestURL, resultString, exp);
	}
}
