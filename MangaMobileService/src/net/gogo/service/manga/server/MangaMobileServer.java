package net.gogo.service.manga.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.gogo.service.manga.helper.Utilities;
import net.gogo.service.manga.model.Manga;
import net.gogo.service.manga.model.MangaSource;
import net.gogo.service.manga.model.MangaSourceManager;
import net.gogo.service.manga.model.SourceMangaReader;

public class MangaMobileServer extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MangaMobileServer() {
		
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//ArrayList<Manga> mangas = MangaDataStore.instance().getMangas();
		//for (int i = 0; i < mangas.size(); ++i) {
		//	resp.getWriter().println(mangas.get(i).getTitle() + " " + mangas.get(i).getUrl());
		//}
		//resp.getWriter().println("Saya tampan luar biasa" + MangaDataStore.instance().getMangasContainString("Naruto").size());

		//resp.getWriter().println((mTestIncrement++) + " " + mangas.size() );
		String ads = req.getParameter("ads");
		if (ads != null) {
			Utilities.IS_ADS_ENABLED = Boolean.parseBoolean(ads);
		}

		resp.getWriter().println("Ads enabled? " + Utilities.IS_ADS_ENABLED);
	}
}
