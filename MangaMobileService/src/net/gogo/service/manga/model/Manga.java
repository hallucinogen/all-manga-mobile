package net.gogo.service.manga.model;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import net.gogo.service.manga.helper.Utilities;
import net.gogo.service.manga.server.MangaDataStore;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Manga implements Comparable<Manga> {
	private static String TAG = "MANGA";

	@PrimaryKey
	public Key encodedKey;
	
	public Manga(String title, String inURL){
		mTitle 			= title;
		url				= inURL;
		mChapters		= new ArrayList<Chapter>();
		mGenre			= new ArrayList<String>();
		mRecommended 	= new ArrayList<Manga>();
		encodedKey		= KeyFactory.createKey(Manga.class.getSimpleName(), url);
		mAuthor			= "";
		
		// make tags from title for search
		final String[] tags = mTitle.toLowerCase().split("(?!^)\\b");
		mTags	= new ArrayList<String>();

		for (int i = 0; i < tags.length; ++i) {
			mTags.add(tags[i]);
		}

		// determine manga source from URL
		ArrayList<MangaSource> sources = MangaSourceManager.allInstance().getMangaSources();
		for (MangaSource source : sources) {
			if (source.isUsingTheMangaSource(inURL)) {
				mMangaSource = source.mangaSourceIdentifier();
			}
		}
	}

	/**
	 * download Manga metadata
	 * Metadata including :
	 * - all chapter url
	 * - manga summary
	 * - genre
	 */
	public void downloadMangaMetadata(boolean downloadDetailed, String language){
		downloadChapterListAndGenre(downloadDetailed, language);
		resolveMangaupdates(downloadDetailed);
		//Collections.sort(mChapterURLs);
	}

	public void downloadChapterListAndGenre(boolean downloadDetailed, String language) {
		MangaSourceManager.allInstance().downloadChapterListAndGenre(this, downloadDetailed, language);
	}
	
	private void resolveMangaupdates(boolean downloadDetailed) {
		if (!downloadDetailed) {
			downloadMangaupdates();
		} else {
			// if it request detailed manga and manga from database has it, let it get from DB
			Manga mangaFromDatabase = null;//MangaDataStore.instance().getManga(url);
			Document mangaUpdatesDoc = null;

			boolean gagal = true;
			int attempt = 0;

			while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
				try {
					++attempt;
					if (mangaFromDatabase != null && mangaFromDatabase.mMangaUpdatesURL != null) {
						Connection con = Jsoup.connect(mangaFromDatabase.mMangaUpdatesURL);
						Response resp = con.execute();
						String html = new String(resp.bodyAsBytes(), "ISO-8859-1");
						mangaUpdatesDoc = Jsoup.parse(html);
						
						if (mangaUpdatesDoc == null) {
							mangaUpdatesDoc = downloadMangaupdates();
						} else {
							// fetch data from mangaFromDatabase
							mRating = mangaFromDatabase.mRating;
							mAuthor = mangaFromDatabase.mAuthor;
							mStatusAtOrigin = mangaFromDatabase.mStatusAtOrigin;
							mStatusScanlation = mangaFromDatabase.mStatusScanlation;
						}
					} else {
						// redownload from mangaupdates, and assign some variables
						mangaUpdatesDoc = downloadMangaupdates();
					}

					gagal = false;
				} catch (IOException iEx) {
				} catch (UnsupportedCharsetException unEx) {
					// TODO: have to do something, but let's fall back for now
					++attempt;
					break;
				}
			}

			// get recommended manga
			mRecommended.clear();
			
			if (mangaUpdatesDoc == null) return;

			Elements recommendeds = mangaUpdatesDoc.select("div[id=div_recom_link]").select("u");
			for (Element recommended : recommendeds) {
				final String recommendation = recommended.text();

				if (recommendation.equals("More...")) continue;
				// search manga and exclude doujinshi
				mRecommended.addAll(MangaDataStore.instance().searchManga(recommended.text(),
						null,
						new String[] {"Doujinshi"},
						new String[] {"goodmanga", "batoto", "mangapanda", "mangahere"}));
			}
		}
	}

	private Document downloadMangaupdates() {
		mRating = 0;
		Document retval = null;

		// test rating if it is manga or manhwa or doujinshi
		for (int ratingType = 0; ratingType < Utilities.MANGAUPDATES_QUERY.length; ++ratingType) {
			// if it is Doujinshi, only search against Doujinshi query
			if (mGenre.contains("Doujinshi")) {
				if (ratingType != 2) {
					continue;
				}
			} else {
				if (ratingType == 2) {
					continue;
				}
			}

			String ratingQuery = Utilities.MANGAUPDATES_QUERY[ratingType] + URLEncoder.encode(mTitle);
			boolean gagal = true;
			String url = null;
			int attempt = 0;

			gagal = true;
			while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
				try {
					Connection con = Jsoup.connect(ratingQuery);
					Response resp = con.execute();
					String html = new String(resp.bodyAsBytes(), "ISO-8859-1");
					Document soup = Jsoup.parse(html);
					gagal = false;

					Element manga = soup.select("table[class=text series_rows_table]").select("td[class=text pad col1]").first().select("a").first();

					if (manga != null) {
						url = manga.attr("href");
					}

					// clean up
					con = null;
					resp = null;
					soup = null;
				} catch (IOException iEx) {
					++attempt;
				} catch (NullPointerException nEx) {
					url = null;
					++attempt;
				} catch (UnsupportedCharsetException unEx) {
					url = null;
					break;
				}
			}
			
			// get url from mangaupdates
			System.out.println("URL = " + url);

			if (url == null) continue;

			Document mangaUpdatesDoc = null;
			gagal = true;
			while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
				try {
					Connection con = Jsoup.connect(url);
					Response resp = con.execute();
					String html = new String(resp.bodyAsBytes(), "ISO-8859-1");
					mangaUpdatesDoc = Jsoup.parse(html);
					gagal = false;

					// clean up
					con = null;
					resp = null;
					html = null;
				} catch (IOException iEx) {
					++attempt;
				}
			}

			if (gagal || mangaUpdatesDoc == null) continue;
			
			final float newRating = resolveRating(mangaUpdatesDoc);
			
			// check which is the best URL
			if (mRating < newRating || retval == null) {
				mRating = newRating;

				// get author and artist
				resolveAuthorAndArtist(mangaUpdatesDoc);
				resolveStatus(mangaUpdatesDoc);

				mMangaUpdatesURL = url;

				retval = mangaUpdatesDoc;
			}
		}
		
		return retval;
	}
	
	private float resolveRating(Document mangaUpdatesDoc) {
		// get rating!
		
		//final String html = mangaUpdatesDoc.toString();

		Element ratingRaw = null;
		try {
			ratingRaw = mangaUpdatesDoc.select("div[class=sContent]:containsOwn(" + Utilities.RATING_STRING + ")").select("b").first();
		} catch (Exception ex) {
			
		}
		//int posRating = html.indexOf(Utilities.RATING_STRING);
		String rating = ratingRaw != null ? ratingRaw.html() : "";
		/*if (posRating > 0) {
			posRating += Utilities.RATING_STRING.length();
			posRating = html.indexOf("<b>", posRating);
			posRating += "<b>".length();
			while (html.charAt(posRating) != '<') {
				rating += html.charAt(posRating);
				++posRating;
			}
		}*/

		try {
			final float newRating = Float.parseFloat(rating);
			return newRating;
		} catch (NumberFormatException nEx) {
			
		}

		return 0;
	}

	private void resolveAuthorAndArtist(Document mangaUpdatesDoc) {
		ArrayList<String> artist = new ArrayList<String>();

		// get all artist and author while eliminating double entity
		Elements authorRaw = mangaUpdatesDoc.select("a[title=Author Info]").select("u");

		for (Element author : authorRaw) {
			final String authorName = author.text();
			if (!artist.contains(authorName)) {
				artist.add(authorName);
			}
		}

		if (artist.size() > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append(artist.get(0));
			for (int i = 1, n = artist.size() - 1; i < n; ++i) {
				sb.append(", ");
				sb.append(artist.get(i));
			}
			
			if (artist.size() > 1) {
				sb.append(" and ");
				sb.append(artist.get(artist.size() - 1));
			}
			
			mAuthor = sb.toString();
			if (mAuthor.length() > 100) {
				mAuthor = "";
			}
		} else {
			mAuthor = "";
		}
	}

	private void resolveStatus(Document mangaUpdatesDoc) {
		Elements elements = mangaUpdatesDoc.select("div[class=sCat]");

		for (Element element : elements) {
			Element boldChild = element.select("b").first();
			if (boldChild != null && boldChild.text().equals("Status in Country of Origin")) {
				 mStatusAtOrigin = element.nextElementSibling().text().trim();
				 if (mStatusAtOrigin.length() > 100) {
					 mStatusAtOrigin = "N/A";
				 }
			}
			if (boldChild != null && boldChild.text().equals("Completely Scanlated?")) {
				 mStatusScanlation = element.nextElementSibling().text().trim();
				 if (mStatusScanlation.length() > 100) {
					 mStatusAtOrigin = "N/A";
				 }
			}
		}
	}

	// getter
	//public ArrayList<Chapter> getChapters()	{	return mChapters;		}
	public String getTitle()					{	return mTitle;			}
	public String getUrl() 						{	return url;				}
	public Text getDescription() 				{	return mDescription;	}
	public float getRating() 					{	return mRating;			}
	public ArrayList<String> getGenre() 		{	return mGenre;			}
	public ArrayList<String> getTags()			{	return mTags;			}
	public ArrayList<Manga> getRecommended()	{	return mRecommended;	}
	public String getLanguage()					{	return mLanguage;		}
	public String getMangaSource()				{	return mMangaSource;	}

	public String getBookmarkPreferenceString() {
		return mTitle + "_BOOKMARK";
	}

	public void setRating(float rating) 	{	mRating = rating;		}
	public void setLanguage(String lang)	{	mLanguage = lang;		}

	public void setDescription(String desc) {
		mDescription = new Text(StringEscapeUtils.unescapeHtml4(desc));
	}
	
	public void setUrl(String inURL) {
		url = inURL;
	}

	public void addChapter(String title, String url) {
		mChapters.add(new Chapter(url, this, title));
	}

	public void addGenre(String genre) {
		mGenre.add(genre);
	}
	
	public JSONObject toJSON() {
		JSONObject jsonResult = new JSONObject();

		try {
			jsonResult.put(JSON_TITLE, mTitle);
			jsonResult.put(JSON_URL, url);
			jsonResult.put(JSON_DESCRIPTION, mDescription != null ? mDescription.getValue() : "");
			jsonResult.put(JSON_ARTIST, mAuthor != null ? mAuthor : "");
			jsonResult.put(JSON_COVER_IMAGE_PATH, mCoverImagePath);
			jsonResult.put(JSON_RATING, mRating);
			jsonResult.put(JSON_STATUS_ORIGIN, mStatusAtOrigin != null ? mStatusAtOrigin : "");
			jsonResult.put(JSON_STATUS_SCANLATION, mStatusScanlation != null ? mStatusScanlation : "");
			jsonResult.put(JSON_LANGUAGE, mLanguage != null ? mLanguage : "");
			JSONArray array = new JSONArray();
			if (mChapters != null) {
			for (int i = 0, n = mChapters.size(); i < n; ++i) {
				array.put(mChapters.get(i).toJSON());
			}
			}
			jsonResult.put(JSON_CHAPTERS, array);

			// put genre
			if (mGenre != null && mGenre.size() > 0) {
				array = new JSONArray();
				for (int i = 0, n = mGenre.size(); i < n; ++i) {
					array.put(mGenre.get(i));
				}
				jsonResult.put(JSON_GENRE, array);
			}

			// add recommendation
			if (mRecommended != null && mRecommended.size() > 0) {
				array = new JSONArray();
				for (int i = 0, n = mRecommended.size(); i < n; ++i) {
					array.put(mRecommended.get(i).toJSON());
				}
				jsonResult.put(JSON_RECOMMENDED, array);
			}
		} catch (JSONException jEx) {
			
		}
		return jsonResult;
	}
	
	@Override
	public int compareTo(Manga o) {
		return mTitle.compareToIgnoreCase(o.mTitle);
	}

	@Persistent
	private String url, mMangaUpdatesURL;
	Text mDescription;
	String mCoverImagePath;
	@Persistent private String mTitle;
	@Persistent private String mAuthor, mStatusAtOrigin, mStatusScanlation, mLanguage;
	@Persistent private ArrayList<String> mTags;
	private transient ArrayList<Manga> mRecommended;
	@Persistent private float mRating;
	@Persistent ArrayList<String> mGenre;
	@Persistent private String mMangaSource;
	transient ArrayList<Chapter> mChapters;

	public static final String JSON_TITLE = "title";
	public static final String JSON_URL = "url";
	public static final String JSON_CHAPTERS = "chapters";
	public static final String JSON_DESCRIPTION = "description";
	public static final String JSON_COVER_IMAGE_PATH = "coverImagePath";
	public static final String JSON_RATING = "rating";
	public static final String JSON_GENRE = "genre";
	public static final String JSON_ARTIST = "artist";
	public static final String JSON_STATUS_ORIGIN = "status_origin";
	public static final String JSON_STATUS_SCANLATION = "status_scanlation";
	public static final String JSON_RECOMMENDED = "recommended";
	public static final String JSON_LANGUAGE = "lang";
}
