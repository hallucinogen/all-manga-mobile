package net.gogo.service.manga.model;

import java.io.IOException;
import java.util.ArrayList;

import net.gogo.service.manga.helper.Utilities;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.google.appengine.api.datastore.Text;


public class SourceAnimeA extends MangaSource {
	private final static String TAG = "Source AnimeA";

	@Override
	public String mangaSourceIdentifier() {
		return "animea";
	}

	@Override
	public String downloadImagePath(String pageURL) {
		boolean gagal = true;
		while (gagal){
			try {

				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("img[class=mangaimg]") == null || soup.select("img[class=mangaimg]").first() == null) continue;
				String imageSRC = soup.select("img[class=mangaimg]").first().attr("src");
				return imageSRC;
			} catch (IOException ex){
			}
		}
		return null;
	}

	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(chapter.getFullURL()).get();
				Element selection = soup.select("select[name=page]").first();
				
				final String hurdle = ".html";
				
				String pageURLPredcessor = chapter.getFullURL();
				if (pageURLPredcessor.endsWith(hurdle)) {
					// remove .html at the end of page
					pageURLPredcessor = pageURLPredcessor.substring(0, pageURLPredcessor.length() - hurdle.length());
				}
				pageURLPredcessor += "-page-";

				if (selection == null) continue;
				Elements options = selection.getElementsByTag("option");
				gagal = false;
				int i = 0;
				for (Element option : options){
					// by javascript
					String push = pageURLPredcessor + option.attr("value") + ".html";
					chapter.mImageURLs.add(push);
					++i;
				}
			} catch (IOException ex){
			}
		}
	}

	@Override
	public ArrayList<Manga> downloadMangaList() {
		ArrayList<Manga> mangaList = new ArrayList<Manga>();
		boolean sukses = true;
		int page = 0;
		// download selama masih bisa download page
		while (sukses) {
			sukses = false;
			String url = Utilities.ANIMEA_URL + "/browse.html?page=" + page;
			
			boolean gagal = true;
			int attempt = 0;
			
			System.out.println("Source Anime A: URL = " + url);
			while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
				try {
					Document soup = Jsoup.connect(url).get();
					gagal = false;

					// lewat sini means berhasil konek
					Elements links = soup.select("ul[class=mangalist]").select("div[class=manga_desc]");
					
					for (Element link : links){
						Element manga = link.getElementsByTag("a").first();
						sukses = true;
						mangaList.add(new Manga(manga.text(), Utilities.ANIMEA_URL + manga.attr("href")));
					}
				} catch (IOException ex){
					++attempt;
				}
			}
			++page;
		}
		
		return mangaList;
	}

	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains(Utilities.ANIMEA_URL);
	}

	@Override
	public void downloadChapterListAndGenre(Manga manga, boolean downloadChapters) {
		String url = manga.getUrl();
		manga.mChapters.clear();
		
		System.out.println("[Source Anime A] try to download " + manga.getTitle() + " from " + manga.getUrl() );
		boolean gagal = true;
		int attempt = 0;
		Document soup = null;
		while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
			try {
				soup = Jsoup.connect(url).get();
				gagal = false;
			} catch (IOException iEx) {
				++attempt;
			}
		}

		if (gagal) return;

		Elements links = null;
		if (soup == null) return;

		// get cover and description first
		// get cover image
		links = soup.select("img[class=manga_img_big]");
		if (links != null && links.first() != null) {
			manga.mCoverImagePath = links.first().attr("src");
		}
		// get manga description
		links = soup.select("div[class=left_container smaller]");
		if (links != null) {
			links = links.first().select("p");
			if (links != null) {
				manga.mDescription = new Text(StringEscapeUtils.unescapeHtml4(links.get(1).text()));	
			}
		}
		// get genre
		links = soup.select("ul[class=manga_info]").first().select("li");
		for (Element link : links) {
			if (link.child(0).text().trim().equals("Genre(s):")) {
				Elements genreLinks = link.select("a");
				for (Element genreLink : genreLinks) {
					manga.mGenre.add(genreLink.html());
				}
			}
		}

		// now get manga listing

		// let's check first whether we should download it or not?
		if (downloadChapters) {
			boolean sukses = true;
			int page = 1;
			while (sukses) {
				sukses = false;
				links = soup.select("ul[class=chapters_list]").select("a");
				for (Element link : links) {
					if (link.text() == null || link.text().trim().equals("")) continue;
					//Log.i(TAG, "Chapter " + link.select("a").text());
					manga.mChapters.add(new Chapter(Utilities.ANIMEA_URL + link.attr("href"), manga, link.text()));
					// TODO: not sure whether it has lot of page or not
					//sukses = true;
				}
				// get html for next page
				if (!sukses) break;
				gagal = true;
				attempt = 0;
				++page;
				url = manga.getUrl() + "?page=" + page;
				while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
					try {
						soup = Jsoup.connect(url).get();
						gagal = false;
					} catch (IOException iEx) {
						++attempt;
					}
				}
				if (!gagal) {

				} else {
					break;
				}
			}
		}
	}
}
