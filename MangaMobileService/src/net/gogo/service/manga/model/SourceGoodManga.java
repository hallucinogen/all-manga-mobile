package net.gogo.service.manga.model;

import java.io.IOException;
import java.util.ArrayList;

import net.gogo.service.manga.helper.Utilities;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.google.appengine.api.datastore.Text;


public class SourceGoodManga extends MangaSource {
	private final static String TAG = "Source Good Manga";

	@Override
	public String mangaSourceIdentifier() {
		return "goodmanga";
	}

	@Override
	public String downloadImagePath(String pageURL) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("div[id=manga_viewer]") == null 
						|| soup.select("div[id=manga_viewer]").select("img").first() == null) continue;
				String imageSRC = soup.select("div[id=manga_viewer]").select("img").first().attr("src");
				return imageSRC;
			} catch (IOException ex){
			}
		}
		return null;
	}

	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(chapter.getFullURL()).get();
				Element selection = soup.select("div[id=assets]").select("select[name=page_select]").first();

				if (selection == null) continue;

				Elements options = selection.getElementsByTag("option");
				gagal = false;
				int i = 0;
				for (Element option : options){
					String push = option.attr("value");
					chapter.mImageURLs.add(push);
					++i;
				}
			} catch (IOException ex){
			}
		}
	}

	@Override
	public ArrayList<Manga> downloadMangaList() {
		ArrayList<Manga> mangaList = new ArrayList<Manga>();
		boolean sukses = true;
		int page = 1;
		// download selama masih bisa download page
		/*while (sukses) {
			sukses = false;
			String url = Utilities.GOODMANGA_URL + "/manga-list?page=" + page;
			
			boolean gagal = true;
			int attempt = 0;
			
			System.out.println("Source Good Manga: URL = " + url);
			while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
				try {
					Document soup = Jsoup.connect(url).get();
					gagal = false;

					// lewat sini means berhasil konek
					Elements links = soup.select("table[id=listing]").select("td");
					
					for (Element link : links){
						Elements mangaLinks = link.getElementsByTag("a");
						for (Element manga : mangaLinks){
							sukses = true;
							mangaList.add(new Manga(manga.text(), Utilities.GOODMANGA_URL + manga.attr("href")));
						}
					}
				} catch (IOException ex){
					++attempt;
				}
			}
			++page;
		}*/

		String url = Utilities.GOODMANGA_URL + "/manga-list";

		boolean gagal = true;
		int attempt = 0;
		
		System.out.println("Source Good Manga: URL = " + url);
		while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
			try {
				Document soup = Jsoup.connect(url).get();
				gagal = false;

				// lewat sini means berhasil konek
				Elements links = soup.select("table[class=series_index]").select("td");

				for (Element link : links){
					Elements mangaLinks = link.getElementsByTag("a");
					for (Element manga : mangaLinks){
						sukses = true;
						mangaList.add(new Manga(manga.text(), manga.attr("href")));
					}
				}
			} catch (IOException ex){
				++attempt;
			}
		}
		
		return mangaList;
	}

	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains(Utilities.GOODMANGA_URL) || url.contains(Utilities.GOODMANGA_URL_2);
	}

	@Override
	public void downloadChapterListAndGenre(Manga manga, boolean downloadChapters) {
		String url = manga.getUrl();
		manga.mChapters.clear();
		boolean gagal = true;
		int attempt = 0;
		Document soup = null;
		while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
			try {

				soup = Jsoup.connect(url).get();
				gagal = false;
			} catch (IOException iEx) {
				++attempt;
			}
		}

		if (gagal) return;
		
		Elements links = null;
		if (soup == null) return;

		// get cover and description first
		// get cover image
		links = soup.select("div[id=series_info]");
		if (links != null) {
			links = links.select("img");
			if (links != null) {
				Element link = links.first();
				if (link != null) {
					manga.mCoverImagePath = link.attr("src");
				}
			}
		}
		// get manga description
		links = soup.select("div[id=series_details]");
		if (links != null) {
			links = links.select("span");
			if (links != null) {
				for (Element link: links) {
					if (link.text().equals("Description:")) {
						manga.mDescription = new Text(StringEscapeUtils.unescapeHtml4(link.nextElementSibling().text()));
					} else if (link.text().equals("Genres:")) {
						Element genreParent = link.parent();
						Elements genres = genreParent.select("span[class=red_box]").select("a");
						for (Element genre: genres) {
							manga.mGenre.add(genre.text());
						}
					}
				}
			}
		}
		/*
		// get genre
		links = soup.select("th");
		for (Element link : links) {
			if (link.text().equals("Genres:")) {
				Elements genreLinks = link.nextElementSibling().select("a");
				for (Element genreLink : genreLinks) {
					manga.mGenre.add(genreLink.html());
				}
			}
		}*/

		// now get manga listing
		
		// let's check first whether we should download it or not?
		if (downloadChapters) {
			boolean sukses = true;
			int page = 1;
			while (sukses) {
				sukses = false;
				links = soup.select("div[id=chapters]").select("li");
				for (Element link : links) {
					if (link.select("a").text() == null || link.select("a").text().trim().equals("")) continue;
					//Log.i(TAG, "Chapter " + link.select("a").text());
					manga.mChapters.add(new Chapter(link.select("a").attr("href"), manga, link.select("a").text()));
				}
				// get html for next page
				
				links = soup.select("ul[class=pagination]").select("a");
				for (Element link : links) {
					if (link.text().equalsIgnoreCase("next")) {
						sukses = true;
					}
				}
				
				if (!sukses) {
					break;
				}
 				
				gagal = true;
				attempt = 0;
				++page;
				url = manga.getUrl() + "?page=" + page;
				while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
					try {
						soup = Jsoup.connect(url).get();
						gagal = false;
					} catch (IOException iEx) {
						++attempt;
					}
				}
				if (!gagal) {
					
				} else {
					break;
				}
			}
		}
	}
}
