package net.gogo.service.manga.model;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.appengine.api.datastore.Text;

import net.gogo.service.manga.helper.Utilities;

public class SourceBatotoMalay extends SourceBatoto {
	@Override
	public String mangaSourceIdentifier() {
		return "batoto-malay";
	}

	@Override
	public String getLanguage() {
		return "Malay";
	}
}
