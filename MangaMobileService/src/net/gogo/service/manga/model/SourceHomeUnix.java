package net.gogo.service.manga.model;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class SourceHomeUnix extends MangaSource {
	@Override
	public String mangaSourceIdentifier() {
		return "homeunix";
	}

	String blank = " ";

	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		// TODO Auto-generated method stub

	}

	@Override
	public void downloadChapterListAndGenre(Manga manga,
			boolean downloadChapters) {
		// TODO Auto-generated method stub

	}

	@Override
	public String downloadImagePath(String pageURL) {
		// TODO Auto-generated method stub
		boolean gagal = true;
		while (gagal) {
			try {
				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("!--[ALT= ]") == null)
					return null;
				Element div = soup.select("!--[ALT= ]").first();
				// if (!-- == null) return null;
				// String imageSRC = !--.select("IMG").first().attr("SRC");
				// return imageSRC;
			} catch (IOException ex) {
			}
		}
		return null;
	}

	@Override
	public ArrayList<Manga> downloadMangaList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUsingTheMangaSource(String url) {
		// TODO Auto-generated method stub
		return url.contains("http://read.homeunix.com");
	}

}
