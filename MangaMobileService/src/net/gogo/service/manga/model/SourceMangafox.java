package net.gogo.service.manga.model;

import java.io.IOException;
import java.util.ArrayList;

import net.gogo.service.manga.helper.Utilities;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.google.appengine.api.datastore.Text;


public class SourceMangaFox extends MangaSource {
	private final static String TAG = "Source Mangafox";
	@Override
	public String mangaSourceIdentifier() {
		return "mangafox";
	}

	@Override
	public String downloadImagePath(String pageURL) {
		boolean gagal = true;
		while (gagal){
			try {

				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("div[id=viewer]") == null || soup.select("div[id=viewer]").select("img") == null ||
						soup.select("div[id=viewer]").select("img").first() == null) continue;
				String imageSRC = soup.select("div[id=viewer]").select("img").first().attr("src");
				return imageSRC;
			} catch (IOException ex){
			}
		}
		return null;
	}

	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(chapter.getFullURL()).get();
				Element selection = soup.select("select[class=m]").first();

				final String hurdle = ".html";

				String pageURLPredcessor = chapter.getFullURL();
				if (pageURLPredcessor.endsWith(hurdle)) {
					// remove /*.html at the end of page
					int lastSlashPos = pageURLPredcessor.lastIndexOf("/");
					pageURLPredcessor = pageURLPredcessor.substring(0, lastSlashPos + 1);
				}
				System.out.println(pageURLPredcessor);

				if (selection == null) continue;
				Elements options = selection.getElementsByTag("option");
				gagal = false;
				int i = 0;
				for (Element option : options){
					// by javascript
					String push = pageURLPredcessor + option.attr("value") + ".html";
					chapter.mImageURLs.add(push);
					++i;
				}
			} catch (IOException ex){
			}
		}
	}

	@Override
	public ArrayList<Manga> downloadMangaList() {
		ArrayList<Manga> mangaList = new ArrayList<Manga>();
		boolean sukses = true;
		int page = 1;
		// download selama masih bisa download page
		while (sukses) {
			sukses = false;
			String url = Utilities.MANGAFOX_URL + "/directory/" + page + ".htm?az";
			
			boolean gagal = true;
			int attempt = 0;

			System.out.println("downloading from " + url);
			while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
				try {
					Document soup = Jsoup.connect(url).get();
					gagal = false;

					System.gc();
					// lewat sini means berhasil konek
					Elements links = soup.select("ul[class=list]").select("div[class=manga_text]");

					for (Element link : links){
						Element manga = link.getElementsByTag("a").first();
						sukses = true;
						mangaList.add(new Manga(manga.text(), manga.attr("href")));
					}
				} catch (IOException ex){
					++attempt;
				}
			}
			++page;

			if (mangaList.size() > 5000) {
				break;
			}
		}
		
		return mangaList;
	}

	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains(Utilities.MANGAFOX_URL);
	}

	@Override
	public void downloadChapterListAndGenre(Manga manga, boolean downloadChapters) {
		String url = manga.getUrl();
		manga.mChapters.clear();

		System.out.println("[Source Mangafox] try to download " + manga.getTitle() + " from " + manga.getUrl() );
		boolean gagal = true;
		int attempt = 0;
		Document soup = null;
		while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
			try {
				soup = Jsoup.connect(url).get();
				gagal = false;
			} catch (IOException iEx) {
				++attempt;
			}
		}

		if (gagal) return;

		Elements links = null;
		if (soup == null) return;

		// get cover and description first
		// get cover image
		links = soup.select("div[class=cover]").select("img");
		if (links != null && links.first() != null) {
			manga.mCoverImagePath = links.first().attr("src");
		}
		// get manga description
		try {
			links = soup.select("p[class=summary]");
			if (links != null && links.first() != null) {
				manga.mDescription = new Text(StringEscapeUtils.unescapeHtml4(links.first().text()));	
			}
			// get genre
			links = soup.select("div[id=title]").first().select("table").first().select("tr").select("td").get(3).select("a");
			for (Element genreLink : links) {
				manga.mGenre.add(genreLink.html());
			}
		} catch (NullPointerException nEx) {
			
		}

		// now get manga listing

		// let's check first whether we should download it or not?
		if (downloadChapters) {
			boolean sukses = true;
			int page = 1;
			while (sukses) {
				sukses = false;
				links = soup.select("ul[class=chlist]").select("a[class=tips]");
				for (Element link : links) {
					if (link.text() == null || link.text().trim().equals("")) continue;
					//Log.i(TAG, "Chapter " + link.select("a").text());
					manga.mChapters.add(new Chapter(link.attr("href"), manga, link.text()));
					// TODO: not sure whether it has lot of page or not
					//sukses = true;
				}
				// get html for next page
				if (!sukses) break;

				// HACK: supposedly will not pass this
				gagal = true;
				attempt = 0;
				++page;
				url = manga.getUrl() + "?page=" + page;
				while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
					try {
						soup = Jsoup.connect(url).get();
						gagal = false;
					} catch (IOException iEx) {
						++attempt;
					}
				}
				if (!gagal) {

				} else {
					break;
				}
			}
		}
	}
}
