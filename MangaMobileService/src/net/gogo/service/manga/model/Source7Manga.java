package net.gogo.service.manga.model;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Source7Manga extends MangaSource {
	@Override
	public String mangaSourceIdentifier() {
		return "7manga";
	}

	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		// TODO Auto-generated method stub

	}

	@Override
	public void downloadChapterListAndGenre(Manga manga,
			boolean downloadChapters) {
		// TODO Auto-generated method stub

	}

	@Override
	public String downloadImagePath(String pageURL) {
		// TODO Auto-generated method stub
		boolean gagal = true;
		while (gagal) {
			try {
				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("img[name=TheImg]") == null
						|| soup.select("img[name=TheImg]").first() == null)
					continue;
				String imageSRC = soup.select("img[name=TheImg]").first()
						.attr("src");
				return imageSRC;
			} catch (IOException ex) {
			}
		}
		return null;
	}

	@Override
	public ArrayList<Manga> downloadMangaList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isUsingTheMangaSource(String url) {
		// TODO Auto-generated method stub
		return url.contains("http://www.7manga.com");
	}
}
