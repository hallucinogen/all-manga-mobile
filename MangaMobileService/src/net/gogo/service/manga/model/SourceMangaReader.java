package net.gogo.service.manga.model;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

import net.gogo.service.manga.helper.Utilities;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.google.appengine.api.datastore.Text;

public class SourceMangaReader extends MangaSource {
	private final static String TAG = "Source Manga Reader";

	@Override
	public String mangaSourceIdentifier() {
		return "mangareader";
	}

	@Override
	public String downloadImagePath(String pageURL) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("img[name=img]") == null || soup.select("img[name=img]").first() == null) continue;
				String imageSRC = soup.select("img[name=img]").first().attr("src");
				return imageSRC;
			} catch (IOException ex){
			}
		}
		return null;
	}

	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		boolean gagal = true;
		while (gagal){
			try {
				System.out.println(chapter.getFullURL());
				Document soup = Jsoup.connect(chapter.getFullURL()).get();
				Element selection = soup.select("select[id=pageMenu]").first();

				if (selection == null) continue;
				Elements options = selection.getElementsByTag("option");
				gagal = false;
				int i = 0;
				for (Element option : options){
					String push = Utilities.MANGAREADER_URL + option.attr("value");
					chapter.mImageURLs.add(push);
					++i;
				}
			} catch (IOException ex){
			}
		}
	}

	@Override
	public ArrayList<Manga> downloadMangaList() {
		ArrayList<Manga> mangaList = new ArrayList<Manga>();
		String url = Utilities.MANGAREADER_URL + "/alphabetical";
		
		boolean gagal = true;
		int attempt = 0;
		
		while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
			try {
				Document soup = Jsoup.connect(url).get();
				gagal = false;
				
				// lewat sini means berhasil konek
				Elements links = soup.select("ul[class=series_alpha]");
				
				for (Element link : links){
					Elements mangaLinks = link.getElementsByTag("a");
					for (Element manga : mangaLinks){
						mangaList.add(new Manga(manga.text(), Utilities.MANGAREADER_URL + manga.attr("href")));
					}
				}
				
			} catch (IOException ex){
				++attempt;
			}
		}
		
		return mangaList;
	}

	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains(Utilities.MANGAREADER_URL) || url.contains(Utilities.MANGAREADER_URL_2);
	}

	@Override
	public void downloadChapterListAndGenre(Manga manga, boolean downloadChapters) {
		String url = manga.getUrl();

		while (url.contains(Utilities.MANGAREADER_URL)) {
			url = url.replace(Utilities.MANGAREADER_URL, "");
		}
		while (url.startsWith("/")) {
			url = url.replaceFirst("/", "");
		}
		url = Utilities.MANGAREADER_URL + "/" + url;
		manga.mChapters.clear();
		boolean gagal = true;
		int attempt = 0;
		Document soup = null;
		while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
			try {
				soup = Jsoup.connect(url).get();
				gagal = false;
			} catch (IOException iEx) {
				++attempt;
			}
		}

		if (gagal) return;
		
		
		Elements links = soup.getElementsByTag("div");
		for (Element link : links){
			if (link.attr("class").equals("chico_manga") && !link.parent().parent().parent().attr("id").equals("latestchapters")){
				// check if we should persist the chapters
				if (downloadChapters)
					manga.mChapters.add(0, new Chapter(Utilities.MANGAREADER_URL + link.parent().select("a").attr("href"), manga, link.parent().select("a").text()));
			} else if (link.attr("id").equals("readmangasum")) {
				manga.mDescription = new Text(StringEscapeUtils.unescapeHtml4(link.select("p").text()));
			} else if (link.attr("id").equals("mangaimg")) {
				manga.mCoverImagePath = link.select("img").attr("src");
			}
		}
		// get genre
		links = soup.getElementsByTag("span");
		for (Element link : links){
			if (link.attr("class").equals("genretags")){
				manga.mGenre.add(link.html());
			}
		}
	}
}
