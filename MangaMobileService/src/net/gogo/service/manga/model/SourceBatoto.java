package net.gogo.service.manga.model;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.appengine.api.datastore.Text;

import net.gogo.service.manga.helper.Utilities;

public class SourceBatoto extends MangaSource {
	private final static String TAG = "Batoto Manga Source";

	@Override
	public String mangaSourceIdentifier() {
		return "batoto";
	}

	@Override
	public String downloadImagePath(String pageURL) {
		boolean gagal = true;
		while (gagal){
			try {

				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("div[id=full_image]") == null || soup.select("div[id=full_image]").select("img") == null || soup.select("div[id=full_image]").select("img").first() == null) continue;
				String imageSRC = soup.select("div[id=full_image]").select("img").first().attr("src");
				return imageSRC;
			} catch (IOException ex){
			}
		}
		return null;
	}
	
	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(chapter.getFullURL()).get();
				Element selection = soup.select("select[id=page_select]").first();

				if (selection == null) continue;
				Elements options = selection.getElementsByTag("option");
				gagal = false;
				int i = 0;
				for (Element option : options){
					String push = option.attr("value");
					chapter.mImageURLs.add(push);
					++i;
				}
			} catch (IOException ex){
			}
		}
	}
	
	@Override
	public ArrayList<Manga> downloadMangaList() {
		ArrayList<Manga> mangaList = new ArrayList<Manga>();
		boolean sukses = true;
		int page = 1;

		// download selama masih bisa download page
		while (sukses) {
			sukses = false;
			String url = Utilities.BATOTO_URL + "/search?&p=" + page;

			boolean gagal = true;
			int attempt = 0;

			while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
				try {
					System.out.println("Download = " + url);
					Document soup = Jsoup.connect(url).get();
					gagal = false;

					// lewat sini means berhasil konek
					Elements links = soup.select("table[class=ipb_table chapters_list]").select("td[style=text-align: left;]").select("strong");

					for (Element link : links) {
						Elements mangaLinks = link.getElementsByTag("a");
						for (Element manga : mangaLinks) {
							sukses = true;

							// check if there's such language here
							String mangaURL = manga.attr("href");
							Manga mangaTest = new Manga(manga.text(), mangaURL);

							downloadChapterListAndGenre(mangaTest, true);

							if (mangaTest.mChapters.size() > 0) {
								final String lang = getLanguage();
								mangaTest.setLanguage(lang);
								// if not english, embed language identifier
								if (!lang.equalsIgnoreCase("english")) {
									mangaTest.setUrl(mangaTest.getUrl() + "#" + lang);
									mangaTest.setLanguage(lang);

									System.out.println(mangaTest.getUrl());
								}
								mangaList.add(mangaTest);
							}
						}
					}
				} catch (IOException ex){
					++attempt;
				}
			}
			++page;
		}
		
		return mangaList;
	}
	
	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains(Utilities.BATOTO_URL) || url.contains(Utilities.BATOTO_URL_2);
	}
	
	@Override
	public void downloadChapterListAndGenre(Manga manga,
			boolean downloadChapters) {
		String url = manga.getUrl();
		manga.mChapters.clear();
		boolean gagal = true;
		int attempt = 0;
		Document soup = null;
		while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
			try {

				soup = Jsoup.connect(url).get();
				gagal = false;
			} catch (IOException iEx) {
				++attempt;
			}
		}

		if (gagal) return;
		
		Elements links = null;
		if (soup == null) return;

		// get cover and description first
		// get cover image
		links = soup.select("div[class=ipsBox]");
		if (links != null) {
			links = links.select("img");
			if (links != null) {
				Element link = links.first();
				if (link != null) {
					manga.mCoverImagePath = link.attr("src");
				}
			}
		}
		
		// set manga language
		manga.setLanguage(getLanguage());

		// get manga description
		// get genre
		links = soup.select("table[class=ipb_table]").select("td");
		for (Element link : links) {
			if (link.text().equals("Genres:")) {
				Elements genreSpans = link.nextElementSibling().select("span");
				for (Element genreSpan : genreSpans) {
					manga.mGenre.add(genreSpan.text());
				}
			} else if (link.text().equals("Description:")) {
				manga.mDescription = new Text(StringEscapeUtils.unescapeHtml4(link.nextElementSibling().text()));
			}
		}

		// now get manga listing
		
		// let's check first whether we should download it or not?
		if (downloadChapters) {
			boolean sukses = true;
			int page = 1;
			while (sukses) {
				sukses = false;
				links = soup.select("table[class=ipb_table chapters_list]").select("tr[class=row lang_" + getLanguage() + "]");
				for (Element link : links) {
					if (link.child(0) != null && link.child(0).select("a").text() == null || link.child(0).select("a").text().trim().equals("")) continue;
					//Log.i(TAG, "Chapter " + link.select("a").text());
					manga.mChapters.add(new Chapter(link.child(0).select("a").attr("href"), manga, link.child(0).select("a").text()));
					//sukses = true;
				}
				// get html for next page
				if (!sukses) break;
				gagal = true;
				attempt = 0;
				++page;
				// dunno why the paging is by st=page * 20???
				url = manga.getUrl() + "?st=" + (page * 20);
				while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
					try {
						soup = Jsoup.connect(url).get();
						gagal = false;
					} catch (IOException iEx) {
						++attempt;
					}
				}
				if (!gagal) {
					
				} else {
					break;
				}
			}
		}
	}
}
