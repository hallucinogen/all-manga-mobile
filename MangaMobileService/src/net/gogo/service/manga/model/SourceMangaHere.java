package net.gogo.service.manga.model;

import java.io.IOException;
import java.util.ArrayList;

import net.gogo.service.manga.helper.Utilities;

import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.google.appengine.api.datastore.Text;

public class SourceMangaHere extends MangaSource {
	@Override
	public String mangaSourceIdentifier() {
		return "mangahere";
	}

	@Override
	public void downloadChapterImageURLs(Chapter chapter) {
		try {
			Document soup = Jsoup.connect(chapter.getFullURL()).get();
			Element selection = soup.select("select[class=wid60]").first();

			Elements options = selection.getElementsByTag("option");
			int i = 0;
			for (Element option : options){
				String push = option.attr("value");
				chapter.mImageURLs.add(push);
				++i;
			}
		} catch (IOException ex){
		}
	}
	
	@Override
	public void downloadChapterListAndGenre(Manga manga,
			boolean downloadChapters) {
		String url = manga.getUrl();
		manga.mChapters.clear();
		boolean gagal = true;
		int attempt = 0;
		Document soup = null;
		while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
			try {

				soup = Jsoup.connect(url).get();
				gagal = false;
			} catch (IOException iEx) {
				++attempt;
			}
		}

		if (gagal) return;
		
		Elements links = null;
		if (soup == null) return;

		// get cover and description first
		// get cover image
		links = soup.select("img[class=img]");
		if (links != null) {
			Element link = links.first();
			if (link != null) {
				manga.mCoverImagePath = link.attr("src");
			}
		}
		// get manga description
		links = soup.select("p[id=show]");
		if (links != null) {
			manga.mDescription = new Text(StringEscapeUtils.unescapeHtml4(links.first().text()));
		}
		// get genre
		links = soup.select("label");
		for (Element link : links) {
			if (link.text().equals("Genre(s):")) {
				String[] genres = link.parent().text().split(",");
				for (String genre : genres) {
					manga.mGenre.add(genre.trim());
				}
			}
		}

		// now get manga listing
		links = soup.select("a[class=color_0077]");
		for (Element link : links){
			if (link.parent() != null && link.parent().tagName().equals("span") && link.parent().className().equals("left")) {
				manga.mChapters.add(new Chapter(link.attr("href"), manga, link.text().trim()));
			}
		}
	}
	
	@Override
	public String downloadImagePath(String pageURL) {
		boolean gagal = true;
		while (gagal){
			try {
				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("img[id=image]") == null || soup.select("img[id=image]").first() == null) continue;
				String imageSRC = soup.select("img[id=image]").first().attr("src");
				return imageSRC;
			} catch (IOException ex){
			}
		}
		
		return null;
	}
	
	public java.util.ArrayList<Manga> downloadMangaList() {
		ArrayList<Manga> mangaList = new ArrayList<Manga>();
		String url = Utilities.MANGAHERE_URL + "/mangalist/";
		
		boolean gagal = true;
		int attempt = 0;
		
		while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
			try {
				Document soup = Jsoup.connect(url).get();
				gagal = false;
				
				// lewat sini means berhasil konek
				Elements mangaLinks = soup.select("a[class=manga_info]");
				
				for (Element manga : mangaLinks){
					mangaList.add(new Manga(manga.text(), manga.attr("href")));
				}
				
			} catch (IOException ex){
				++attempt;
			}
		}
		
		return mangaList;
	}
	
	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains(Utilities.MANGAHERE_URL) || url.contains(Utilities.MANGAHERE_URL_2);
	}
}
