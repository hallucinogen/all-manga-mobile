package net.gogo.service.manga.model;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.gogo.service.manga.server.MangaDataStore;
import net.gogo.service.manga.server.MangaMobileTitleFetcher;

public abstract class MangaSource {
	private final static String TAG = MangaMobileTitleFetcher.class.getName();
	public MangaSource() {
		
	}
	
	// functions to be implemented by each manga source
	public abstract ArrayList<Manga> downloadMangaList();
	public abstract void downloadChapterListAndGenre(Manga manga, boolean downloadChapters);
	public abstract void downloadChapterImageURLs(Chapter chapter);
	public abstract String downloadImagePath(String pageURL);
	public abstract boolean isUsingTheMangaSource(String url);
	public abstract String mangaSourceIdentifier();
	
	public String getLanguage() {
		return "English";
	}

	public String downloadImagePath(Chapter chapter, int index) {
		return downloadImagePath(chapter.mImageURLs.get(index));
	}
	
	public void downloadCompleteMangaList() {
		Logger.getLogger(TAG).log(Level.INFO, "Downloading complete manga list");
		System.out.println("Downloading complete manga list");
		ArrayList<Manga> mangas = downloadMangaList();

		Logger.getLogger(TAG).log(Level.INFO, "Downloading manga list complete! Now downloading metadata :)");
		System.out.println("Downloading manga list complete! Now downloading metadata :)");
		// for each manga, download metadata
		while (mangas.size() > 0) {
			Manga manga = mangas.get(0);
			System.out.println("[Manga Source] manga title = " + manga.getTitle());
			Logger.getLogger(TAG).log(Level.INFO, "[Manga Source] manga title = " + manga.getTitle());
			// no title? continue!
			if (manga.getTitle().trim().isEmpty()) {
				mangas.remove(0);
				continue;
			}
			manga.downloadMangaMetadata(false, getLanguage());
			System.gc();

			// for each chapter, download the image url
			/*ArrayList<Chapter> chapters = manga.getChapters();
			for (int j = 0, m = chapters.size(); j < m; ++j) {
				Chapter chapter = chapters.get(j);

				downloadChapterImageURLs(chapter);
				ArrayList<String> imagePaths = new ArrayList<String>();
				ArrayList<String> baseImagePaths = chapter.mImageURLs;
				
				// for each page, renew the url to be the downloaded image path
				for (int k = 0, p = baseImagePaths.size(); k < p; ++k ) {
					imagePaths.add(downloadImagePath(chapter, k));
				}
				chapter.mImageURLs = imagePaths;
			}*/
			System.out.println(manga.getTitle() + " " + manga.getUrl());
			MangaDataStore.instance().addManga(manga);

			/*try {
				// untuk menipu Google
				Thread.sleep(10000);
			} catch (InterruptedException iEx) {
				
			}*/
			
			mangas.remove(0);
			System.gc();
		}
	}
}
