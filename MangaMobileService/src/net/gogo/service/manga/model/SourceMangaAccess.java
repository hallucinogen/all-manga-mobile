package net.gogo.service.manga.model;

import java.io.IOException;
import java.util.ArrayList;

import net.gogo.service.manga.helper.Utilities;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SourceMangaAccess extends MangaSource {
	@Override
	public String mangaSourceIdentifier() {
		return "mangaaccess";
	}


	public void downloadChapterImageURLs(Chapter chapter) {
	};

	@Override
	public void downloadChapterListAndGenre(Manga manga,
			boolean downloadChapters) {
		// TODO Auto-generated method stub

	}

	@Override
	public String downloadImagePath(String pageURL) {
		// TODO Auto-generated method stub
		boolean gagal = true;
		while (gagal) {
			try {
				Document soup = Jsoup.connect(pageURL).get();
				gagal = false;
				if (soup == null || soup.select("div[id=pic]") == null)
					return null;
				Element div = soup.select("div[id=pic]").first();
				if (div == null)
					return null;
				String imageSRC = div.select("img").first().attr("src");
				return imageSRC;
			} catch (IOException ex) {
			}
		}
		return null;
	}

	@Override
	public ArrayList<Manga> downloadMangaList() {
		ArrayList<Manga> mangaList = new ArrayList<Manga>();
		String url = "http://www.manga-access.com/manga/list";

		boolean gagal = true;
		int attempt = 0;

		while (gagal && attempt < Utilities.DOWNLOAD_ATTEMPT) {
			try {
				Document soup = Jsoup.connect(url).get();
				gagal = false;

				// lewat sini means berhasil konek
				Elements links = soup.select("div[style=float:right;]");

				for (Element link : links) {
					Elements mangaLinks = link.parent().select("a");
					for (Element manga : mangaLinks) {
						mangaList
								.add(new Manga(manga.text(),
										Utilities.MANGAREADER_URL
												+ manga.attr("href")));
					}
				}
			} catch (IOException ex) {
				++attempt;
			}
		}

		return mangaList;
	}

	@Override
	public boolean isUsingTheMangaSource(String url) {
		return url.contains("http://www.manga-access.com");
	}

}
