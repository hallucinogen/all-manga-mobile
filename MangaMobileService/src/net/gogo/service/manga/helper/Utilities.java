package net.gogo.service.manga.helper;

public class Utilities {


	public static final int DOWNLOAD_ATTEMPT	= 5;
	public static final String MANGAREADER_URL	= "http://mangareader.net";
	public static final String MANGAREADER_URL_2= "http://www.mangareader.net";
	public static final String MANGAPANDA_URL	= "http://mangapanda.com";
	public static final String MANGAPANDA_URL_2	= "http://www.mangapanda.com";
	public static final String GOODMANGA_URL	= "http://goodmanga.net";
	public static final String GOODMANGA_URL_2	= "http://www.goodmanga.net";
	public static final String BATOTO_URL		= "http://batoto.net";
	public static final String BATOTO_URL_2		= "http://www.batoto.net";
	public static final String ANIMEA_URL		= "http://manga.animea.net";
	public static final String MANGAHERE_URL	= "http://mangahere.com";
	public static final String MANGAHERE_URL_2	= "http://www.mangahere.com";
	public static final String MANGAFOX_URL		= "http://mangafox.me";
	
	public static final String[] ALL_MANGA_URL = new String[] {
		MANGAREADER_URL,
		MANGAPANDA_URL,
		GOODMANGA_URL,
		BATOTO_URL_2,
		ANIMEA_URL,
		MANGAHERE_URL_2
	};
	
	//public static final String RATING_QUERY		= "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&q=mangaupdates+";
	public static final String[] MANGAUPDATES_QUERY	= new String[] {
		"http://www.mangaupdates.com/series.html?stype=title&type=manga&search=",
		"http://www.mangaupdates.com/series.html?stype=title&type=manhwa&search=",
		"http://www.mangaupdates.com/series.html?stype=title&search="
	};
	public static final int DOUJINSHI_QUERY		= 2;
	public static final String RATING_STRING		= "Bayesian Average:";
	public static final String RECOMEMEND_STRING	= "Recommendations";
	public static final String ADS_ENABLED			= "adsEnabled";
	public static boolean IS_ADS_ENABLED			= true;
}
