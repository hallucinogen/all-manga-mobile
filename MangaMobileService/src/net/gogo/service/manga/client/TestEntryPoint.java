package net.gogo.service.manga.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

public class TestEntryPoint implements EntryPoint {
	@Override
	public void onModuleLoad() {
		Label label = new Label("I'm awesome " + GWT.getModuleBaseURL());

        RootPanel.get().add(label);
	}
}
