package net.gogo.mobile.promanga;

import java.util.ArrayList;
import java.util.Collections;

import net.gogo.mobile.allmanga.MangaDetailAct;
import net.gogo.mobile.allmanga.MangaShelvesAct;
import net.gogo.mobile.allmanga.SettingAct;
import net.gogo.mobile.allmanga.adapter.MangaGridAdapter;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Manga;
import net.gogo.mobile.allmanga.view.ShelvesView;
import net.gogo.mobile.allmangalib.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.view.MenuItem;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.Gallery;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

public class ProMangaShelvesAct extends MangaShelvesAct  {
	private final static String TAG = ProMangaShelvesAct.class.getSimpleName();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// HACK: we should not change read limit here
		Utilities.BASE_READ_LIMIT = Utilities.INFINITE_READ_LIMIT;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen_shelves);

		mMangas = new ArrayList<Manga>();

		// create view
		mShelvesView = (ShelvesView) findViewById(R.id.grid_shelves);
		mProgress = new ProgressDialog(this, R.style.SpecialDialog);

		// persist search query
		final Bundle extras = getIntent().getExtras();
		
		if (extras != null) {
			mSearchQuery = extras.getString(Utilities.INTENT_TITLE);
			mIncludeGenre = new ArrayList<String>();
			mExcludeGenre = new ArrayList<String>();
			mSelectedSources = new ArrayList<String>();
			
			try {
				JSONObject json = new JSONObject(extras.getString(Utilities.INTENT_GENRE));
				
				JSONArray includeArray = json.getJSONArray(Utilities.GENRE_INCLUDE);
				JSONArray excludeArray = json.getJSONArray(Utilities.GENRE_EXCLUDE);
				JSONArray sourcesArray = json.getJSONArray(Utilities.MANGA_SOURCES);
				
				for (int i = 0, n = includeArray.length(); i < n; ++i) {
					mIncludeGenre.add(includeArray.getString(i));
				}
				
				for (int i = 0, n = excludeArray.length(); i < n; ++i) {
					mExcludeGenre.add(excludeArray.getString(i));
				}
				
				for (int i = 0, n = sourcesArray.length(); i < n; ++i) {
					mSelectedSources.add(sourcesArray.getString(i));
				}
			} catch (JSONException jEx) {
				Log.e(TAG, "Error while decoding JSON");
			}
		}
		
		// get density
		final float density = getResources().getDisplayMetrics().density;

		// make manga source popup
		LayoutInflater inflater = LayoutInflater.from(this);
		mMangaSourcePopup = (RelativeLayout) inflater.inflate(R.layout.shelf_selection_popup, null);
		mMangaSourcePopupWindow = new PopupWindow(mMangaSourcePopup, WindowManager.LayoutParams.WRAP_CONTENT, (int)(120 * density));
		
		// assign popup view variables
		mMangaSources		= (Gallery) mMangaSourcePopup.findViewById(R.id.manga_sources);
		
		if (getSupportActionBar() != null) {
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}
		
		// if it is searching, only search once
		if (mSearchQuery != null) {
			// want to show by search query
			initializeModelBySearch();
		}
	}
	
	public void onMangaSourceClicked(Manga manga) {
		final Intent intent = new Intent(getApplicationContext(), ProMangaDetailAct.class);
		intent.putExtra(Utilities.INTENT_MANGA, manga.toJSON().toString());

		startActivity(intent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		final int itemId = item.getItemId();

		if (itemId == android.R.id.home) {
			finish();
		} else if (itemId == R.id.sort_alphabet) {
			Collections.sort(mMangas);
			mShelvesView.setAdapter(new MangaGridAdapter(this, mMangas, mSearchQuery == null));
		} else if (itemId == R.id.sort_rating) {
			Collections.sort(mMangas, new Manga.RatingComparator());
			mShelvesView.setAdapter(new MangaGridAdapter(this, mMangas, mSearchQuery == null));
		} else if (itemId == R.id.setting) {
			final Intent intent = new Intent(getApplicationContext(), SettingAct.class);
			startActivity(intent);
		}
		
		return true;
	}
}
