package net.gogo.mobile.promanga;

import net.gogo.mobile.allmanga.MangaDetailAct;
import net.gogo.mobile.allmanga.helper.AllChapterDownloaderIntentService;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmangalib.R;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.actionbarsherlock.view.MenuItem;

public class ProMangaDetailAct extends MangaDetailAct {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manga_detail);
		startMetadataDownload(false);
	}

	@Override
	protected void setupView() {
		super.setupView();
		if (mMangaMetadataFrag != null) {
			mMangaMetadataFrag.setMangaDetailClass(ProMangaDetailAct.class);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);

		final int itemId = item.getItemId();
		if (itemId == R.id.download_all) {
			Intent intent = new Intent(this,
					AllChapterDownloaderIntentService.class);
			intent.putExtra(Utilities.INTENT_MANGA, mManga.toJSON().toString());
			intent.putExtra(Utilities.INTENT_DETAIL_CLASS,
					ProMangaDetailAct.class);
			startService(intent);
			Toast.makeText(
					this,
					getString(R.string.start_download_all_chapter,
							mManga.getTitle()), Toast.LENGTH_SHORT).show();
		}

		return true;
	}
}
