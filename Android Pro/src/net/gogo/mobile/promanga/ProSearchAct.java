package net.gogo.mobile.promanga;

import net.gogo.mobile.allmanga.SearchAct;
import net.gogo.mobile.allmanga.adapter.AdvancedSearchAdapter;
import net.gogo.mobile.allmanga.helper.Utilities;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ProSearchAct extends SearchAct {
	private final static String TAG = ProSearchAct.class.getSimpleName();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);

		// initialize view
		mTitleImage			= (ImageView) findViewById(R.id.title);
		mSearchBar 			= (EditText) findViewById(R.id.search);
		mAdvanced 			= (ExpandableListView) findViewById(R.id.advanced);
		mSearchButton 		= (Button) findViewById(R.id.search_button);
		mAdvanceButton 		= (Button) findViewById(R.id.advance_button);
		mSearchButtonGroup	= (LinearLayout) findViewById(R.id.search_button_group);
		mSearchMain			= (LinearLayout) findViewById(R.id.search_main);

		// initialize animation
		mGenreEnterAnimation 	= AnimationUtils.loadAnimation(this, R.anim.advance_search_enter);
		mGenreOutAnimation		= AnimationUtils.loadAnimation(this, R.anim.advance_search_out);

		mSearchBar.setOnKeyListener(this);

		mSearchButton.setOnClickListener(this);
		mAdvanceButton.setOnClickListener(this);
		
		mShouldHideTitleImage = getResources().getBoolean(R.bool.should_hide_title);

		//mGenreSearch.setAdapter(new AdvanceSearchItemAdapter(this));
		mAdvanced.setAdapter(new AdvancedSearchAdapter(this));
		mAdvanced.expandGroup(AdvancedSearchAdapter.MANGASOURCE_GROUP);
	}

	@Override
	protected void doSearch() {
		// we have to implement this since (free and paid) are referring to different activity
		Intent intent = new Intent(this, ProMangaShelvesAct.class);
		intent.putExtra(Utilities.INTENT_TITLE, mSearchBar.getText().toString());

		putSearchIntentExtras(intent);

		startActivity(intent);

		// clear focus
		mSearchBar.clearFocus();
	}
}
