package net.gogo.mobile.promanga;

import net.gogo.mobile.allmanga.SettingAct;
import net.gogo.mobile.allmanga.helper.Utilities;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public class MainAct extends TabActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	// HACK: should not do it here
    	Utilities.BASE_READ_LIMIT = Utilities.INFINITE_READ_LIMIT;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Utilities.initializeApps(getApplicationContext(), ProMangaDetailAct.class);

        // make required variable for tabs
        final Resources resource 	= getResources();
        TabHost host 				= getTabHost();
        TabHost.TabSpec spec;
        Intent intent;

        // make search tab
        intent = new Intent(this, ProSearchAct.class);
        spec = host.newTabSpec("search")
        	.setIndicator(getString(R.string.search), resource.getDrawable(android.R.drawable.ic_menu_search))
        	.setContent(intent);
        host.addTab(spec);

        // make read list tab
        intent = new Intent(this, ProMangaShelvesAct.class);
        spec = host.newTabSpec("readlist")
        	.setIndicator(getString(R.string.read_list), resource.getDrawable(R.drawable.not_bookmarked))
        	.setContent(intent);
        host.addTab(spec);

        // make setting tab
        intent = new Intent(this, SettingAct.class);
        spec = host.newTabSpec("setting")
        	.setIndicator(getString(R.string.setting), resource.getDrawable(android.R.drawable.ic_menu_manage))
        	.setContent(intent);
        host.addTab(spec);

        // TODO: for now, let us disable this menu upload and make in other time
        /*
        // make download queue tab
        intent = new Intent(this, SettingAct.class);
        spec = host.newTabSpec("download_queue")
        	.setIndicator(getString(R.string.download_queue), resource.getDrawable(android.R.drawable.ic_menu_upload))
        	.setContent(intent);
        host.addTab(spec);*/
        
        // set the background of the tab so it looks iPhone-ish
        for (int i = 0; i < host.getTabWidget().getChildCount(); ++i) {
        	host.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.tab_background);
        }
    }
}