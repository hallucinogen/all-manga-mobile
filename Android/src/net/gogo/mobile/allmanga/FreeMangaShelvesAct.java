package net.gogo.mobile.allmanga;

import java.util.ArrayList;
import java.util.Collections;

import net.gogo.mobile.allmanga.adapter.MangaGridAdapter;
import net.gogo.mobile.allmanga.adapter.MangaSourceGalleryAdapter;
import net.gogo.mobile.allmanga.helper.MangaMobileServiceStub;
import net.gogo.mobile.allmanga.helper.MangaMobileStorageStub;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmanga.model.Manga;
import net.gogo.mobile.allmanga.model.UnifiedManga;
import net.gogo.mobile.allmanga.view.ShelvesView;
import net.gogo.mobile.allmangalib.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.google.ads.AdRequest.ErrorCode;

public class FreeMangaShelvesAct extends MangaShelvesAct implements AdListener {
	private final static String TAG = FreeMangaShelvesAct.class.getSimpleName();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen_shelves);

		mMangas = new ArrayList<Manga>();

		// create view
		mShelvesView = (ShelvesView) findViewById(R.id.grid_shelves);
		mProgress = new ProgressDialog(this, R.style.SpecialDialog);

		// persist search query
		final Bundle extras = getIntent().getExtras();

		if (extras != null) {
			mSearchQuery = extras.getString(Utilities.INTENT_TITLE);
			mIncludeGenre = new ArrayList<String>();
			mExcludeGenre = new ArrayList<String>();
			mSelectedSources = new ArrayList<String>();
			
			try {
				JSONObject json = new JSONObject(extras.getString(Utilities.INTENT_GENRE));
				
				JSONArray includeArray = json.getJSONArray(Utilities.GENRE_INCLUDE);
				JSONArray excludeArray = json.getJSONArray(Utilities.GENRE_EXCLUDE);
				JSONArray sourcesArray = json.getJSONArray(Utilities.MANGA_SOURCES);
				
				for (int i = 0, n = includeArray.length(); i < n; ++i) {
					mIncludeGenre.add(includeArray.getString(i));
				}
				
				for (int i = 0, n = excludeArray.length(); i < n; ++i) {
					mExcludeGenre.add(excludeArray.getString(i));
				}
				
				for (int i = 0, n = sourcesArray.length(); i < n; ++i) {
					mSelectedSources.add(sourcesArray.getString(i));
				}
			} catch (JSONException jEx) {
				Log.e(TAG, "Error while decoding JSON");
			}
		}
		
		// get density
		final float density = getResources().getDisplayMetrics().density;

		// make manga source popup
		LayoutInflater inflater = LayoutInflater.from(this);
		mMangaSourcePopup = (RelativeLayout) inflater.inflate(R.layout.shelf_selection_popup, null);
		mMangaSourcePopupWindow = new PopupWindow(mMangaSourcePopup, WindowManager.LayoutParams.WRAP_CONTENT, (int)(120 * density));

		// assign popup view variables
		mMangaSources		= (Gallery) mMangaSourcePopup.findViewById(R.id.manga_sources);

		if (getSupportActionBar() != null) {
			getSupportActionBar().setHomeButtonEnabled(true);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}
		
		// if it is searching, only search once
		if (mSearchQuery != null) {
			// want to show by search query
			initializeModelBySearch();
		}

		mAdView = (AdView) findViewById(R.id.ad);
		if (MangaMobileStorageStub.ADS_ENABLED) {
			mAdView.loadAd(new AdRequest());
			mAdView.setAdListener(this);
		}
	}

	@Override
	public void onDismissScreen(Ad arg0) {
		MangaMobileStorageStub.addAdsScoreInPreference(this);
	}

	@Override public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {}
	@Override public void onLeaveApplication(Ad arg0) {}
	@Override public void onPresentScreen(Ad arg0) {}
	@Override public void onReceiveAd(Ad arg0) {}

	@Override
	protected void onDestroy() {
		mAdView.destroy();
		super.onDestroy();
	}

	public void onMangaSourceClicked(Manga manga) {
		final Intent intent = new Intent(getApplicationContext(), FreeMangaDetailAct.class);
		intent.putExtra(Utilities.INTENT_MANGA, manga.toJSON().toString());

		startActivity(intent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		final int itemId = item.getItemId();
		
		if (itemId == android.R.id.home) {
			finish();
		} else if (itemId == R.id.sort_alphabet) {
			Collections.sort(mMangas);
			mShelvesView.setAdapter(new MangaGridAdapter(this, mMangas, mSearchQuery == null));
		} else if (itemId == R.id.sort_rating) {
			Collections.sort(mMangas, new Manga.RatingComparator());
			mShelvesView.setAdapter(new MangaGridAdapter(this, mMangas, mSearchQuery == null));
		} else if (itemId == R.id.setting) {
			final Intent intent = new Intent(getApplicationContext(), SettingAct.class);
			startActivity(intent);
		}

		return true;
	}

	private AdView mAdView;
}
