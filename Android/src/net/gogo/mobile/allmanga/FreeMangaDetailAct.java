package net.gogo.mobile.allmanga;

import net.gogo.mobile.allmanga.helper.AllChapterDownloaderIntentService;
import net.gogo.mobile.allmanga.helper.MangaMobileStorageStub;
import net.gogo.mobile.allmanga.helper.Utilities;
import net.gogo.mobile.allmangalib.R;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.actionbarsherlock.view.MenuItem;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdView;

public class FreeMangaDetailAct extends MangaDetailAct implements AdListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manga_detail);
		startMetadataDownload(false);

		mAdView = (AdView)findViewById(R.id.ad);
		if (MangaMobileStorageStub.ADS_ENABLED) {
			mAdView.loadAd(new AdRequest());
			mAdView.setAdListener(this);
		}
	}

	@Override
	protected void setupView() {
		super.setupView();
		if (mMangaMetadataFrag != null) {
			mMangaMetadataFrag.setMangaDetailClass(FreeMangaDetailAct.class);
		}
	}

	@Override
	public void onDismissScreen(Ad ads) {
		MangaMobileStorageStub.addAdsScoreInPreference(this);
	}
	
	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {}
	@Override
	public void onLeaveApplication(Ad arg0) {}
	@Override
	public void onPresentScreen(Ad arg0) {}
	@Override
	public void onReceiveAd(Ad arg0) {}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		mAdView.destroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);

		final int itemId = item.getItemId();
		if (itemId == R.id.download_all) {
			Intent intent = new Intent(this, AllChapterDownloaderIntentService.class);
			intent.putExtra(Utilities.INTENT_MANGA, mManga.toJSON().toString());
			intent.putExtra(Utilities.INTENT_DETAIL_CLASS, FreeMangaDetailAct.class);
			startService(intent);
			Toast.makeText(this, getString(R.string.start_download_all_chapter, mManga.getTitle()), Toast.LENGTH_SHORT).show();
		} 

		return true;
	}

	// ads stuff
	private AdView mAdView;
}
