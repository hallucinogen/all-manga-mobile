package net.gogo.mobile.allmanga;

import net.gogo.mobile.allmanga.adapter.AdvancedSearchAdapter;
import net.gogo.mobile.allmanga.helper.MangaMobileStorageStub;
import net.gogo.mobile.allmanga.helper.Utilities;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdView;

public class FreeSearchAct extends SearchAct implements AdListener {
	private final static String TAG = FreeSearchAct.class.getSimpleName();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search);

		// initialize view
		mTitleImage			= (ImageView) findViewById(R.id.title);
		mSearchBar 			= (EditText) findViewById(R.id.search);
		mAdvanced 			= (ExpandableListView) findViewById(R.id.advanced);
		mSearchButton 		= (Button) findViewById(R.id.search_button);
		mAdvanceButton 		= (Button) findViewById(R.id.advance_button);
		mProButton 			= (Button) findViewById(R.id.pro_button);
		mSearchButtonGroup	= (LinearLayout) findViewById(R.id.search_button_group);
		mSearchMain			= (LinearLayout) findViewById(R.id.search_main);

		// initialize animation
		mGenreEnterAnimation 	= AnimationUtils.loadAnimation(this, R.anim.advance_search_enter);
		mGenreOutAnimation		= AnimationUtils.loadAnimation(this, R.anim.advance_search_out);

		mSearchBar.setOnKeyListener(this);

		mSearchButton.setOnClickListener(this);
		mAdvanceButton.setOnClickListener(this);
		mProButton.setOnClickListener(this);
		
		mShouldHideTitleImage = getResources().getBoolean(R.bool.should_hide_title);

		//mGenreSearch.setAdapter(new AdvanceSearchItemAdapter(this));
		mAdvanced.setAdapter(new AdvancedSearchAdapter(this));
		mAdvanced.expandGroup(AdvancedSearchAdapter.MANGASOURCE_GROUP);
		
		mAdView = (AdView)findViewById(R.id.ad);
		if (MangaMobileStorageStub.ADS_ENABLED) {
			mAdView.loadAd(new AdRequest());
			mAdView.setAdListener(this);
		}
	}
	
	@Override
	public void onDismissScreen(Ad ads) {
		MangaMobileStorageStub.addAdsScoreInPreference(this);
	}
	
	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {}
	@Override
	public void onLeaveApplication(Ad arg0) {}
	@Override
	public void onPresentScreen(Ad arg0) {}
	@Override
	public void onReceiveAd(Ad arg0) {}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();

		mAdView.destroy();
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);

		if (v == mAdvanceButton) {
			if (mAdvanced.getVisibility() == View.VISIBLE) {
				mAdView.setVisibility(View.GONE);
			} else {
				mAdView.setVisibility(View.VISIBLE);
			}
		} else if (v == mProButton) {
			String appPackageName="net.gogo.mobile.promanga";
			Intent marketIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+appPackageName));
			marketIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			startActivity(marketIntent);
		}
	}

	@Override
	protected void doSearch() {
		Intent intent = new Intent(this, FreeMangaShelvesAct.class);
		intent.putExtra(Utilities.INTENT_TITLE, mSearchBar.getText().toString());

		putSearchIntentExtras(intent);
		startActivity(intent);
		// clear focus
		mSearchBar.clearFocus();
	}

	// ads stuff
	private AdView mAdView;

	private Button mProButton;
}
